package com.zeitheron.mpc.fuel;

import com.zeitheron.hammercore.annotations.MCFBus;
import com.zeitheron.mpc.init.ItemsMPC;
import com.zeitheron.mpc.items.ItemMultiMaterial;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.furnace.FurnaceFuelBurnTimeEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@MCFBus
public class FuelHandler
{
	@SubscribeEvent
	public void burn(FurnaceFuelBurnTimeEvent e)
	{
		e.setBurnTime(Math.max(e.getBurnTime(), getBurnTime(e.getItemStack())));
	}
	
	public int getBurnTime(ItemStack fuel)
	{
		if(fuel.getItem() == Items.WHEAT)
			return 200;
		if(fuel.getItem() == Item.getItemFromBlock(Blocks.DEADBUSH))
			return 400;
		if(fuel.getItem() == Item.getItemFromBlock(Blocks.HAY_BLOCK))
			return 2000;
		if(ItemMultiMaterial.EnumMultiMaterialType.PEAT.isThisMaterial(fuel))
			return 1000;
		return 0;
	}
}
