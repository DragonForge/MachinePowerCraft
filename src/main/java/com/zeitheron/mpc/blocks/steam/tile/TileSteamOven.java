package com.zeitheron.mpc.blocks.steam.tile;

import com.zeitheron.hammercore.tile.TileSyncableTickable;
import com.zeitheron.hammercore.utils.InterItemStack;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.hammercore.utils.inventory.InventoryDummy;
import com.zeitheron.mpc.api.tile.ISteamAcceptor;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;

public class TileSteamOven extends TileSyncableTickable implements ISidedInventory, ISteamAcceptor, IFluidHandler
{
	private InventoryDummy inv = new InventoryDummy(4);
	public double steamStored = 0.0;
	public double[] steamValues = new double[4];
	public FluidTank waterBuf = new FluidTank(8000);
	
	@Override
	public void tick()
	{
		boolean hasChanged = false;
		double prevSteam = this.steamStored;
		this.steamStored -= this.addProgress(Math.min(this.steamStored, 0.001));
		this.steamStored = Math.max(0.0, this.steamStored);
		for(int i = 0; i < 4; ++i)
		{
			if(this.canSmelt(i))
			{
				if(this.steamStored == 0.0)
				{
					this.steamValues[i] = Math.max(this.steamValues[i] - 0.01 / this.inv.getStackInSlot(i).getCount(), 0.0);
				}
				if(this.steamValues[i] < 0.01 * this.inv.getStackInSlot(i).getCount())
					continue;
				double water = this.steamValues[i] * 1000.0 / 20.0;
				this.waterBuf.fill(new FluidStack(FluidRegistry.WATER, (int) water), true);
				this.smelt(i);
				hasChanged = true;
				continue;
			}
			if(this.canSmelt(i) || this.steamValues[i] == 0.0)
				continue;
			this.steamValues[i] = 0.0;
			hasChanged = true;
		}
		if(prevSteam != this.steamStored)
		{
			hasChanged = true;
		}
		if(hasChanged && !this.world.isRemote)
		{
			this.sync();
		}
	}
	
	public boolean canSmelt(int slot)
	{
		return !InterItemStack.isStackNull(FurnaceRecipes.instance().getSmeltingResult(this.inv.getStackInSlot(slot)));
	}
	
	public void smelt(int slot)
	{
		if(this.canSmelt(slot))
		{
			ItemStack in = this.inv.getStackInSlot(slot);
			ItemStack stack = FurnaceRecipes.instance().getSmeltingResult(in).copy();
			stack.setCount(stack.getCount() * in.getCount());
			this.inv.setInventorySlotContents(slot, stack);
			this.steamValues[slot] = 0.0;
		}
	}
	
	public double addProgress(double amt)
	{
		int i;
		if(amt <= 0.0)
		{
			return 0.0;
		}
		int smeltable = 0;
		for(int i2 = 0; i2 < 4; ++i2)
		{
			if(!this.canSmelt(i2))
				continue;
			++smeltable;
		}
		if(smeltable == 0)
		{
			return 0.0;
		}
		amt /= smeltable;
		double prevTotal = 0.0;
		for(i = 0; i < 4; ++i)
		{
			prevTotal += this.steamValues[i];
		}
		for(i = 0; i < 4; ++i)
		{
			if(!this.canSmelt(i))
				continue;
			double[] arrd = this.steamValues;
			int n = i;
			arrd[n] = arrd[n] + amt;
		}
		double cTotal = 0.0;
		for(int i3 = 0; i3 < 4; ++i3)
		{
			cTotal += this.steamValues[i3];
		}
		return cTotal - prevTotal;
	}
	
	@Override
	public double acceptSteam(double amt)
	{
		double canAccept = Math.min(2.0 - this.steamStored, amt);
		this.steamStored += canAccept;
		return canAccept;
	}
	
	@Override
	public boolean canConnectTo(EnumFacing facing)
	{
		return facing != EnumFacing.UP;
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		nbt.setTag("Inventory", WorldUtil.saveInv(this.inv));
		nbt.setDouble("steamStored", this.steamStored);
		NBTTagCompound tag = new NBTTagCompound();
		this.waterBuf.writeToNBT(tag);
		nbt.setTag("Fluid", tag);
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		WorldUtil.readInv(nbt.getTagList("Inventory", 10), this.inv);
		this.steamStored = nbt.getDouble("steamStored");
		this.waterBuf.readFromNBT(nbt.getCompoundTag("Fluid"));
	}
	
	@Override
	public int getSizeInventory()
	{
		return this.inv.getSizeInventory();
	}
	
	@Override
	public boolean isEmpty()
	{
		return this.inv.isEmpty();
	}
	
	@Override
	public ItemStack getStackInSlot(int index)
	{
		return this.inv.getStackInSlot(index);
	}
	
	@Override
	public ItemStack decrStackSize(int index, int count)
	{
		return this.inv.decrStackSize(index, count);
	}
	
	@Override
	public ItemStack removeStackFromSlot(int index)
	{
		return this.inv.removeStackFromSlot(index);
	}
	
	@Override
	public void setInventorySlotContents(int index, ItemStack stack)
	{
		this.inv.setInventorySlotContents(index, stack);
	}
	
	@Override
	public int getInventoryStackLimit()
	{
		return this.inv.getInventoryStackLimit();
	}
	
	@Override
	public boolean isUsableByPlayer(EntityPlayer player)
	{
		return this.inv.isUsableByPlayer(player, this.pos);
	}
	
	@Override
	public void openInventory(EntityPlayer player)
	{
	}
	
	@Override
	public void closeInventory(EntityPlayer player)
	{
	}
	
	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack)
	{
		ItemStack smelted = FurnaceRecipes.instance().getSmeltingResult(stack);
		return stack.getItem() instanceof ItemFood && !InterItemStack.isStackNull(smelted) && smelted.getItem() instanceof ItemFood;
	}
	
	@Override
	public int getField(int id)
	{
		return 0;
	}
	
	@Override
	public void setField(int id, int value)
	{
	}
	
	@Override
	public int getFieldCount()
	{
		return 0;
	}
	
	@Override
	public void clear()
	{
		this.inv.clear();
	}
	
	@Override
	public String getName()
	{
		return "Steam Boiler";
	}
	
	@Override
	public boolean hasCustomName()
	{
		return false;
	}
	
	@Override
	public int[] getSlotsForFace(EnumFacing side)
	{
		return this.inv.getAllAvaliableSlots();
	}
	
	@Override
	public boolean canInsertItem(int index, ItemStack itemStackIn, EnumFacing direction)
	{
		return this.isItemValidForSlot(index, itemStackIn);
	}
	
	@Override
	public boolean canExtractItem(int index, ItemStack stack, EnumFacing direction)
	{
		return !this.isItemValidForSlot(index, stack);
	}
	
	@Override
	public IFluidTankProperties[] getTankProperties()
	{
		return this.waterBuf.getTankProperties();
	}
	
	@Override
	public int fill(FluidStack resource, boolean doFill)
	{
		return 0;
	}
	
	@Override
	public FluidStack drain(FluidStack resource, boolean doDrain)
	{
		return this.waterBuf.drain(resource, doDrain);
	}
	
	@Override
	public FluidStack drain(int maxDrain, boolean doDrain)
	{
		return this.waterBuf.drain(maxDrain, doDrain);
	}
}
