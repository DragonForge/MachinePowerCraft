package com.zeitheron.mpc.blocks.steam.tile;

import java.util.List;

import com.zeitheron.hammercore.tile.TileSyncable;
import com.zeitheron.hammercore.utils.EnumRotation;
import com.zeitheron.mpc.api.tile.ISteamAcceptor;
import com.zeitheron.mpc.init.BlocksMPC;
import com.zeitheron.mpc.init.DamageSourceMPC;

import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.math.AxisAlignedBB;

public class TileSteamGrinder extends TileSyncable implements ISteamAcceptor
{
	@Override
	public double acceptSteam(double amt)
	{
		IBlockState state = this.world.getBlockState(this.pos);
		if(state.getBlock() == BlocksMPC.STEAM_GRINDER)
		{
			EnumFacing facing = EnumFacing.byName(((EnumRotation) state.getValue((IProperty) EnumRotation.FACING)).getName());
			double dmg = amt * 1000.0;
			this.world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, this.pos.getX() + 0.5, this.pos.getY() + 0.5, this.pos.getZ() + 0.5, facing.getFrontOffsetX() / 5.0, 0.0, facing.getFrontOffsetZ() / 5.0, new int[0]);
			this.world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, this.pos.getX() + 0.5, this.pos.getY() + 0.5, this.pos.getZ() + 0.5, facing.getFrontOffsetX() / 5.0, 0.2, facing.getFrontOffsetZ() / 5.0, new int[0]);
			this.world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, this.pos.getX() + 0.5, this.pos.getY() + 0.5, this.pos.getZ() + 0.5, facing.getFrontOffsetX() / 5.0, -0.2, facing.getFrontOffsetZ() / 5.0, new int[0]);
			List<EntityLivingBase> entities = null;
			if(facing == EnumFacing.EAST)
			{
				this.world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, this.pos.getX() + 0.5, this.pos.getY() + 0.5, this.pos.getZ() + 0.5, facing.getFrontOffsetX() / 5.0, 0.0, 0.2, new int[0]);
				this.world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, this.pos.getX() + 0.5, this.pos.getY() + 0.5, this.pos.getZ() + 0.5, facing.getFrontOffsetX() / 5.0, 0.0, -0.2, new int[0]);
				entities = this.world.getEntitiesWithinAABB(EntityLivingBase.class, new AxisAlignedBB(this.pos.getX() + 5, this.pos.getY() - 3, this.pos.getZ() - 3, this.pos.getX() + 0.75, this.pos.getY() + 3, this.pos.getZ() + 3));
			}
			if(facing == EnumFacing.WEST)
			{
				this.world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, this.pos.getX() + 0.5, this.pos.getY() + 0.5, this.pos.getZ() + 0.5, facing.getFrontOffsetX() / 5.0, 0.0, 0.2, new int[0]);
				this.world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, this.pos.getX() + 0.5, this.pos.getY() + 0.5, this.pos.getZ() + 0.5, facing.getFrontOffsetX() / 5.0, 0.0, -0.2, new int[0]);
				entities = this.world.getEntitiesWithinAABB(EntityLivingBase.class, new AxisAlignedBB(this.pos.getX() - 5, this.pos.getY() - 3, this.pos.getZ() - 3, this.pos.getX() + 0.25, this.pos.getY() + 3, this.pos.getZ() + 3));
			}
			if(facing == EnumFacing.SOUTH)
			{
				this.world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, this.pos.getX() + 0.5, this.pos.getY() + 0.5, this.pos.getZ() + 0.5, 0.2, 0.0, facing.getFrontOffsetZ() / 5.0, new int[0]);
				this.world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, this.pos.getX() + 0.5, this.pos.getY() + 0.5, this.pos.getZ() + 0.5, -0.2, 0.0, facing.getFrontOffsetZ() / 5.0, new int[0]);
				entities = this.world.getEntitiesWithinAABB(EntityLivingBase.class, new AxisAlignedBB(this.pos.getX() - 3, this.pos.getY() - 3, this.pos.getZ() + 3, this.pos.getX() + 3, this.pos.getY() + 3, this.pos.getZ() - 0.25));
			}
			if(facing == EnumFacing.NORTH)
			{
				this.world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, this.pos.getX() + 0.5, this.pos.getY() + 0.5, this.pos.getZ() + 0.5, 0.2, 0.0, facing.getFrontOffsetZ() / 5.0, new int[0]);
				this.world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, this.pos.getX() + 0.5, this.pos.getY() + 0.5, this.pos.getZ() + 0.5, -0.2, 0.0, facing.getFrontOffsetZ() / 5.0, new int[0]);
				entities = this.world.getEntitiesWithinAABB(EntityLivingBase.class, new AxisAlignedBB(this.pos.getX() - 3, this.pos.getY() - 3, this.pos.getZ() - 3, this.pos.getX() + 3, this.pos.getY() + 3, this.pos.getZ() + 0.75));
			}
			if(entities != null)
			{
				for(EntityLivingBase living : entities)
				{
					living.attackEntityFrom(DamageSourceMPC.STEAM_GRINDER, (float) (dmg / entities.size()));
				}
				return amt;
			}
		}
		return 0.0;
	}
	
	@Override
	public boolean canConnectTo(EnumFacing facing)
	{
		IBlockState state = this.world.getBlockState(this.pos);
		if(state.getBlock() == BlocksMPC.STEAM_GRINDER)
		{
			EnumFacing block = EnumFacing.byName(((EnumRotation) state.getValue((IProperty) EnumRotation.FACING)).getName());
			return facing == block.getOpposite();
		}
		return false;
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
	}
}
