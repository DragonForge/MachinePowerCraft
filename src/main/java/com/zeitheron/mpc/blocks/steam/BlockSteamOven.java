package com.zeitheron.mpc.blocks.steam;

import com.zeitheron.mpc.blocks.BlockMachineBaseUnrotateable;
import com.zeitheron.mpc.blocks.steam.tile.TileSteamOven;
import com.zeitheron.mpc.ui.gui.GuiSteamOven;
import com.zeitheron.mpc.ui.inv.ContainerSteamOven;

import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockSteamOven extends BlockMachineBaseUnrotateable<TileSteamOven> implements ITileEntityProvider
{
	public BlockSteamOven()
	{
		this.setUnlocalizedName("steam_oven");
	}
	
	@Override
	public Class<TileSteamOven> getTileClass()
	{
		return TileSteamOven.class;
	}
	
	@Override
	public void spawnDrops(TileSteamOven tile, World w, BlockPos p, IBlockState s)
	{
	}
	
	@Override
	public boolean hasGui()
	{
		return true;
	}
	
	@Override
	public Object getClientGuiPart(EntityPlayer player, TileSteamOven tile)
	{
		return new GuiSteamOven(player, tile);
	}
	
	@Override
	public Object getServerGuiPart(EntityPlayer player, TileSteamOven tile)
	{
		return new ContainerSteamOven(player, tile);
	}
}
