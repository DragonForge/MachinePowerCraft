package com.zeitheron.mpc.blocks.steam;

import com.zeitheron.hammercore.utils.EnumRotation;
import com.zeitheron.mpc.blocks.BlockMachineBase;
import com.zeitheron.mpc.blocks.steam.tile.TileSteamGrinder;

import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockSteamGrinder extends BlockMachineBase<TileSteamGrinder>
{
	public BlockSteamGrinder()
	{
		this.setUnlocalizedName("steam_grinder");
	}
	
	@Override
	public Class<TileSteamGrinder> getTileClass()
	{
		return TileSteamGrinder.class;
	}
	
	@Override
	public void spawnDrops(TileSteamGrinder tile, World w, BlockPos p, IBlockState s)
	{
	}
	
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		return super.onBlockActivated(worldIn, pos, state, playerIn, hand, facing, hitX, hitY, hitZ);
	}
	
	@Override
	public boolean rotateBlock(World w, BlockPos p, EnumFacing s)
	{
		if(s.getAxis() != EnumFacing.Axis.Y)
		{
			return false;
		}
		IBlockState state = w.getBlockState(p);
		EnumRotation facing = (EnumRotation) state.getValue((IProperty) EnumRotation.FACING);
		state = state.withProperty((IProperty) EnumRotation.FACING, (Comparable) EnumRotation.valueOf(EnumFacing.byName(facing.getName()).rotateAround(s.getAxis()).name()));
		TileEntity te = w.getTileEntity(p);
		w.setBlockState(p, state, 3);
		te.validate();
		w.setTileEntity(p, te);
		return true;
	}
	
	@Override
	public BlockRenderLayer getBlockLayer()
	{
		return BlockRenderLayer.CUTOUT;
	}
	
	@Override
	public boolean isOpaqueCube(IBlockState p_isOpaqueCube_1_)
	{
		return false;
	}
	
	@Override
	public boolean isNormalCube(IBlockState p_isOpaqueCube_1_)
	{
		return false;
	}
	
	public boolean isFullyOpaque(IBlockState p_isFullyOpaque_1_)
	{
		return false;
	}
	
	@Override
	public boolean isFullCube(IBlockState p_isFullCube_1_)
	{
		return false;
	}
	
	@Override
	public boolean causesSuffocation(IBlockState state)
	{
		return false;
	}
	
	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
	{
		EnumRotation facing = (EnumRotation) state.getValue((IProperty) EnumRotation.FACING);
		if(facing == EnumRotation.EAST)
		{
			return new AxisAlignedBB(0.0, 0.0, 0.0, 0.06875, 1.0, 1.0);
		}
		if(facing == EnumRotation.WEST)
		{
			return new AxisAlignedBB(0.93125, 0.0, 0.0, 1.0, 1.0, 1.0);
		}
		if(facing == EnumRotation.SOUTH)
		{
			return new AxisAlignedBB(0.0, 0.0, 0.0, 1.0, 1.0, 0.06875);
		}
		if(facing == EnumRotation.NORTH)
		{
			return new AxisAlignedBB(0.0, 0.0, 0.93125, 1.0, 1.0, 1.0);
		}
		return FULL_BLOCK_AABB;
	}
}