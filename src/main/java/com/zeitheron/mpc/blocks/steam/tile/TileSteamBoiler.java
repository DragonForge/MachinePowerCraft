package com.zeitheron.mpc.blocks.steam.tile;

import com.zeitheron.hammercore.api.handlers.IHandlerProvider;
import com.zeitheron.hammercore.tile.TileSyncableTickable;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.hammercore.utils.inventory.InventoryDummy;
import com.zeitheron.mpc.api.tile.IClickable;
import com.zeitheron.mpc.api.tile.ISteamAcceptor;
import com.zeitheron.mpc.api.tile.ISteamSender;
import com.zeitheron.mpc.matreg.MaterialRegistry;
import com.zeitheron.mpc.utils.PhysicsUtil;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.FluidUtil;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandlerItem;
import net.minecraftforge.fluids.capability.IFluidTankProperties;

public class TileSteamBoiler extends TileSyncableTickable implements IClickable, IFluidHandler, ISidedInventory, ISteamSender
{
	public InventoryDummy inv = new InventoryDummy(1);
	public double waterTempC = 20.0;
	public double waterKG;
	public double jFuelLeft;
	public double lastJFuelInjected;
	public double jTransmission = 4000.0;
	public double jLoss = 100.0;
	public double steamUnits = 0.0;
	public double biomeTemp = 20.0;
	private FluidTank waterCacheTank = new FluidTank(1000);
	private final int[] slots = new int[] { 0 };
	private final int[] empty = new int[0];
	
	@Override
	public void tick()
	{
		PhysicsUtil.Material fuel;
		Chunk c = this.world.getChunkFromBlockCoords(this.pos);
		double totalTemp = 0.0;
		for(EnumFacing f : EnumFacing.VALUES)
		{
			Biome b;
			if(f.getAxis() == EnumFacing.Axis.Y || (b = c.getBiome(this.pos.offset(f), this.world.getBiomeProvider())) == null)
				continue;
			totalTemp += b.getTemperature() * 25.0f;
		}
		Biome b = c.getBiome(this.pos, this.world.getBiomeProvider());
		if(b != null)
		{
			totalTemp += b.getTemperature() * 25.0f;
		}
		this.biomeTemp = totalTemp / 5.0;
		if(this.ticksExisted == 5 && !this.world.isRemote)
		{
			this.sync();
		}
		if(this.waterCacheTank.getFluidAmount() > 0)
		{
			double prop = this.addWater(this.waterCacheTank.getFluidAmount() / 1000.0 * 20.0);
			this.waterCacheTank.getFluid().amount -= (int) Math.ceil(prop / 20.0 * 1000.0);
		}
		if((fuel = MaterialRegistry.getMaterial(this.inv.getStackInSlot(0))) != null && fuel.getFuelProperty() != null && this.jFuelLeft <= 0.0 && this.waterKG > 0.0)
		{
			this.jFuelLeft += fuel.getFuelProperty().getJoulesProduced(0.25);
			this.lastJFuelInjected = fuel.getFuelProperty().getJoulesProduced(0.25);
			this.inv.getStackInSlot(0).shrink(1);
		}
		boolean hasChanged = false;
		PhysicsUtil.Material water = PhysicsUtil.Material.WATER;
		if(Double.isNaN(this.waterTempC) || Double.isInfinite(this.waterTempC))
		{
			this.waterTempC = water.getMeltableProperty().meltTemperature;
		}
		if(this.waterKG <= 0.0)
		{
			this.waterTempC = this.biomeTemp;
		}
		this.waterKG = Math.max(0.0, this.waterKG);
		if(this.waterTempC > this.biomeTemp && this.waterKG > 0.0)
		{
			this.waterTempC -= water.getMeltableProperty().getTempGiven(this.waterKG, this.jLoss);
			hasChanged = true;
		}
		if(this.jFuelLeft > 0.0)
		{
			this.jFuelLeft -= this.jLoss;
			hasChanged = true;
		}
		if(this.waterTempC < water.getMeltableProperty().meltTemperature && this.waterKG > 0.0)
		{
			double jg = Math.min(this.jTransmission, this.jFuelLeft);
			this.jFuelLeft -= jg;
			this.waterTempC += water.getMeltableProperty().getTempGiven(this.waterKG, jg);
			hasChanged = true;
		} else if(this.waterTempC >= water.getMeltableProperty().meltTemperature)
		{
			if(this.waterTempC > water.getMeltableProperty().meltTemperature)
			{
				double delta = this.waterTempC - water.getMeltableProperty().meltTemperature;
				this.waterTempC -= delta;
				this.jFuelLeft += water.getMeltableProperty().getJoulesUsedToHeat(this.waterKG, delta);
			}
			double jg = Math.min(this.jTransmission, this.jFuelLeft);
			this.jFuelLeft -= jg;
			double convert = water.getMeltableProperty().getKGMelted(jg);
			convert = Math.min(this.waterKG, convert);
			this.waterKG -= convert;
			this.steamUnits += convert;
			hasChanged = true;
		}
		if(this.steamUnits > 0.0)
		{
			TileEntity te = this.world.getTileEntity(this.pos.up());
			ISteamAcceptor acc = WorldUtil.cast((Object) te, ISteamAcceptor.class);
			IHandlerProvider handlerProvider = WorldUtil.cast(te, IHandlerProvider.class);
			if(handlerProvider != null && handlerProvider.hasHandler(EnumFacing.DOWN, ISteamAcceptor.class, new Object[0]))
			{
				acc = handlerProvider.getHandler(EnumFacing.UP.getOpposite(), ISteamAcceptor.class, new Object[0]);
			}
			if(acc != null && acc.canConnectTo(EnumFacing.DOWN))
			{
				double prev = this.steamUnits;
				this.steamUnits -= acc.acceptSteam(this.steamUnits);
				if(this.steamUnits != prev)
				{
					hasChanged = true;
				}
			} else
			{
				this.world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, this.pos.getX() + 0.5, this.pos.getY() + 1, this.pos.getZ() + 0.5, 0.0, 0.15, 0.0, new int[0]);
				this.steamUnits = 0.0;
				hasChanged = true;
			}
		}
		if(hasChanged && !this.world.isRemote)
		{
			this.sync();
		}
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		NBTTagCompound tag = new NBTTagCompound();
		this.inv.getStackInSlot(0).writeToNBT(tag);
		nbt.setTag("inv", tag);
		tag = new NBTTagCompound();
		this.waterCacheTank.writeToNBT(tag);
		nbt.setTag("fluidcache", tag);
		nbt.setDouble("waterTempC", this.waterTempC);
		nbt.setDouble("waterKG", this.waterKG);
		nbt.setDouble("jFuelLeft", this.jFuelLeft);
		nbt.setDouble("jTransmission", this.jTransmission);
		nbt.setDouble("jLoss", this.jLoss);
		nbt.setDouble("steamUnits", this.steamUnits);
		nbt.setDouble("lastJFuelInjected", this.lastJFuelInjected);
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		this.inv.setInventorySlotContents(0, new ItemStack(nbt.getCompoundTag("inv")));
		this.waterCacheTank.readFromNBT(nbt.getCompoundTag("fluidcache"));
		this.waterTempC = nbt.getDouble("waterTempC");
		this.waterKG = nbt.getDouble("waterKG");
		this.jFuelLeft = nbt.getDouble("jFuelLeft");
		this.jTransmission = nbt.getDouble("jTransmission");
		this.jLoss = nbt.getDouble("jLoss");
		this.steamUnits = nbt.getDouble("steamUnits");
		this.lastJFuelInjected = nbt.getDouble("lastJFuelInjected");
	}
	
	public boolean isWaterContainer(ItemStack stack)
	{
		IFluidHandlerItem h = FluidUtil.getFluidHandler(stack);
		if(h != null)
		{
			FluidStack fs = h.drain(1, false);
			return fs != null && fs.getFluid() == FluidRegistry.WATER;
		}
		return false;
	}
	
	public double addWater(double kg)
	{
		kg = Math.max(0.0, Math.min(250.0, this.waterKG + kg));
		double prevKG = this.waterKG;
		double prevC = this.waterTempC;
		double delta = kg - prevKG;
		this.waterKG = kg;
		this.waterTempC = prevKG > 0.0 ? Math.max((prevC * prevKG + this.biomeTemp * delta) / (prevKG + delta), this.biomeTemp) : this.biomeTemp;
		return delta;
	}
	
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		double lpb = 20.0;
		if(playerIn.getHeldItem(hand).getItem() == Items.WATER_BUCKET && this.waterKG + lpb <= 250.0)
		{
			if(!playerIn.capabilities.isCreativeMode)
				playerIn.setHeldItem(hand, new ItemStack(Items.BUCKET));
			this.addWater(lpb);
			return true;
		}
		if(this.isWaterContainer(playerIn.getHeldItem(hand)) && this.waterKG + lpb <= 250.0)
		{
			IFluidHandlerItem h = FluidUtil.getFluidHandler(playerIn.getHeldItem(hand));
			h.drain(1000, true);
			this.addWater(lpb);
			return true;
		}
		return false;
	}
	
	@Override
	public boolean getBlockActivatedReaction(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		return true;
	}
	
	@Override
	public IFluidTankProperties[] getTankProperties()
	{
		return this.waterCacheTank.getTankProperties();
	}
	
	@Override
	public int fill(FluidStack resource, boolean doFill)
	{
		if(resource != null && resource.getFluid() == FluidRegistry.WATER)
		{
			return this.waterCacheTank.fill(resource, doFill);
		}
		return 0;
	}
	
	@Override
	public FluidStack drain(FluidStack resource, boolean doDrain)
	{
		return null;
	}
	
	@Override
	public FluidStack drain(int maxDrain, boolean doDrain)
	{
		return null;
	}
	
	@Override
	public int getSizeInventory()
	{
		return this.inv.getSizeInventory();
	}
	
	@Override
	public boolean isEmpty()
	{
		return this.inv.isEmpty();
	}
	
	@Override
	public ItemStack getStackInSlot(int index)
	{
		return this.inv.getStackInSlot(index);
	}
	
	@Override
	public ItemStack decrStackSize(int index, int count)
	{
		return this.inv.decrStackSize(index, count);
	}
	
	@Override
	public ItemStack removeStackFromSlot(int index)
	{
		return this.inv.removeStackFromSlot(index);
	}
	
	@Override
	public void setInventorySlotContents(int index, ItemStack stack)
	{
		this.inv.setInventorySlotContents(index, stack);
	}
	
	@Override
	public int getInventoryStackLimit()
	{
		return this.inv.getInventoryStackLimit();
	}
	
	@Override
	public boolean isUsableByPlayer(EntityPlayer player)
	{
		return this.inv.isUsableByPlayer(player, this.pos);
	}
	
	@Override
	public void openInventory(EntityPlayer player)
	{
	}
	
	@Override
	public void closeInventory(EntityPlayer player)
	{
	}
	
	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack)
	{
		PhysicsUtil.Material mat = MaterialRegistry.getMaterial(stack);
		return mat != null && mat.getFuelProperty() != null && mat.getFuelProperty().getJoulesProduced(1.0) > 0.0;
	}
	
	@Override
	public int getField(int id)
	{
		return 0;
	}
	
	@Override
	public void setField(int id, int value)
	{
	}
	
	@Override
	public int getFieldCount()
	{
		return 0;
	}
	
	@Override
	public void clear()
	{
		this.inv.clear();
	}
	
	@Override
	public String getName()
	{
		return "Steam Boiler";
	}
	
	@Override
	public boolean hasCustomName()
	{
		return false;
	}
	
	@Override
	public int[] getSlotsForFace(EnumFacing side)
	{
		return side != EnumFacing.UP ? this.slots : this.empty;
	}
	
	@Override
	public boolean canInsertItem(int index, ItemStack itemStackIn, EnumFacing direction)
	{
		return direction != EnumFacing.UP && index == 0 && this.isItemValidForSlot(index, itemStackIn);
	}
	
	@Override
	public boolean canExtractItem(int index, ItemStack stack, EnumFacing direction)
	{
		return direction != EnumFacing.UP && index == 0;
	}
	
	@Override
	public boolean canConnectTo(EnumFacing facing)
	{
		return facing == EnumFacing.UP;
	}
}
