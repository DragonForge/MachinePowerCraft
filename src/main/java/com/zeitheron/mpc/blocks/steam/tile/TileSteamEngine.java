package com.zeitheron.mpc.blocks.steam.tile;

import java.util.HashSet;

import com.zeitheron.hammercore.tile.TileSyncable;
import com.zeitheron.hammercore.utils.EnumRotation;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.mpc.api.tile.IKineticAcceptor;
import com.zeitheron.mpc.api.tile.IKineticSRC;
import com.zeitheron.mpc.api.tile.ISteamAcceptor;
import com.zeitheron.mpc.api.tile.IStraightKineticTransporter;
import com.zeitheron.mpc.init.BlocksMPC;

import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.IBlockState;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;

public class TileSteamEngine extends TileSyncable implements ISteamAcceptor, IKineticSRC
{
	public double rotation = 0.0;
	public double prevRotation = 0.0;
	
	@Override
	public boolean canConnectTo(EnumFacing facing)
	{
		EnumFacing facing0;
		IBlockState state = this.world.getBlockState(this.pos);
		if(state.getBlock() == BlocksMPC.STEAM_ENGINE && facing == (facing0 = (EnumFacing) state.getValue((IProperty) EnumRotation.EFACING)))
			return true;
		return false;
	}
	
	public double toDegrees(double steam)
	{
		return steam * 3600;
	}
	
	public double decreaseDegs(double deg, double kg)
	{
		return deg / kg;
	}
	
	@Override
	public double acceptSteam(double amt)
	{
		double deg = this.toDegrees(amt);
		IBlockState state = this.world.getBlockState(this.pos);
		if(state.getBlock() == BlocksMPC.STEAM_ENGINE)
		{
			Object check;
			EnumFacing facing = state.getValue(EnumRotation.EFACING).getOpposite();
			HashSet<IStraightKineticTransporter> shafts = new HashSet<IStraightKineticTransporter>();
			IKineticAcceptor acc = null;
			int i = 0;
			while(deg > 0.0 && ++i <= 128 && this.world.isAreaLoaded((BlockPos) (check = this.pos.offset(facing, i)), (BlockPos) check))
			{
				IBlockState s0 = this.world.getBlockState((BlockPos) check);
				TileEntity te = this.world.getTileEntity((BlockPos) check);
				IStraightKineticTransporter transporter = WorldUtil.cast((Object) te, IStraightKineticTransporter.class);
				IKineticAcceptor acceptor = WorldUtil.cast((Object) te, IKineticAcceptor.class);
				if(acceptor != null && acceptor.canAcceptKineticFrom(facing.getOpposite()))
				{
					deg = this.decreaseDegs(deg, acceptor.getWeightInKilograms());
					acc = acceptor;
					break;
				}
				if(transporter == null || !transporter.canTransport(s0, facing.getOpposite()))
					break;
				deg = this.decreaseDegs(deg, transporter.getWeightInKilograms());
				shafts.add(transporter);
			}
			prevRotation = rotation;
			rotation += deg;
			rotation %= 3600.0;
			if(Double.isNaN(rotation) || Double.isInfinite(rotation))
				rotation = 0.0;
			for(IStraightKineticTransporter shaft : shafts)
				shaft.rotate(rotation);
			if(acc != null && acc.canAcceptKineticFrom(facing.getOpposite()))
			{
				acc.acceptKinetic(deg, facing.getOpposite());
				acc.setVisualRotation(rotation, facing.getOpposite());
			}
		}
		return amt;
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		nbt.setDouble("rotation", this.rotation);
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		this.rotation = nbt.getDouble("rotation");
	}
	
	@Override
	public boolean isSourceEmittingTo(EnumFacing facing)
	{
		return this.canConnectTo(facing.getOpposite());
	}
}