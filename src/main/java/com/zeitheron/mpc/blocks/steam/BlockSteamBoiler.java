package com.zeitheron.mpc.blocks.steam;

import com.zeitheron.mpc.blocks.BlockMachineBase;
import com.zeitheron.mpc.blocks.steam.tile.TileSteamBoiler;
import com.zeitheron.mpc.ui.gui.GuiSteamBoiler;
import com.zeitheron.mpc.ui.inv.ContainerSteamBoiler;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockSteamBoiler extends BlockMachineBase<TileSteamBoiler>
{
	public BlockSteamBoiler()
	{
		this.setUnlocalizedName("steam_boiler");
		this.setHardness(3.5f);
		this.setHarvestLevel("pickaxe", 1);
	}
	
	@Override
	public void spawnDrops(TileSteamBoiler tile, World w, BlockPos p, IBlockState s)
	{
		tile.inv.drop(w, p);
	}
	
	@Override
	public Class<TileSteamBoiler> getTileClass()
	{
		return TileSteamBoiler.class;
	}
	
	@Override
	public boolean hasGui()
	{
		return true;
	}
	
	@Override
	public Object getClientGuiPart(EntityPlayer player, TileSteamBoiler tile)
	{
		return new GuiSteamBoiler(player, tile);
	}
	
	@Override
	public Object getServerGuiPart(EntityPlayer player, TileSteamBoiler tile)
	{
		return new ContainerSteamBoiler(player, tile);
	}
}