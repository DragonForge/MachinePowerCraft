package com.zeitheron.mpc.blocks.steam;

import com.zeitheron.hammercore.utils.EnumRotation;
import com.zeitheron.hammercore.utils.wrench.IWrenchItem;
import com.zeitheron.mpc.blocks.BlockMachineBaseUnrotateable;
import com.zeitheron.mpc.blocks.energy.BlockPoweredBase;
import com.zeitheron.mpc.blocks.steam.tile.TileSteamEngine;

import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockSteamEngine extends BlockMachineBaseUnrotateable<TileSteamEngine>
{
	public BlockSteamEngine()
	{
		this.setUnlocalizedName("steam_engine");
	}
	
	@Override
	public Class<TileSteamEngine> getTileClass()
	{
		return TileSteamEngine.class;
	}
	
	@Override
	public void spawnDrops(TileSteamEngine tile, World w, BlockPos p, IBlockState s)
	{
	}
	
	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		return this.getDefaultState().withProperty((IProperty) EnumRotation.EFACING, (Comparable) EnumFacing.values()[meta]);
	}
	
	@Override
	public int getMetaFromState(IBlockState state)
	{
		int meta = ((EnumFacing) state.getValue((IProperty) EnumRotation.EFACING)).ordinal();
		return meta;
	}
	
	@Override
	protected BlockStateContainer createBlockState()
	{
		return new BlockStateContainer(this, this.getProperties());
	}
	
	public IProperty[] getProperties()
	{
		return new IProperty[] { EnumRotation.EFACING };
	}
	
	@Override
	public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack)
	{
		worldIn.setBlockState(pos, state.withProperty((IProperty) EnumRotation.EFACING, EnumFacing.getDirectionFromEntityLiving(pos, placer).getOpposite()), 2);
	}
	
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		boolean isNormal;
		super.onBlockActivated(worldIn, pos, state, playerIn, hand, facing, hitX, hitY, hitZ);
		IWrenchItem wrench = BlockPoweredBase.hasUseableWrench(playerIn, hand);
		boolean bl = isNormal = wrench != null;
		if(isNormal)
		{
			boolean wasRotated = false;
			if(!playerIn.isSneaking())
			{
				wasRotated = this.rotateBlock(worldIn, pos, facing);
			}
			return wasRotated;
		}
		return false;
	}
	
	@Override
	public boolean rotateBlock(World w, BlockPos p, EnumFacing s)
	{
		IBlockState state = w.getBlockState(p);
		EnumFacing facing = (EnumFacing) state.getValue((IProperty) EnumRotation.EFACING);
		state = state.withProperty((IProperty) EnumRotation.EFACING, (Comparable) facing.rotateAround(s.getAxis()));
		TileEntity te = w.getTileEntity(p);
		w.setBlockState(p, state, 3);
		te.validate();
		w.setTileEntity(p, te);
		return true;
	}
	
	@Override
	public BlockRenderLayer getBlockLayer()
	{
		return BlockRenderLayer.CUTOUT;
	}
	
	@Override
	public boolean isOpaqueCube(IBlockState p_isOpaqueCube_1_)
	{
		return false;
	}
	
	@Override
	public boolean isNormalCube(IBlockState p_isOpaqueCube_1_)
	{
		return false;
	}
	
	public boolean isFullyOpaque(IBlockState p_isFullyOpaque_1_)
	{
		return false;
	}
	
	@Override
	public boolean isFullCube(IBlockState p_isFullCube_1_)
	{
		return false;
	}
	
	@Override
	public boolean causesSuffocation(IBlockState state)
	{
		return false;
	}
}