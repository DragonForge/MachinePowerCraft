/* Decompiled with CFR 0_123.
 * 
 * Could not load the following classes: net.minecraft.block.Block
 * net.minecraft.block.BlockSoulSand net.minecraft.block.SoundType
 * net.minecraft.block.state.IBlockState
 * net.minecraft.entity.player.EntityPlayer net.minecraft.item.ItemStack
 * net.minecraft.util.math.BlockPos net.minecraft.world.IBlockAccess
 * net.minecraft.world.World */
package com.zeitheron.mpc.blocks;

import java.util.Arrays;
import java.util.List;

import com.zeitheron.mpc.items.ItemMultiMaterial;

import net.minecraft.block.BlockSoulSand;
import net.minecraft.block.SoundType;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockPeat extends BlockSoulSand
{
	public BlockPeat()
	{
		setUnlocalizedName("peat_block");
		setHarvestLevel("shovel", 1);
		setSoundType(SoundType.GROUND);
		setHardness(2.0f);
	}
	
	@Override
	public List<ItemStack> getDrops(IBlockAccess world, BlockPos pos, IBlockState state, int fortune)
	{
		return Arrays.asList(ItemMultiMaterial.EnumMultiMaterialType.PEAT.stack(RANDOM.nextInt(2 + fortune) + 1));
	}
	
	@Override
	public boolean canSilkHarvest(World world, BlockPos pos, IBlockState state, EntityPlayer player)
	{
		return false;
	}
}