package com.zeitheron.mpc.blocks.kinetic;

import com.zeitheron.mpc.blocks.BlockMachineBase;
import com.zeitheron.mpc.blocks.kinetic.tile.TileMechanicalCompressor;
import com.zeitheron.mpc.ui.gui.GuiMechanicalCompressor;
import com.zeitheron.mpc.ui.inv.ContainerMechanicalCompressor;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockMechanicalCompressor extends BlockMachineBase<TileMechanicalCompressor>
{
	public BlockMechanicalCompressor()
	{
		this.setUnlocalizedName("mechanical_compressor");
	}
	
	@Override
	public Class<TileMechanicalCompressor> getTileClass()
	{
		return TileMechanicalCompressor.class;
	}
	
	@Override
	public void spawnDrops(TileMechanicalCompressor tile, World w, BlockPos p, IBlockState s)
	{
	}
	
	@Override
	public boolean hasGui()
	{
		return true;
	}
	
	@Override
	public Object getClientGuiPart(EntityPlayer player, TileMechanicalCompressor tile)
	{
		return new GuiMechanicalCompressor(player, tile);
	}
	
	@Override
	public Object getServerGuiPart(EntityPlayer player, TileMechanicalCompressor tile)
	{
		return new ContainerMechanicalCompressor(player, tile);
	}
}
