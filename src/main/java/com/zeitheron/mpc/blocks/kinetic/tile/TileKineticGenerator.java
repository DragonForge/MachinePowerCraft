package com.zeitheron.mpc.blocks.kinetic.tile;

import com.zeitheron.hammercore.api.handlers.IHandlerProvider;
import com.zeitheron.hammercore.utils.EnumRotation;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.hammercore.utils.math.vec.Cuboid6;
import com.zeitheron.mpc.api.energy.EnergyPacket;
import com.zeitheron.mpc.api.energy.IAmpereReceiver;
import com.zeitheron.mpc.api.energy.impl.TileAmpereSenderBase;
import com.zeitheron.mpc.api.tile.IKineticAcceptor;

import net.minecraft.block.properties.IProperty;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;

public class TileKineticGenerator extends TileAmpereSenderBase implements IKineticAcceptor
{
	public double rotation = 0.0;
	public double prevRotation = 0.0;
	public long _client_frame_1;
	public long _client_frame_2;
	public Cuboid6[] cuboids = new Cuboid6[0];
	public EnumFacing facing;
	public final boolean[] coils = new boolean[8];
	public long lastCuboidBaked = 0;
	
	public void bakeCuboids(EnumFacing facing)
	{
		this.facing = facing;
		Cuboid6[] array = new Cuboid6[this.coils.length + 1];
		array[this.coils.length] = new Cuboid6(0.0, 0.0, 0.0, 1.0, 0.6875, 1.0);
		for(int i = 0; i < array.length - 1; ++i)
		{
			double x;
			double min;
			double width;
			double depth;
			double max;
			double mod;
			Cuboid6 c = null;
			if(facing.getAxis() == EnumFacing.Axis.X)
			{
				mod = 0.875;
				width = 16.0 / (array.length - 1) / 16.0;
				depth = width * i;
				x = 0.0625 + depth;
				min = x * mod;
				max = (x + width) * mod;
				c = new Cuboid6(min, 0.375, 0.375, max, 0.7, 0.625);
			}
			if(facing.getAxis() == EnumFacing.Axis.Z)
			{
				mod = 0.875;
				width = 16.0 / (array.length - 1) / 16.0;
				depth = width * i;
				x = 0.0625 + depth;
				min = x * mod;
				max = (x + width) * mod;
				c = new Cuboid6(0.375, 0.375, min, 0.625, 0.7, max);
			}
			array[i] = c;
		}
		this.cuboids = array;
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		for(int coil = 0; coil < this.coils.length; ++coil)
		{
			nbt.setBoolean("coil" + coil, this.coils[coil]);
		}
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		for(int coil = 0; coil < this.coils.length; ++coil)
		{
			this.coils[coil] = nbt.getBoolean("coil" + coil);
		}
	}
	
	@Override
	public double getWeightInKilograms()
	{
		return 1.25;
	}
	
	@Override
	public boolean canAcceptKineticFrom(EnumFacing facing)
	{
		EnumFacing face = EnumFacing.byName(((EnumRotation) this.world.getBlockState(this.pos).getValue((IProperty) EnumRotation.FACING)).getName());
		return facing == face;
	}
	
	@Override
	public void acceptKinetic(double deg, EnumFacing facing)
	{
		if(canAcceptKineticFrom(facing))
		{
			this.prevRotation = deg;
			this.rotation += deg;
			this.rotation %= 360.0;
			this._client_frame_1 = System.currentTimeMillis();
			this._client_frame_2 = this._client_frame_1 + 50;
			int coils = 0;
			for(int i = 0; i < this.coils.length; ++i)
			{
				if(!this.coils[i])
					continue;
				++coils;
			}
			double amp = Math.abs(deg) / 180.0 * coils;
			int connections = 0;
			for(EnumFacing f : EnumFacing.VALUES)
			{
				if(!this.world.isBlockLoaded(this.pos.offset(f)) || !(this.world.getTileEntity(this.pos.offset(f)) instanceof IHandlerProvider) && !(this.world.getTileEntity(this.pos.offset(f)) instanceof IAmpereReceiver))
					continue;
				IHandlerProvider provider = WorldUtil.cast(this.world.getTileEntity(this.pos.offset(f)), IHandlerProvider.class);
				IAmpereReceiver rec = WorldUtil.cast(this.world.getTileEntity(this.pos.offset(f)), IAmpereReceiver.class);
				if((provider == null || !provider.hasHandler(f.getOpposite(), IAmpereReceiver.class)) && rec == null)
					continue;
				++connections;
			}
			EnergyPacket pkt = EnergyPacket.create(amp / connections);
			for(EnumFacing f : EnumFacing.VALUES)
			{
				if(!this.world.isBlockLoaded(this.pos.offset(f)) || !(this.world.getTileEntity(this.pos.offset(f)) instanceof IHandlerProvider) && !(this.world.getTileEntity(this.pos.offset(f)) instanceof IAmpereReceiver))
					continue;
				IHandlerProvider provider = WorldUtil.cast(this.world.getTileEntity(this.pos.offset(f)), IHandlerProvider.class);
				IAmpereReceiver rec = WorldUtil.cast(this.world.getTileEntity(this.pos.offset(f)), IAmpereReceiver.class);
				if(rec == null && provider != null)
					rec = provider.getHandler(f.getOpposite(), IAmpereReceiver.class);
				if(rec == null)
					continue;
				rec.acceptPacket(pkt, f.getOpposite());
			}
		}
		
		if(Double.isNaN(rotation))
			rotation = 0;
	}
	
	@Override
	public boolean canConnectTo(EnumFacing facing)
	{
		return !this.canAcceptKineticFrom(facing);
	}
}
