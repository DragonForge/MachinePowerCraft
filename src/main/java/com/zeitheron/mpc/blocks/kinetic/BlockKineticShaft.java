package com.zeitheron.mpc.blocks.kinetic;

import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.utils.SoundUtil;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.hammercore.utils.wrench.IWrenchItem;
import com.zeitheron.mpc.blocks.BlockMachineBaseUnrotateable;
import com.zeitheron.mpc.blocks.energy.BlockPoweredBase;
import com.zeitheron.mpc.blocks.kinetic.tile.TileKineticShaft;
import com.zeitheron.mpc.blocks.kinetic.tile.TileKineticShaft.EnumShaftModule;
import com.zeitheron.mpc.items.ItemMultiMaterial.EnumMultiMaterialType;

import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockKineticShaft extends BlockMachineBaseUnrotateable<TileKineticShaft>
{
	public static final PropertyEnum<EnumFacing.Axis> DIR = PropertyEnum.create("dir", EnumFacing.Axis.class);
	public static final AxisAlignedBB AABB_X = new AxisAlignedBB(0.0, 0.375, 0.375, 1.0, 0.625, 0.625);
	public static final AxisAlignedBB AABB_Y = new AxisAlignedBB(0.375, 0.0, 0.375, 0.625, 1.0, 0.625);
	public static final AxisAlignedBB AABB_Z = new AxisAlignedBB(0.375, 0.375, 0.0, 0.625, 0.625, 1.0);
	
	public BlockKineticShaft()
	{
		setUnlocalizedName("kinetic_shaft");
	}
	
	@Override
	public Class<TileKineticShaft> getTileClass()
	{
		return TileKineticShaft.class;
	}
	
	@Override
	public void spawnDrops(TileKineticShaft tile, World w, BlockPos p, IBlockState s)
	{
	}

	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		return this.getDefaultState().withProperty(DIR, EnumFacing.Axis.values()[meta]);
	}

	@Override
	public int getMetaFromState(IBlockState state)
	{
		int meta = state.getValue(DIR).ordinal();
		return meta;
	}

	@Override
	protected BlockStateContainer createBlockState()
	{
		return new BlockStateContainer(this, DIR);
	}
	
	@Override
	public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack)
	{
		worldIn.setBlockState(pos, state.withProperty(DIR, EnumFacing.getDirectionFromEntityLiving(pos, placer).getAxis()), 2);
	}
	
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		super.onBlockActivated(worldIn, pos, state, playerIn, hand, facing, hitX, hitY, hitZ);
		
		IWrenchItem wrench = BlockPoweredBase.hasUseableWrench(playerIn, hand);
		if(wrench != null)
		{
			boolean wasRotated = false;
			if(!playerIn.isSneaking())
				wasRotated = this.rotateBlock(worldIn, pos, facing);
			return wasRotated;
		}
		
		ItemStack stack = playerIn.getHeldItem(hand);
		
		if(!worldIn.isRemote && EnumMultiMaterialType.WATER_WHEEL.isThisMaterial(stack))
		{
			TileKineticShaft tile = WorldUtil.cast(worldIn.getTileEntity(pos), TileKineticShaft.class);
			if(tile != null && tile.module.empty())
			{
				tile.module = EnumShaftModule.WATER_WHEEL;
				if(!playerIn.capabilities.isCreativeMode)
					stack.shrink(1);
				tile.sync();
				HCNet.swingArm(playerIn, hand);
				SoundUtil.playSoundEffect(tile.getLocation(), "entity.leashknot.place", .5F, .6F, SoundCategory.PLAYERS);
			}
		} else if(!worldIn.isRemote && EnumMultiMaterialType.WIND_MILL.isThisMaterial(stack))
		{
			TileKineticShaft tile = WorldUtil.cast(worldIn.getTileEntity(pos), TileKineticShaft.class);
			if(tile != null && tile.module.empty())
			{
				tile.module = EnumShaftModule.WIND_MILL;
				if(!playerIn.capabilities.isCreativeMode)
					stack.shrink(1);
				tile.sync();
				HCNet.swingArm(playerIn, hand);
				SoundUtil.playSoundEffect(tile.getLocation(), "entity.leashknot.place", .5F, .6F, SoundCategory.PLAYERS);
			}
		}
		
		return false;
	}
	
	@Override
	public boolean rotateBlock(World w, BlockPos p, EnumFacing s)
	{
		IBlockState state = w.getBlockState(p);
		EnumFacing.Axis axis = state.getValue(DIR);
		if(s.getAxis() == EnumFacing.Axis.Y && (axis == EnumFacing.Axis.X || axis == EnumFacing.Axis.Z))
			axis = axis == EnumFacing.Axis.X ? EnumFacing.Axis.Z : EnumFacing.Axis.X;
		if(s.getAxis() == EnumFacing.Axis.X && (axis == EnumFacing.Axis.Y || axis == EnumFacing.Axis.Z))
			axis = axis == EnumFacing.Axis.Y ? EnumFacing.Axis.Z : EnumFacing.Axis.Y;
		if(s.getAxis() == EnumFacing.Axis.Z && (axis == EnumFacing.Axis.X || axis == EnumFacing.Axis.Y))
			axis = axis == EnumFacing.Axis.X ? EnumFacing.Axis.Y : EnumFacing.Axis.X;
		state = state.withProperty(DIR, axis);
		TileEntity te = w.getTileEntity(p);
		w.setBlockState(p, state, 3);
		te.validate();
		w.setTileEntity(p, te);
		return true;
	}
	
	@Override
	public EnumBlockRenderType getRenderType(IBlockState s)
	{
		return EnumBlockRenderType.INVISIBLE;
	}

	@Override
	public BlockRenderLayer getBlockLayer()
	{
		return BlockRenderLayer.CUTOUT;
	}

	@Override
	public boolean isOpaqueCube(IBlockState p_isOpaqueCube_1_)
	{
		return false;
	}

	@Override
	public boolean isNormalCube(IBlockState p_isOpaqueCube_1_)
	{
		return false;
	}

	@Override
	public boolean isFullCube(IBlockState p_isFullCube_1_)
	{
		return false;
	}

	@Override
	public boolean causesSuffocation(IBlockState state)
	{
		return false;
	}
	
	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
	{
		TileKineticShaft tile = WorldUtil.cast(source.getTileEntity(pos), TileKineticShaft.class);
		boolean wheel = tile != null && !tile.module.empty();
		
		EnumFacing.Axis axis = state.getValue(DIR);
		return wheel ? FULL_BLOCK_AABB : axis == EnumFacing.Axis.X ? AABB_X : (axis == EnumFacing.Axis.Z ? AABB_Z : AABB_Y);
	}
}