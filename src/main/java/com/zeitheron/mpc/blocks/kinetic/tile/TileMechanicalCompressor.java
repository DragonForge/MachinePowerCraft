package com.zeitheron.mpc.blocks.kinetic.tile;

import com.zeitheron.hammercore.tile.TileSyncable;
import com.zeitheron.hammercore.utils.InterItemStack;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.hammercore.utils.inventory.InventoryDummy;
import com.zeitheron.hammercore.utils.match.item.ItemContainer;
import com.zeitheron.hammercore.utils.match.item.ItemMatchParams;
import com.zeitheron.mpc.api.EnumMachineType;
import com.zeitheron.mpc.api.recipes.compressor.RecipesCompressor;
import com.zeitheron.mpc.api.tile.IKineticAcceptor;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;

public class TileMechanicalCompressor extends TileSyncable implements IKineticAcceptor, ISidedInventory
{
	public final InventoryDummy inv = new InventoryDummy(2);
	private final ItemMatchParams PARAMS = new ItemMatchParams().setUseDamage(true).setUseNBT(true);
	public double degrees = 0.0;
	
	public void compress()
	{
		this.degrees = 0.0;
		ItemStack out = RecipesCompressor.get(EnumMachineType.MECHANICAL).get(this.inv.getStackInSlot(0));
		if(out.isEmpty())
			return;
		ItemStack slot = this.inv.getStackInSlot(1);
		if(!InterItemStack.isStackNull(out) && (InterItemStack.isStackNull(slot) || ItemContainer.create(slot).matches(out, this.PARAMS) && slot.getCount() + out.getCount() <= slot.getMaxStackSize()))
		{
			if(InterItemStack.isStackNull(slot))
				this.inv.setInventorySlotContents(1, out.copy());
			else
				slot.grow(out.getCount());
			this.inv.getStackInSlot(0).shrink(1);
		}
	}
	
	public boolean canCompress()
	{
		if(this.inv.getStackInSlot(0).isEmpty())
			return false;
		ItemStack out = RecipesCompressor.get(EnumMachineType.MECHANICAL).get(this.inv.getStackInSlot(0));
		if(out.isEmpty())
			return false;
		ItemStack slot = this.inv.getStackInSlot(1);
		return !InterItemStack.isStackNull(out) && (InterItemStack.isStackNull(slot) || ItemContainer.create(slot).matches(out, this.PARAMS) && slot.getCount() + out.getCount() <= slot.getMaxStackSize());
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		nbt.setTag("Inventory", WorldUtil.saveInv(this.inv));
		nbt.setDouble("degrees", this.degrees);
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		WorldUtil.readInv(nbt.getTagList("Inventory", 10), this.inv);
		this.degrees = nbt.getDouble("degrees");
	}
	
	@Override
	public boolean canAcceptKineticFrom(EnumFacing facing)
	{
		return true;
	}
	
	@Override
	public void acceptKinetic(double deg, EnumFacing facing)
	{
		this.degrees = this.canCompress() ? (this.degrees += deg) : 0.0;
		if(this.degrees >= 2880.0)
		{
			if(!this.world.isRemote)
			{
				this.compress();
				this.sync();
			} else
			{
				this.degrees = 0.0;
			}
		}
	}
	
	@Override
	public int getSizeInventory()
	{
		return this.inv.getSizeInventory();
	}
	
	@Override
	public boolean isEmpty()
	{
		return this.inv.isEmpty();
	}
	
	@Override
	public ItemStack getStackInSlot(int index)
	{
		return this.inv.getStackInSlot(index);
	}
	
	@Override
	public ItemStack decrStackSize(int index, int count)
	{
		return this.inv.decrStackSize(index, count);
	}
	
	@Override
	public ItemStack removeStackFromSlot(int index)
	{
		return this.inv.removeStackFromSlot(index);
	}
	
	@Override
	public void setInventorySlotContents(int index, ItemStack stack)
	{
		this.inv.setInventorySlotContents(index, stack);
	}
	
	@Override
	public int getInventoryStackLimit()
	{
		return this.inv.getInventoryStackLimit();
	}
	
	@Override
	public boolean isUsableByPlayer(EntityPlayer player)
	{
		return this.inv.isUsableByPlayer(player, this.pos);
	}
	
	@Override
	public void openInventory(EntityPlayer player)
	{
	}
	
	@Override
	public void closeInventory(EntityPlayer player)
	{
	}
	
	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack)
	{
		return index == 0 && !RecipesCompressor.get(EnumMachineType.MECHANICAL).get(stack).isEmpty();
	}
	
	@Override
	public int getField(int id)
	{
		return 0;
	}
	
	@Override
	public void setField(int id, int value)
	{
	}
	
	@Override
	public int getFieldCount()
	{
		return 0;
	}
	
	@Override
	public void clear()
	{
		this.inv.clear();
	}
	
	@Override
	public String getName()
	{
		return "Mechanical Crusher";
	}
	
	@Override
	public boolean hasCustomName()
	{
		return false;
	}
	
	@Override
	public int[] getSlotsForFace(EnumFacing side)
	{
		return this.inv.getAllAvaliableSlots();
	}
	
	@Override
	public boolean canInsertItem(int index, ItemStack itemStackIn, EnumFacing direction)
	{
		return this.isItemValidForSlot(index, itemStackIn);
	}
	
	@Override
	public boolean canExtractItem(int index, ItemStack stack, EnumFacing direction)
	{
		return index == 1;
	}
	
	@Override
	public double getWeightInKilograms()
	{
		return 1.0;
	}
}
