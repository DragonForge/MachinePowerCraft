package com.zeitheron.mpc.blocks.kinetic.tile;

import java.util.HashSet;
import java.util.Set;

import com.zeitheron.hammercore.tile.TileSyncableTickable;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.mpc.api.tile.IKineticAcceptor;
import com.zeitheron.mpc.api.tile.IKineticSRC;
import com.zeitheron.mpc.api.tile.IStraightKineticTransporter;
import com.zeitheron.mpc.intr.exnihiliocreatio.EXNCRotator;

import net.minecraft.block.state.IBlockState;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;

public class TileMechanicalLever extends TileSyncableTickable implements IKineticAcceptor
{
	public Set<EnumFacing> CONNECTIONS = new HashSet<EnumFacing>();
	public double[] rotation = new double[6];
	public double[] prevRotation = new double[6];
	public double[] _client_rotation = new double[6];
	public long[] _client_frame_1 = new long[6];
	public long[] _client_frame_2 = new long[6];
	private int callsPerTick = 0;
	
	@Override
	public void tick()
	{
		if(this.ticksExisted % 10 == 0)
			this.updateConnections();
		if(this.callsPerTick > 8)
		{
			if(!this.world.isRemote)
			{
				this.world.createExplosion(null, this.pos.getX(), this.pos.getY(), this.pos.getZ(), 1.0f, false);
				this.world.destroyBlock(this.pos, true);
			}
			return;
		}
		this.callsPerTick = 0;
	}
	
	public void updateConnections()
	{
		HashSet<EnumFacing> CONNECTIONS = new HashSet<EnumFacing>();
		for(EnumFacing f : EnumFacing.VALUES)
		{
			boolean canConnect = false;
			TileEntity te = this.world.getTileEntity(this.pos.offset(f));
			IKineticSRC src = WorldUtil.cast((Object) te, IKineticSRC.class);
			canConnect = src != null && src.isSourceEmittingTo(f.getOpposite()) ? true : this.canSendTo(f);
			if(canConnect)
			{
				CONNECTIONS.add(f);
				continue;
			}
			this.rotation[f.ordinal()] = 0.0;
			this.prevRotation[f.ordinal()] = 0.0;
			this._client_rotation[f.ordinal()] = 0.0;
			this._client_frame_1[f.ordinal()] = 0;
			this._client_frame_2[f.ordinal()] = 0;
		}
		this.CONNECTIONS = CONNECTIONS;
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		for(int i = 0; i < 6; ++i)
			this.rotation[i] = nbt.getDouble("rotation" + i);
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		for(int i = 0; i < 6; ++i)
			nbt.setDouble("rotation" + i, this.rotation[i]);
	}
	
	@Override
	public boolean canAcceptKineticFrom(EnumFacing facing)
	{
		return true;
	}
	
	@Override
	public void acceptKinetic(double deg, EnumFacing facing)
	{
		this.prevRotation[facing.ordinal()] = deg;
		double[] arrd = this.rotation;
		int n = facing.ordinal();
		arrd[n] = arrd[n] + deg;
		double[] arrd2 = this.rotation;
		int n2 = facing.ordinal();
		arrd2[n2] = arrd2[n2] % 360.0;
		this._client_frame_1[facing.ordinal()] = System.currentTimeMillis();
		this._client_frame_2[facing.ordinal()] = this._client_frame_1[facing.ordinal()] + 50;
		if(this.callsPerTick > 8 || Thread.currentThread().getStackTrace().length >= 800)
		{
			if(!this.world.isRemote)
			{
				this.world.createExplosion(null, this.pos.getX(), this.pos.getY(), this.pos.getZ(), 1.0f, false);
				this.world.destroyBlock(this.pos, true);
			}
			return;
		}
		++this.callsPerTick;
		int sides = 0;
		for(EnumFacing side : EnumFacing.VALUES)
		{
			if(side == facing || !this.canSendTo(side))
				continue;
			++sides;
		}
		if(sides == 0)
			return;
		for(EnumFacing side : EnumFacing.VALUES)
		{
			if(side != facing && this.canSendTo(side))
				this.sendTo(side, deg / sides);
			if(!Double.isNaN(this.rotation[side.ordinal()]) && !Double.isInfinite(this.rotation[side.ordinal()]))
				continue;
			this.rotation[side.ordinal()] = 0.0;
		}
	}
	
	public boolean canSendTo(EnumFacing face)
	{
		BlockPos pos = this.pos.offset(face);
		if(!this.world.isAreaLoaded(pos, pos))
			return false;
		TileEntity te = this.world.getTileEntity(pos);
		if(EXNCRotator.isRotateable(te))
			return true;
		IStraightKineticTransporter transporter = WorldUtil.cast((Object) te, IStraightKineticTransporter.class);
		if(transporter != null && transporter.canTransport(this.world.getBlockState(pos), face.getOpposite()))
			return true;
		IKineticAcceptor acceptor = WorldUtil.cast((Object) te, IKineticAcceptor.class);
		if(acceptor != null && acceptor.canAcceptKineticFrom(face.getOpposite()))
			return true;
		return false;
	}
	
	public double decreaseDegs(double deg, double kg)
	{
		return deg / kg;
	}
	
	public void sendTo(EnumFacing facing, double deg)
	{
		BlockPos check;
		HashSet<IStraightKineticTransporter> shafts = new HashSet<IStraightKineticTransporter>();
		IKineticAcceptor acc = null;
		int i = 0;
		while(deg > 0.0 && ++i <= 128 && this.world.isAreaLoaded(check = this.pos.offset(facing, i), check))
		{
			IBlockState s0 = world.getBlockState(check);
			TileEntity te = world.getTileEntity(check);
			IStraightKineticTransporter transporter = WorldUtil.cast(te, IStraightKineticTransporter.class);
			IKineticAcceptor acceptor = WorldUtil.cast(te, IKineticAcceptor.class);
			if(acceptor != null && acceptor.canAcceptKineticFrom(facing.getOpposite()))
			{
				acc = acceptor;
				deg = decreaseDegs(deg, acceptor.getWeightInKilograms());
				break;
			} else if(EXNCRotator.isRotateable(te))
			{
				EXNCRotator.addEffectiveRotation(te, (float) deg / -1.2F);
				break;
			}
			if(transporter == null || !transporter.canTransport(s0, facing.getOpposite()))
				break;
			deg = this.decreaseDegs(deg, transporter.getWeightInKilograms());
			shafts.add(transporter);
		}
		this.prevRotation[facing.ordinal()] = deg;
		double[] arrd = this.rotation;
		int n = facing.ordinal();
		arrd[n] = arrd[n] + deg;
		double[] arrd2 = this.rotation;
		int n2 = facing.ordinal();
		arrd2[n2] = arrd2[n2] % 360.0;
		if(Double.isNaN(this.rotation[facing.ordinal()]) || Double.isInfinite(this.rotation[facing.ordinal()]))
			this.rotation[facing.ordinal()] = 0.0;
		for(IStraightKineticTransporter shaft : shafts)
			shaft.rotate(this.rotation[facing.ordinal()]);
		if(acc != null && acc.canAcceptKineticFrom(facing.getOpposite()))
		{
			acc.acceptKinetic(deg, facing.getOpposite());
			acc.setVisualRotation(rotation[facing.ordinal()], facing.getOpposite());
		}
		this._client_frame_1[facing.ordinal()] = System.currentTimeMillis();
		this._client_frame_2[facing.ordinal()] = this._client_frame_1[facing.ordinal()] + 50;
		if(acc instanceof TileMechanicalLever)
		{
			TileMechanicalLever l = (TileMechanicalLever) acc;
			l.prevRotation[facing.getOpposite().ordinal()] = deg;
			l.rotation[facing.getOpposite().ordinal()] = this.rotation[facing.ordinal()];
			double[] arrd3 = l.rotation;
			int n3 = facing.getOpposite().ordinal();
			arrd3[n3] = arrd3[n3] % 360.0;
			if(Double.isNaN(l.rotation[facing.getOpposite().ordinal()]) || Double.isInfinite(l.rotation[facing.getOpposite().ordinal()]))
			{
				l.rotation[facing.getOpposite().ordinal()] = 0.0;
			}
			l._client_frame_1[facing.getOpposite().ordinal()] = System.currentTimeMillis();
			l._client_frame_2[facing.getOpposite().ordinal()] = l._client_frame_1[facing.getOpposite().ordinal()] + 50;
		}
	}
	
	@Override
	public double getWeightInKilograms()
	{
		return this.CONNECTIONS.size() * 0.625;
	}
	
	@Override
	public void setVisualRotation(double deg, EnumFacing from)
	{
		this.rotation[from.ordinal()] = deg;
	}
}