package com.zeitheron.mpc.blocks.kinetic;

import com.zeitheron.mpc.blocks.BlockMachineBase;
import com.zeitheron.mpc.blocks.kinetic.tile.TileMechanicalCrusher;
import com.zeitheron.mpc.ui.gui.GuiMechanicalCrusher;
import com.zeitheron.mpc.ui.inv.ContainerMechanicalCrusher;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockMechanicalCrusher extends BlockMachineBase<TileMechanicalCrusher>
{
	public BlockMechanicalCrusher()
	{
		this.setUnlocalizedName("mechanical_crusher");
		this.setHardness(3.5f);
		this.setHarvestLevel("pickaxe", 1);
	}
	
	@Override
	public void spawnDrops(TileMechanicalCrusher tile, World w, BlockPos p, IBlockState s)
	{
		tile.inv.drop(w, p);
	}
	
	@Override
	public Class<TileMechanicalCrusher> getTileClass()
	{
		return TileMechanicalCrusher.class;
	}
	
	@Override
	public boolean hasGui()
	{
		return true;
	}
	
	@Override
	public Object getClientGuiPart(EntityPlayer player, TileMechanicalCrusher tile)
	{
		return new GuiMechanicalCrusher(player, tile);
	}
	
	@Override
	public Object getServerGuiPart(EntityPlayer player, TileMechanicalCrusher tile)
	{
		return new ContainerMechanicalCrusher(player, tile);
	}
}