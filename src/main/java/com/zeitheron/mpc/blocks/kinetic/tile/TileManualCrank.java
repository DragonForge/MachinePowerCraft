package com.zeitheron.mpc.blocks.kinetic.tile;

import java.util.HashSet;

import com.zeitheron.hammercore.tile.TileSyncable;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.mpc.api.tile.IKineticAcceptor;
import com.zeitheron.mpc.api.tile.IKineticSRC;
import com.zeitheron.mpc.api.tile.IStraightKineticTransporter;
import com.zeitheron.mpc.intr.exnihiliocreatio.EXNCRotator;

import net.minecraft.block.state.IBlockState;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;

public class TileManualCrank extends TileSyncable implements IKineticSRC
{
	public double rotation = 0.0;
	public double prevRotation = 0.0;
	public double _client_rotation = 0.0;
	public long _client_frame_1;
	public long _client_frame_2;
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
	}
	
	@Override
	public boolean isSourceEmittingTo(EnumFacing facing)
	{
		return facing == EnumFacing.DOWN;
	}
	
	public void send(double deg)
	{
		Object check;
		EnumFacing facing = EnumFacing.DOWN;
		HashSet<IStraightKineticTransporter> shafts = new HashSet<IStraightKineticTransporter>();
		IKineticAcceptor acc = null;
		int i = 0;
		while(deg > 0.0 && ++i <= 128 && this.world.isAreaLoaded((BlockPos) (check = this.pos.offset(facing, i)), (BlockPos) check))
		{
			IBlockState s0 = this.world.getBlockState((BlockPos) check);
			TileEntity te = this.world.getTileEntity((BlockPos) check);
			IStraightKineticTransporter transporter = WorldUtil.cast((Object) te, IStraightKineticTransporter.class);
			IKineticAcceptor acceptor = WorldUtil.cast((Object) te, IKineticAcceptor.class);
			if(acceptor != null && acceptor.canAcceptKineticFrom(facing.getOpposite()))
			{
				deg = this.decreaseDegs(deg, acceptor.getWeightInKilograms());
				acc = acceptor;
				break;
			} else if(EXNCRotator.isRotateable(te))
			{
				EXNCRotator.addEffectiveRotation(te, (float) deg / -1.2F);
				break;
			}
			if(transporter == null || !transporter.canTransport(s0, facing.getOpposite()))
				break;
			deg = this.decreaseDegs(deg, transporter.getWeightInKilograms());
			shafts.add(transporter);
		}
		this.prevRotation = this.rotation;
		this.rotation += deg;
		this.rotation %= 3600.0;
		if(Double.isNaN(this.rotation) || Double.isInfinite(this.rotation))
			this.rotation = 0.0;
		for(IStraightKineticTransporter shaft : shafts)
			shaft.rotate(this.rotation);
		if(acc != null && acc.canAcceptKineticFrom(facing.getOpposite()))
			acc.acceptKinetic(deg, facing.getOpposite());
		this._client_frame_1 = System.currentTimeMillis();
		this._client_frame_2 = this._client_frame_1 + 500;
	}
	
	public double decreaseDegs(double deg, double kg)
	{
		return deg / kg;
	}
}