package com.zeitheron.mpc.blocks.kinetic;

import com.zeitheron.mpc.blocks.BlockMachineBaseUnrotateable;
import com.zeitheron.mpc.blocks.kinetic.tile.TileMechanicalLever;

import net.minecraft.block.state.IBlockState;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockMechanicalLever extends BlockMachineBaseUnrotateable<TileMechanicalLever>
{
	public BlockMechanicalLever()
	{
		this.setUnlocalizedName("mechanical_lever");
	}
	
	@Override
	public Class<TileMechanicalLever> getTileClass()
	{
		return TileMechanicalLever.class;
	}
	
	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
	{
		return FULL_BLOCK_AABB.shrink(0.28125);
	}
	
	@Override
	public void spawnDrops(TileMechanicalLever tile, World w, BlockPos p, IBlockState s)
	{
	}
	
	@Override
	public BlockRenderLayer getBlockLayer()
	{
		return BlockRenderLayer.CUTOUT;
	}
	
	@Override
	public boolean isOpaqueCube(IBlockState p_isOpaqueCube_1_)
	{
		return false;
	}
	
	@Override
	public boolean isNormalCube(IBlockState p_isOpaqueCube_1_)
	{
		return false;
	}
	
	public boolean isFullyOpaque(IBlockState p_isFullyOpaque_1_)
	{
		return false;
	}
	
	@Override
	public boolean isFullCube(IBlockState p_isFullCube_1_)
	{
		return false;
	}
	
	@Override
	public boolean causesSuffocation(IBlockState state)
	{
		return false;
	}
}