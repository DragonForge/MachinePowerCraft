package com.zeitheron.mpc.blocks.kinetic.tile;

import java.util.HashSet;

import com.zeitheron.hammercore.tile.ITileDroppable;
import com.zeitheron.hammercore.tile.TileSyncableTickable;
import com.zeitheron.hammercore.utils.FastNoise;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.mpc.api.tile.IKineticAcceptor;
import com.zeitheron.mpc.api.tile.IStraightKineticTransporter;
import com.zeitheron.mpc.blocks.kinetic.BlockKineticShaft;
import com.zeitheron.mpc.init.BlocksMPC;
import com.zeitheron.mpc.intr.exnihiliocreatio.EXNCRotator;
import com.zeitheron.mpc.items.ItemMultiMaterial.EnumMultiMaterialType;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumFacing.Axis;
import net.minecraft.util.EnumFacing.AxisDirection;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class TileKineticShaft extends TileSyncableTickable implements IStraightKineticTransporter, ITileDroppable
{
	public double rotation = 0.0;
	public double prevRotation = 0.0;
	public double _client_rotation = 0.0;
	public long _client_frame_1;
	public long _client_frame_2;
	
	public EnumShaftModule module = EnumShaftModule.NONE;
	
	@Override
	public void tick()
	{
		if(module.empty())
			return;
		
		double rotate = 0;
		
		if(module.isWaterWheel())
		{
			IBlockState ls = getLocation().getState();
			Axis ax = ls.getValue(BlockKineticShaft.DIR);
			
			Vec3d vec = new Vec3d(0, 0, 0);
			
			if(ax != Axis.X)
			{
				IBlockState state = getLocation().offset(EnumFacing.EAST).getState();
				vec = state.getBlock().modifyAcceleration(world, pos.offset(EnumFacing.EAST), null, vec);
				
				state = getLocation().offset(EnumFacing.WEST).getState();
				vec = state.getBlock().modifyAcceleration(world, pos.offset(EnumFacing.WEST), null, vec);
			}
			
			if(ax != Axis.Y)
			{
				IBlockState state = getLocation().offset(EnumFacing.UP).getState();
				vec = state.getBlock().modifyAcceleration(world, pos.offset(EnumFacing.UP), null, vec);
				
				state = getLocation().offset(EnumFacing.DOWN).getState();
				vec = state.getBlock().modifyAcceleration(world, pos.offset(EnumFacing.DOWN), null, vec);
			}
			
			if(ax != Axis.Z)
			{
				IBlockState state = getLocation().offset(EnumFacing.SOUTH).getState();
				vec = state.getBlock().modifyAcceleration(world, pos.offset(EnumFacing.SOUTH), null, vec);
				
				state = getLocation().offset(EnumFacing.NORTH).getState();
				vec = state.getBlock().modifyAcceleration(world, pos.offset(EnumFacing.NORTH), null, vec);
			}
			
			if(ax == Axis.X)
				vec = new Vec3d(0, vec.y, vec.z);
			
			if(ax == Axis.Y)
				vec = new Vec3d(vec.x, 0, vec.z);
			
			if(ax == Axis.Z)
				vec = new Vec3d(vec.x, vec.y, 0);
			
			rotate = vec.x + vec.y + vec.z;
			
			// TODO: Make better rotation calc algorithm
			
			if(rotate < 1)
				rotate = 1 / rotate;
		} else if(module.isWindMill())
		{
			double append = ticksExisted / 256D + pos.getY();
			boolean inv = getPos().toLong() % 2 == 0L;
			
			double x = pos.getX() + (inv ? 0 : append);
			double z = pos.getZ() + (inv ? append : 0);
			
			float y = getPos().getY();
			if(y < world.getSeaLevel())
				y = 0;
			else
				y = y / 220F;
			
			float weather = world.isThundering() ? 4F : world.isRaining() ? 2F : 1F;
			
			float wind = FastNoise.noise(x, z, 7) / 255F * weather;
			
			rotate = wind * y;
		}
		
		if(rotate != 0)
			send(Math.abs(rotate));
	}
	
	public void send(double deg)
	{
		IBlockState ls = getLocation().getState();
		Axis ax = ls.getValue(BlockKineticShaft.DIR);
		
		EnumFacing facing = EnumFacing.getFacingFromAxis(AxisDirection.NEGATIVE, ax);
		
		for(int j = 0; j < 2; ++j)
		{
			HashSet<IStraightKineticTransporter> shafts = new HashSet<>();
			IKineticAcceptor acc = null;
			int i = 0;
			
			BlockPos check;
			while(deg > 0.0 && ++i <= 128 && this.world.isAreaLoaded(check = this.pos.offset(facing, i), check))
			{
				IBlockState s0 = this.world.getBlockState(check);
				TileEntity te = this.world.getTileEntity(check);
				IStraightKineticTransporter transporter = WorldUtil.cast(te, IStraightKineticTransporter.class);
				IKineticAcceptor acceptor = WorldUtil.cast(te, IKineticAcceptor.class);
				if(acceptor != null && acceptor.canAcceptKineticFrom(facing.getOpposite()))
				{
					deg = deg / acceptor.getWeightInKilograms();
					acc = acceptor;
					break;
				} else if(EXNCRotator.isRotateable(te))
				{
					EXNCRotator.addEffectiveRotation(te, (float) deg / -1.2F);
					break;
				}
				if(transporter == null || !transporter.canTransport(s0, facing.getOpposite()))
					break;
				deg = deg / transporter.getWeightInKilograms();
				shafts.add(transporter);
			}
			this.prevRotation = this.rotation;
			this.rotation -= deg;
			this.rotation %= 3600.0;
			if(Double.isNaN(this.rotation) || Double.isInfinite(this.rotation))
				this.rotation = 0.0;
			for(IStraightKineticTransporter shaft : shafts)
			{
				shaft.rotate(this.rotation);
				shaft.setRotation(this.rotation);
			}
			if(acc != null && acc.canAcceptKineticFrom(facing.getOpposite()))
			{
				acc.acceptKinetic(deg, facing.getOpposite());
				acc.setVisualRotation(rotation, facing.getOpposite());
			}
			this._client_frame_1 = System.currentTimeMillis();
			this._client_frame_2 = this._client_frame_1 + 500;
			
			facing = facing.getOpposite();
		}
	}
	
	@Override
	public double getWeightInKilograms()
	{
		return 1.25;
	}
	
	@Override
	public boolean canTransport(IBlockState state, EnumFacing in)
	{
		return state.getBlock() == BlocksMPC.KINETIC_SHAFT && in.getAxis() == state.getValue(BlockKineticShaft.DIR);
	}
	
	@Override
	public void rotate(double deg)
	{
		this.prevRotation = Math.max(deg - this.rotation, 0.0);
		this.rotation = deg;
		this.rotation %= 3600.0;
		this._client_frame_1 = System.currentTimeMillis();
		this._client_frame_2 = this._client_frame_1 + 50;
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		nbt.setDouble("Rotation", this.rotation);
		nbt.setByte("Module", (byte) module.ordinal());
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		this.rotation = nbt.getDouble("Rotation");
		this.module = EnumShaftModule.values()[nbt.getByte("Module") % EnumShaftModule.values().length];
	}
	
	@Override
	public void createDrop(EntityPlayer player, World world, BlockPos pos)
	{
		if(player != null && player.capabilities.isCreativeMode)
			return;
		if(module.isWaterWheel())
			WorldUtil.spawnItemStack(world, pos, EnumMultiMaterialType.WATER_WHEEL.stack());
		if(module.isWindMill())
			WorldUtil.spawnItemStack(world, pos, EnumMultiMaterialType.WIND_MILL.stack());
	}
	
	public static enum EnumShaftModule
	{
		NONE, WIND_MILL, WATER_WHEEL;
		
		public boolean empty()
		{
			return this == NONE;
		}
		
		public boolean isWaterWheel()
		{
			return this == WATER_WHEEL;
		}
		
		public boolean isWindMill()
		{
			return this == WIND_MILL;
		}
	}
}