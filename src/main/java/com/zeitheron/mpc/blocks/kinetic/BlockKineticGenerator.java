package com.zeitheron.mpc.blocks.kinetic;

import javax.annotation.Nullable;

import com.zeitheron.hammercore.api.mhb.BlockTraceable;
import com.zeitheron.hammercore.api.mhb.ICubeManager;
import com.zeitheron.hammercore.utils.EnumRotation;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.hammercore.utils.math.vec.Cuboid6;
import com.zeitheron.mpc.blocks.kinetic.tile.TileKineticGenerator;
import com.zeitheron.mpc.init.ItemsMPC;
import com.zeitheron.mpc.items.ItemMultiMaterial;

import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockKineticGenerator extends BlockTraceable implements ITileEntityProvider, ICubeManager
{
	public BlockKineticGenerator()
	{
		super(Material.ROCK);
		this.setSoundType(SoundType.METAL);
		this.setHardness(4.0f);
		this.setUnlocalizedName("kinetic_generator");
	}
	
	@Override
	@Nullable
	public TileEntity createNewTileEntity(World worldIn, int meta)
	{
		return new TileKineticGenerator();
	}
	
	@Override
	public void onBlockPlacedBy(World world, BlockPos pos, IBlockState state, EntityLivingBase ent, ItemStack stack)
	{
		int l = MathHelper.floor(ent.rotationYaw * 4.0f / 360.0f + 0.5) & 3;
		int meta = 0;
		if(l == 0)
		{
			meta = 1;
		}
		if(l == 1)
		{
			meta = 2;
		}
		if(l == 2)
		{
			meta = 0;
		}
		if(l == 3)
		{
			meta = 3;
		}
		state = this.getStateFromMeta(meta);
		world.setBlockState(pos, state);
		world.markAndNotifyBlock(pos, world.getChunkFromBlockCoords(pos), state, state, 3);
	}
	
	@Override
	public AxisAlignedBB getFullBoundingBox(IBlockAccess world, BlockPos pos, IBlockState state)
	{
		return FULL_BLOCK_AABB;
	}
	
	@Override
	public boolean rotateBlock(World w, BlockPos p, EnumFacing s)
	{
		IBlockState state;
		if(s.getAxis() == EnumFacing.Axis.Y && (state = w.getBlockState(p)).getBlock() == this)
		{
			EnumFacing facing = EnumFacing.byName(((EnumRotation) state.getValue((IProperty) EnumRotation.FACING)).getName());
			facing = facing.rotateY();
			state = state.withProperty((IProperty) EnumRotation.FACING, (Comparable) EnumRotation.valueOf(facing.name()));
			TileEntity te = w.getTileEntity(p);
			w.setBlockState(p, state, 3);
			te.validate();
			w.setTileEntity(p, te);
			return true;
		}
		return false;
	}
	
	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		return this.getDefaultState().withProperty((IProperty) EnumRotation.FACING, (Comparable) EnumRotation.values()[meta]);
	}
	
	@Override
	public int getMetaFromState(IBlockState state)
	{
		int meta = ((EnumRotation) state.getValue((IProperty) EnumRotation.FACING)).ordinal();
		return meta;
	}
	
	@Override
	protected BlockStateContainer createBlockState()
	{
		return new BlockStateContainer(this, this.getProperties());
	}
	
	public IProperty[] getProperties()
	{
		return new IProperty[] { EnumRotation.FACING };
	}
	
	@Override
	public Cuboid6[] getCuboids(World world, BlockPos pos, IBlockState state)
	{
		TileKineticGenerator gen = WorldUtil.cast((Object) world.getTileEntity(pos), TileKineticGenerator.class);
		if(gen != null && (System.currentTimeMillis() - gen.lastCuboidBaked > 2000 || System.currentTimeMillis() - gen.lastCuboidBaked < 0))
		{
			gen.lastCuboidBaked = System.currentTimeMillis();
			gen.bakeCuboids(EnumFacing.byName(((EnumRotation) state.getValue((IProperty) EnumRotation.FACING)).getName()));
		}
		return gen != null ? gen.cuboids : new Cuboid6[] {};
	}
	
	@Override
	public boolean onBoxActivated(int boxID, Cuboid6 box, World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		TileKineticGenerator gen = WorldUtil.cast((Object) worldIn.getTileEntity(pos), TileKineticGenerator.class);
		if(gen != null && boxID < gen.coils.length)
		{
			boolean coil = gen.coils[boxID];
			ItemStack stack = playerIn.getHeldItem(hand);
			if(!coil && ItemMultiMaterial.EnumMultiMaterialType.TIN_COIL.isThisMaterial(stack))
			{
				if(!playerIn.capabilities.isCreativeMode)
					stack.shrink(1);
				gen.coils[boxID] = true;
				if(!worldIn.isRemote)
					gen.sync();
				return true;
			}
			if(coil)
			{
				WorldUtil.spawnItemStack(worldIn, playerIn.posX, playerIn.posY, playerIn.posZ, ItemMultiMaterial.EnumMultiMaterialType.TIN_COIL.stack());
				gen.coils[boxID] = false;
				if(!worldIn.isRemote)
					gen.sync();
				return true;
			}
			return false;
		}
		return false;
	}
	
	@Override
	public BlockRenderLayer getBlockLayer()
	{
		return BlockRenderLayer.CUTOUT;
	}
	
	@Override
	public boolean isOpaqueCube(IBlockState p_isOpaqueCube_1_)
	{
		return false;
	}
	
	@Override
	public boolean isNormalCube(IBlockState p_isOpaqueCube_1_)
	{
		return false;
	}
	
	public boolean isFullyOpaque(IBlockState p_isFullyOpaque_1_)
	{
		return false;
	}
	
	@Override
	public boolean isFullCube(IBlockState p_isFullCube_1_)
	{
		return false;
	}
	
	@Override
	public boolean causesSuffocation(IBlockState state)
	{
		return false;
	}
}
