package com.zeitheron.mpc.blocks.kinetic;

import java.util.List;

import com.zeitheron.hammercore.utils.ChatUtil;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.mpc.blocks.BlockMachineBaseUnrotateable;
import com.zeitheron.mpc.blocks.kinetic.tile.TileManualCrank;

import net.minecraft.block.SoundType;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.util.FakePlayer;

public class BlockManualCrank extends BlockMachineBaseUnrotateable<TileManualCrank>
{
	public static final AxisAlignedBB aabb = new AxisAlignedBB(0.125, 0.0, 0.125, 0.8125, 0.875, 0.8125);
	
	public BlockManualCrank()
	{
		this.setUnlocalizedName("manual_crank");
		this.setSoundType(SoundType.WOOD);
		this.setHardness(2.0f);
		this.setHarvestLevel("axe", 0);
	}
	
	@Override
	public Class<TileManualCrank> getTileClass()
	{
		return TileManualCrank.class;
	}
	
	@Override
	public void spawnDrops(TileManualCrank tile, World w, BlockPos p, IBlockState s)
	{
	}
	
	@Override
	public EnumBlockRenderType getRenderType(IBlockState state)
	{
		return EnumBlockRenderType.INVISIBLE;
	}
	
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		if(playerIn == null || playerIn instanceof FakePlayer)
		{
			worldIn.destroyBlock(pos, true);
			List<EntityPlayerMP> mps = worldIn.getEntitiesWithinAABB(EntityPlayerMP.class, aabb.offset(pos.getX() + 0.5, pos.getY() + 0.5, pos.getZ() + 0.5).grow(10.0));
			for(EntityPlayerMP mp : mps)
				ChatUtil.sendNoSpam(mp, "It seems like you tried to automate manual crank. Well, if you want to automate kinetic energy, please look up for steam boiler, steam pipe and steam engine.");
			return false;
		}
		TileManualCrank crank = WorldUtil.cast((Object) worldIn.getTileEntity(pos), TileManualCrank.class);
		if(crank != null)
			crank.send(30.0);
		return crank != null;
	}
	
	@Override
	public boolean isOpaqueCube(IBlockState p_isOpaqueCube_1_)
	{
		return false;
	}
	
	@Override
	public boolean isNormalCube(IBlockState p_isOpaqueCube_1_)
	{
		return false;
	}
	
	public boolean isFullyOpaque(IBlockState p_isFullyOpaque_1_)
	{
		return false;
	}
	
	@Override
	public boolean isFullCube(IBlockState p_isFullCube_1_)
	{
		return false;
	}
	
	@Override
	public boolean causesSuffocation(IBlockState state)
	{
		return false;
	}
	
	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
	{
		return aabb;
	}
}