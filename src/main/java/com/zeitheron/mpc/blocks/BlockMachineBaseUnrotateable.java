/* Decompiled with CFR 0_123.
 * 
 * Could not load the following classes:
 * com.mrdimka.hammercore.common.InterItemStack
 * com.mrdimka.hammercore.common.utils.WorldUtil
 * com.mrdimka.hammercore.tile.TileSyncable net.minecraft.block.Block
 * net.minecraft.block.ITileEntityProvider net.minecraft.block.SoundType
 * net.minecraft.block.material.Material net.minecraft.block.state.IBlockState
 * net.minecraft.entity.Entity net.minecraft.entity.EntityLivingBase
 * net.minecraft.entity.item.EntityItem net.minecraft.entity.player.EntityPlayer
 * net.minecraft.inventory.IInventory net.minecraft.item.Item
 * net.minecraft.item.ItemStack net.minecraft.nbt.NBTTagCompound
 * net.minecraft.tileentity.TileEntity net.minecraft.util.EnumBlockRenderType
 * net.minecraft.util.EnumFacing net.minecraft.util.EnumHand
 * net.minecraft.util.math.BlockPos net.minecraft.util.math.MathHelper
 * net.minecraft.util.math.RayTraceResult net.minecraft.world.IBlockAccess
 * net.minecraft.world.World net.minecraft.world.chunk.Chunk
 * net.minecraftforge.fml.relauncher.Side
 * net.minecraftforge.fml.relauncher.SideOnly */
package com.zeitheron.mpc.blocks;

import java.util.ArrayList;
import java.util.List;

import com.zeitheron.hammercore.api.ITileBlock;
import com.zeitheron.hammercore.tile.TileSyncable;
import com.zeitheron.hammercore.utils.InterItemStack;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.mpc.api.tile.IClickable;
import com.zeitheron.mpc.ui.GuiHandler;

import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public abstract class BlockMachineBaseUnrotateable<T extends TileEntity> extends Block implements ITileEntityProvider, ITileBlock<T>
{
	public boolean canDropInCreative = false;
	
	public BlockMachineBaseUnrotateable()
	{
		super(Material.IRON);
		this.setSoundType(SoundType.METAL);
		this.setHardness(4.0f);
	}
	
	@Override
	public T createNewTileEntity(World arg0, int arg1)
	{
		try
		{
			return (this.getTileClass().newInstance());
		} catch(IllegalAccessException | InstantiationException e)
		{
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public abstract Class<T> getTileClass();
	
	public boolean hasGui()
	{
		return false;
	}
	
	@SideOnly(value = Side.CLIENT)
	public Object getClientGuiPart(EntityPlayer player, T tile)
	{
		return null;
	}
	
	public Object getServerGuiPart(EntityPlayer player, T tile)
	{
		return null;
	}

	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		TileEntity te = WorldUtil.cast((Object) worldIn.getTileEntity(pos), this.getTileClass());
		if(te != null && te instanceof IClickable && ((IClickable) te).onBlockActivated(worldIn, pos, state, playerIn, hand, facing, hitX, hitY, hitZ))
			return ((IClickable) te).getBlockActivatedReaction(worldIn, pos, state, playerIn, hand, facing, hitX, hitY, hitZ);
		if(!worldIn.isRemote && this.hasGui())
			GuiHandler.openGui(playerIn, GuiHandler.ID.MACHINE, worldIn, pos);
		return this.hasGui();
	}

	@Override
	public boolean rotateBlock(World w, BlockPos p, EnumFacing s)
	{
		int meta = w.getBlockState(p).getBlock().getMetaFromState(w.getBlockState(p)) + 1;
		if(meta > 3)
		{
			meta = 0;
		}
		TileEntity te = w.getTileEntity(p);
		w.setBlockState(p, this.getStateFromMeta(meta));
		if(te != null)
		{
			te.validate();
			w.setTileEntity(p, te);
			te.markDirty();
		}
		return true;
	}

	@Override
	public EnumBlockRenderType getRenderType(IBlockState s)
	{
		return EnumBlockRenderType.MODEL;
	}
	
	public void loadIntoTile(T tile, ItemStack stack, World w, BlockPos p, IBlockState s)
	{
	}
	
	public ItemStack getCustomDrop(T tile, World w, BlockPos p, IBlockState s)
	{
		int energy;
		ItemStack stack = new ItemStack(this.getItemDropped(s, RANDOM, 0));
		if(tile != null && (energy = tile.serializeNBT().getInteger("Battery")) > 0)
		{
			stack.setTagCompound(new NBTTagCompound());
			stack.getTagCompound().setInteger("EnergyStored", energy);
		}
		return stack;
	}

	@Override
	public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player)
	{
		return this.getCustomDrop((WorldUtil.cast((Object) world.getTileEntity(pos), this.getTileClass())), world, pos, state);
	}

	@Override
	public void breakBlock(World w, BlockPos p, IBlockState s)
	{
		if(!w.isRemote)
		{
			TileEntity t = w.getTileEntity(p);
			boolean inst = false;
			try
			{
				TileEntity t0 = t;
				inst = true;
			} catch(Throwable t0)
			{
				// empty catch block
			}
			if(inst)
			{
				this.spawnDrops((T) t, w, p, s);
				if(this.canDropInCreative)
				{
					ItemStack stack = this.getCustomDrop((T) t, w, p, s);
					if(!InterItemStack.isStackNull(stack))
					{
						w.spawnEntity(new EntityItem(w, p.getX() + 0.5, p.getY(), p.getZ() + 0.5, stack));
					} else
					{
						w.spawnEntity(new EntityItem(w, p.getX() + 0.5, p.getY(), p.getZ() + 0.5, new ItemStack(this)));
					}
				}
			}
			if(t instanceof IInventory)
			{
				IInventory l = (IInventory) t;
				for(int i = 0; i < l.getSizeInventory(); ++i)
				{
					if(InterItemStack.isStackNull(l.getStackInSlot(i)))
						continue;
					w.spawnEntity(new EntityItem(w, p.getX() + 0.5, p.getY(), p.getZ() + 0.5, l.getStackInSlot(i)));
				}
			}
		}
		super.breakBlock(w, p, s);
	}

	@Override
	public List<ItemStack> getDrops(IBlockAccess world, BlockPos pos, IBlockState state, int fortune)
	{
		return this.canDropInCreative ? new ArrayList<ItemStack>() : super.getDrops(world, pos, state, fortune);
	}

	@Override
	public void onBlockPlacedBy(World world, BlockPos pos, IBlockState state, EntityLivingBase ent, ItemStack stack)
	{
		int l = MathHelper.floor(ent.rotationYaw * 4.0f / 360.0f + 0.5) & 3;
		int meta = 0;
		if(l == 0)
			meta = 1;
		if(l == 1)
			meta = 2;
		if(l == 2)
			meta = 0;
		if(l == 3)
			meta = 3;
		TileEntity te = world.getTileEntity(pos);
		if(te == null)
			te = this.createNewTileEntity(world, meta);
		world.setTileEntity(pos, te);
		if(te == null)
			return;
		boolean inst = false;
		try
		{
			Object t0 = te;
			inst = true;
		} catch(Throwable t0)
		{
			// empty catch block
		}
		NBTTagCompound comp = stack.getTagCompound();
		if(inst && comp != null && !comp.hasKey("BlockEntityTag"))
			loadIntoTile(WorldUtil.cast(te, getTileClass()), stack, world, pos, state);
		state = getStateFromMeta(meta);
		world.setBlockState(pos, state);
		world.markAndNotifyBlock(pos, world.getChunkFromBlockCoords(pos), state, state, 3);
		TileSyncable s = WorldUtil.cast((Object) te, TileSyncable.class);
		if(!world.isRemote && s != null)
			s.sync();
	}
	
	public abstract void spawnDrops(T var1, World var2, BlockPos var3, IBlockState var4);
}