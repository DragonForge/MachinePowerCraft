package com.zeitheron.mpc.blocks.rubber;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.annotation.Nonnull;

import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.mpc.init.BlocksMPC;
import com.zeitheron.mpc.items.ItemMultiMaterial;

import net.minecraft.block.BlockLeaves;
import net.minecraft.block.BlockPlanks;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockRubberLeaves extends BlockLeaves
{
	public BlockRubberLeaves()
	{
		this.setUnlocalizedName("rubber_tree_leaves");
		Blocks.FIRE.setFireInfo(this, 60, 60);
	}
	
	@Override
	public BlockPlanks.EnumType getWoodType(int meta)
	{
		return BlockPlanks.EnumType.BIRCH;
	}
	
	@Override
	public List<ItemStack> onSheared(@Nonnull ItemStack item, IBlockAccess world, BlockPos pos, int fortune)
	{
		return Arrays.asList(new ItemStack[] { new ItemStack(this, 1) });
	}
	
	@Override
	public Item getItemDropped(IBlockState state, Random rand, int fortune)
	{
		return Item.getItemFromBlock(BlocksMPC.RUBBER_SAPLING);
	}
	
	@Override
	protected void dropApple(World worldIn, BlockPos pos, IBlockState state, int chance)
	{
		if(worldIn.rand.nextInt(100) < 5)
			WorldUtil.spawnItemStack(worldIn, pos, ItemMultiMaterial.EnumMultiMaterialType.RESIN.stack());
	}
	
	@Override
	protected BlockStateContainer createBlockState()
	{
		return new BlockStateContainer(this, new IProperty[] { CHECK_DECAY, DECAYABLE });
	}
	
	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		return this.getDefaultState().withProperty((IProperty) DECAYABLE, (Comparable) Boolean.valueOf((meta & 2) == 0)).withProperty((IProperty) CHECK_DECAY, (Comparable) Boolean.valueOf((meta & 4) > 0));
	}
	
	@Override
	public int getMetaFromState(IBlockState state)
	{
		int i = 0;
		if(!((Boolean) state.getValue((IProperty) DECAYABLE)).booleanValue())
		{
			i |= 2;
		}
		if(((Boolean) state.getValue((IProperty) CHECK_DECAY)).booleanValue())
		{
			i |= 4;
		}
		return i;
	}
}
