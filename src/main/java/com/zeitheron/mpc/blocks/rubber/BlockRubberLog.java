package com.zeitheron.mpc.blocks.rubber;

import java.util.Random;

import com.zeitheron.hammercore.utils.EnumRotation;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockRubberLog extends Block
{
	public static final PropertyEnum<EnumRubberState> state = PropertyEnum.create("state", EnumRubberState.class);
	
	public BlockRubberLog()
	{
		super(Material.WOOD);
		this.setUnlocalizedName("rubber_log");
		this.setSoundType(SoundType.WOOD);
		this.setHardness(1.5f);
		this.setHarvestLevel("axe", 0);
		this.setTickRandomly(true);
		Blocks.FIRE.setFireInfo(this, 30, 30);
	}
	
	@Override
	public int getMetaFromState(IBlockState state)
	{
		return (state.getValue(BlockRubberLog.state)).ordinal();
	}
	
	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		return this.getDefaultState().withProperty(state, EnumRubberState.values()[meta % 16]);
	}
	
	@Override
	protected BlockStateContainer createBlockState()
	{
		return new BlockStateContainer(this, new IProperty[] { state });
	}
	
	@Override
	public void randomTick(World worldIn, BlockPos pos, IBlockState state, Random random)
	{
		if(random.nextDouble() < 0.1 && !worldIn.isRemote)
		{
			EnumRubberState __r = (state.getValue(BlockRubberLog.state));
			EnumRubberState rubber = __r;
			if(rubber.getResinCount() < 3)
			{
				rubber = rubber.setResinCount(rubber.getResinCount() + 1);
			}
			if(rubber != __r)
				worldIn.setBlockState(pos, state.withProperty(BlockRubberLog.state, rubber), 3);
		}
	}
	
	@Override
	public void onBlockPlacedBy(World world, BlockPos pos, IBlockState state, EntityLivingBase ent, ItemStack stack)
	{
		int l = MathHelper.floor(ent.rotationYaw * 4.0f / 360.0f + 0.5) & 3;
		int meta = 0;
		if(l == 0)
		{
			meta = 1;
		}
		if(l == 1)
		{
			meta = 2;
		}
		if(l == 2)
		{
			meta = 0;
		}
		if(l == 3)
		{
			meta = 3;
		}
		state = state.withProperty(BlockRubberLog.state, EnumRubberState.fromFacing(EnumFacing.valueOf(EnumRotation.values()[meta].name())));
		world.setBlockState(pos, state);
		world.markAndNotifyBlock(pos, world.getChunkFromBlockCoords(pos), state, state, 3);
	}
	
	@Override
	public void breakBlock(World worldIn, BlockPos pos, IBlockState state)
	{
		int i = 4;
		int j = 5;
		if(worldIn.isAreaLoaded(pos.add(-5, -5, -5), pos.add(5, 5, 5)))
		{
			for(BlockPos blockpos : BlockPos.getAllInBox(pos.add(-4, -4, -4), pos.add(4, 4, 4)))
			{
				IBlockState iblockstate = worldIn.getBlockState(blockpos);
				if(!iblockstate.getBlock().isLeaves(iblockstate, worldIn, blockpos))
					continue;
				iblockstate.getBlock().beginLeavesDecay(iblockstate, worldIn, blockpos);
			}
		}
	}
	
	@Override
	public boolean canSustainLeaves(IBlockState state, IBlockAccess world, BlockPos pos)
	{
		return true;
	}
	
	@Override
	public boolean isWood(IBlockAccess world, BlockPos pos)
	{
		return true;
	}
	
	public static enum EnumRubberState implements IStringSerializable
	{
		NORTH_0, NORTH_1, NORTH_2, NORTH_3, //
		SOUTH_0, SOUTH_1, SOUTH_2, SOUTH_3, //
		EAST_0, EAST_1, EAST_2, EAST_3, //
		WEST_0, WEST_1, WEST_2, WEST_3;
		
		private EnumRubberState()
		{
		}
		
		public int getResinCount()
		{
			return Integer.parseInt("" + this.name().charAt(this.name().length() - 1));
		}
		
		public EnumRubberState setResinCount(int count)
		{
			if(count >= 0 && count <= 3)
			{
				return EnumRubberState.valueOf(this.name().substring(0, this.name().length() - 1) + count);
			}
			return this.setResinCount(0);
		}
		
		public static EnumRubberState fromFacing(EnumFacing facing)
		{
			if(facing == EnumFacing.UP || facing == EnumFacing.DOWN)
			{
				return null;
			}
			return EnumRubberState.valueOf(facing.name() + "_0");
		}
		
		public EnumFacing getFacing()
		{
			return EnumFacing.valueOf(this.name().substring(0, this.name().length() - 2));
		}
		
		@Override
		public String getName()
		{
			return this.name().toLowerCase();
		}
	}
	
}
