/* Decompiled with CFR 0_123.
 * 
 * Could not load the following classes: net.minecraft.block.Block
 * net.minecraft.block.BlockBush net.minecraft.block.IGrowable
 * net.minecraft.block.SoundType net.minecraft.block.properties.IProperty
 * net.minecraft.block.properties.PropertyInteger
 * net.minecraft.block.state.BlockStateContainer
 * net.minecraft.block.state.IBlockState net.minecraft.util.math.AxisAlignedBB
 * net.minecraft.util.math.BlockPos net.minecraft.world.IBlockAccess
 * net.minecraft.world.World */
package com.zeitheron.mpc.blocks.rubber;

import java.util.Random;

import com.zeitheron.mpc.world.features.WorldGenRubberTree;

import net.minecraft.block.BlockBush;
import net.minecraft.block.IGrowable;
import net.minecraft.block.SoundType;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyInteger;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockRubberSapling extends BlockBush implements IGrowable
{
	public static final PropertyInteger STAGE = PropertyInteger.create("stage", 0, 1);
	protected static final AxisAlignedBB SAPLING_AABB = new AxisAlignedBB(0.09999999403953552, 0.0, 0.09999999403953552, 0.8999999761581421, 1.0, 0.8999999761581421);
	
	public BlockRubberSapling()
	{
		this.setSoundType(SoundType.PLANT);
		this.setHarvestLevel(null, 0);
		this.setTickRandomly(true);
		this.setUnlocalizedName("sapling_rubber");
	}
	
	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
	{
		return SAPLING_AABB;
	}
	
	@Override
	public void updateTick(World worldIn, BlockPos pos, IBlockState state, Random rand)
	{
		if(!worldIn.isRemote)
		{
			super.updateTick(worldIn, pos, state, rand);
			if(worldIn.getLightFromNeighbors(pos.up()) >= 9 && rand.nextInt(7) == 0)
			{
				this.grow(worldIn, pos, state, rand);
			}
		}
	}
	
	public void grow(World worldIn, BlockPos pos, IBlockState state, Random rand)
	{
		if((Integer) state.getValue((IProperty) STAGE) == 0)
		{
			worldIn.setBlockState(pos, state.cycleProperty((IProperty) STAGE), 4);
		} else
		{
			this.generateTree(worldIn, pos, state, rand);
		}
	}
	
	@Override
	public boolean canGrow(World worldIn, BlockPos pos, IBlockState state, boolean isClient)
	{
		return true;
	}
	
	@Override
	public boolean canUseBonemeal(World worldIn, Random rand, BlockPos pos, IBlockState state)
	{
		return worldIn.rand.nextFloat() < 0.45;
	}
	
	@Override
	public void grow(World worldIn, Random rand, BlockPos pos, IBlockState state)
	{
		this.grow(worldIn, pos, state, rand);
	}
	
	public void generateTree(World worldIn, BlockPos pos, IBlockState state, Random rand)
	{
		worldIn.setBlockToAir(pos);
		if(!new WorldGenRubberTree().generate(worldIn, rand, pos))
		{
			worldIn.setBlockState(pos, state, 4);
		}
	}
	
	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		return this.getDefaultState().withProperty((IProperty) STAGE, (Comparable) Integer.valueOf((meta & 8) >> 3));
	}
	
	@Override
	public int getMetaFromState(IBlockState state)
	{
		return 0 | (Integer) state.getValue((IProperty) STAGE) << 3;
	}
	
	@Override
	protected BlockStateContainer createBlockState()
	{
		return new BlockStateContainer(this, new IProperty[] { STAGE });
	}
}
