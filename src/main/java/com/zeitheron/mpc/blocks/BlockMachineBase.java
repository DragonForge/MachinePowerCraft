package com.zeitheron.mpc.blocks;

import com.zeitheron.hammercore.utils.EnumRotation;
import com.zeitheron.hammercore.utils.wrench.IWrenchItem;
import com.zeitheron.mpc.blocks.energy.BlockPoweredBase;

import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public abstract class BlockMachineBase<T extends TileEntity> extends BlockMachineBaseUnrotateable<T> implements ITileEntityProvider
{
	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		return this.getDefaultState().withProperty((IProperty) EnumRotation.FACING, (Comparable) EnumRotation.values()[meta]);
	}
	
	@Override
	public int getMetaFromState(IBlockState state)
	{
		int meta = ((EnumRotation) state.getValue((IProperty) EnumRotation.FACING)).ordinal();
		return meta;
	}
	
	@Override
	protected BlockStateContainer createBlockState()
	{
		return new BlockStateContainer(this, this.getProperties());
	}
	
	public IProperty[] getProperties()
	{
		return new IProperty[] { EnumRotation.FACING };
	}
	
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		boolean isNormal;
		IWrenchItem wrench = BlockPoweredBase.hasUseableWrench(playerIn, hand);
		boolean bl = isNormal = wrench != null;
		if(isNormal && playerIn.isSneaking() && !worldIn.isRemote)
		{
			worldIn.destroyBlock(pos, true);
			return true;
		}
		if(BlockPoweredBase.hasUseableWrench(playerIn, hand) != null && facing.getAxis() == EnumFacing.Axis.Y && !worldIn.isRemote && this.rotateBlock(worldIn, pos, facing))
		{
			return true;
		}
		return super.onBlockActivated(worldIn, pos, state, playerIn, hand, facing, hitX, hitY, hitZ);
	}
	
	@Override
	public boolean rotateBlock(World w, BlockPos p, EnumFacing s)
	{
		IBlockState state;
		if(s.getAxis() == EnumFacing.Axis.Y && (state = w.getBlockState(p)).getBlock() == this)
		{
			EnumFacing facing = EnumFacing.byName(((EnumRotation) state.getValue((IProperty) EnumRotation.FACING)).getName());
			facing = facing.rotateY();
			state = state.withProperty((IProperty) EnumRotation.FACING, (Comparable) EnumRotation.valueOf(facing.name()));
			TileEntity te = w.getTileEntity(p);
			w.setBlockState(p, state, 3);
			te.validate();
			w.setTileEntity(p, te);
			return true;
		}
		return false;
	}
}
