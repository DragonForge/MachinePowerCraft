/* Decompiled with CFR 0_123.
 * 
 * Could not load the following classes: net.minecraft.block.Block
 * net.minecraft.block.SoundType net.minecraft.block.material.Material
 * net.minecraft.block.properties.IProperty
 * net.minecraft.block.properties.PropertyBool
 * net.minecraft.block.state.BlockStateContainer
 * net.minecraft.block.state.IBlockState net.minecraft.util.EnumFacing
 * net.minecraft.util.math.BlockPos net.minecraft.world.IBlockAccess */
package com.zeitheron.mpc.blocks;

import com.zeitheron.mpc.utils.PhysicsUtil;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;

public class BlastCasing extends Block
{
	public static final PropertyBool UP = PropertyBool.create("up");
	public static final PropertyBool DOWN = PropertyBool.create("down");
	public final PhysicsUtil.Material material;
	
	public BlastCasing(String type, PhysicsUtil.Material material)
	{
		super(Material.IRON);
		this.material = material;
		this.setSoundType(SoundType.METAL);
		this.setUnlocalizedName("blast_casing/" + type);
	}
	
	public double getMeltingTemp()
	{
		return this.material.getMeltableProperty().meltTemperature;
	}
	
	@Override
	public IBlockState getActualState(IBlockState state, IBlockAccess worldIn, BlockPos pos)
	{
		return state.withProperty((IProperty) DOWN, (Comparable) Boolean.valueOf(this.canConnectTo(worldIn, pos, EnumFacing.DOWN))).withProperty((IProperty) UP, (Comparable) Boolean.valueOf(this.canConnectTo(worldIn, pos, EnumFacing.UP)));
	}
	
	public boolean canConnectTo(IBlockAccess world, BlockPos pos, EnumFacing up)
	{
		return world.getBlockState(pos.offset(up)).getBlock() == this;
	}
	
	@Override
	public int getMetaFromState(IBlockState state)
	{
		return 0;
	}
	
	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		return this.getDefaultState();
	}
	
	@Override
	protected BlockStateContainer createBlockState()
	{
		return new BlockStateContainer(this, new IProperty[] { DOWN, UP });
	}
}
