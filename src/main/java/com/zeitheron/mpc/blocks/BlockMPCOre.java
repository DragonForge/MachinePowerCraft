/* Decompiled with CFR 0_123.
 * 
 * Could not load the following classes: net.minecraft.block.Block
 * net.minecraft.block.material.Material */
package com.zeitheron.mpc.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class BlockMPCOre extends Block
{
	public BlockMPCOre(String ore)
	{
		super(Material.ROCK);
		this.setHardness(2.5f);
		this.setHarvestLevel("pickaxe", 1);
		this.setUnlocalizedName(ore + "_ore");
	}
}
