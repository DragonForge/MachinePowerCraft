package com.zeitheron.mpc.blocks.energy.tile;

import com.zeitheron.hammercore.api.handlers.IHandlerProvider;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.mpc.api.tile.ISteamAcceptor;

import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumParticleTypes;

public class TileElectricalFurnace extends TileElectricalMachineBase
{
	public TileElectricalFurnace()
	{
		this.neededAmperes = 12.0;
	}
	
	@Override
	public ItemStack getOutput(ItemStack input)
	{
		return FurnaceRecipes.instance().getSmeltingResult(input);
	}
	
	@Override
	public void onItemProcessed(ItemStack itemstack)
	{
		if(itemstack.getItem() == Item.getItemFromBlock(Blocks.SPONGE) && itemstack.getMetadata() == 1)
		{
			double steamUnits = 0.3;
			TileEntity te = this.world.getTileEntity(this.pos.up());
			ISteamAcceptor acc = WorldUtil.cast((Object) te, ISteamAcceptor.class);
			IHandlerProvider handlerProvider = WorldUtil.cast(te, IHandlerProvider.class);
			if(handlerProvider != null && handlerProvider.hasHandler(EnumFacing.DOWN, ISteamAcceptor.class, new Object[0]))
			{
				acc = handlerProvider.getHandler(EnumFacing.UP.getOpposite(), ISteamAcceptor.class, new Object[0]);
			}
			if(acc != null && acc.canConnectTo(EnumFacing.DOWN))
			{
				acc.acceptSteam(steamUnits);
			} else
			{
				this.world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, this.pos.getX() + 0.5, this.pos.getY() + 1, this.pos.getZ() + 0.5, 0.0, 0.15, 0.0, new int[0]);
			}
		}
	}
	
	@Override
	public String getName()
	{
		return "Electrical Furnace";
	}
}