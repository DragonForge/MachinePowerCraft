package com.zeitheron.mpc.blocks.energy;

import java.awt.Color;
import java.util.Random;

import com.zeitheron.hammercore.HammerCore;
import com.zeitheron.mpc.blocks.energy.tile.TileElectricalCrusher;
import com.zeitheron.mpc.ui.gui.GuiElectricalCrusher;
import com.zeitheron.mpc.ui.inv.ContainerElectricalBase;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class BlockElectricalCrusher extends BlockPoweredBase<TileElectricalCrusher>
{
	public BlockElectricalCrusher()
	{
		this.setUnlocalizedName("electrical_crusher");
	}
	
	@Override
	public Class<TileElectricalCrusher> getTileClass()
	{
		return TileElectricalCrusher.class;
	}
	
	@Override
	public boolean hasGui()
	{
		return true;
	}
	
	@Override
	public Object getClientGuiPart(EntityPlayer player, TileElectricalCrusher tile)
	{
		return new GuiElectricalCrusher(player, tile);
	}
	
	@Override
	public Object getServerGuiPart(EntityPlayer player, TileElectricalCrusher tile)
	{
		return new ContainerElectricalBase(player, tile);
	}

	@Override
	public void randomDisplayTick(IBlockState stateIn, World worldIn, BlockPos pos, Random rand)
	{
		if(BlockPoweredBase.isPowered(stateIn))
		{
			boolean high = rand.nextBoolean();
			EnumFacing enumfacing = BlockPoweredBase.getFacing(stateIn);
			double d0 = pos.getX() + 0.5;
			double d1 = pos.getY() + rand.nextDouble() * 3.0 / 16.0 + (high ? 0.5625 : 0.0);
			double d2 = pos.getZ() + 0.5;
			double d4 = rand.nextDouble() * 0.6 - 0.3;
			double spread = 0.54;
			switch(enumfacing)
			{
			case WEST:
			{
				HammerCore.particleProxy.spawnZap(worldIn, new Vec3d(d0 - spread, d1, d2 + d4), new Vec3d(d0, d1 - (high ? 0.5625 : 0.0), d2), Color.CYAN.getRGB());
				break;
			}
			case EAST:
			{
				HammerCore.particleProxy.spawnZap(worldIn, new Vec3d(d0 + spread, d1, d2 + d4), new Vec3d(d0, d1 - (high ? 0.5625 : 0.0), d2), Color.CYAN.getRGB());
				break;
			}
			case NORTH:
			{
				HammerCore.particleProxy.spawnZap(worldIn, new Vec3d(d0 + d4, d1, d2 - spread), new Vec3d(d0, d1 - (high ? 0.5625 : 0.0), d2), Color.CYAN.getRGB());
				break;
			}
			case SOUTH:
			{
				HammerCore.particleProxy.spawnZap(worldIn, new Vec3d(d0 + d4, d1, d2 + spread), new Vec3d(d0, d1 - (high ? 0.5625 : 0.0), d2), Color.CYAN.getRGB());
				break;
			}
			default:
			break;
			}
		}
	}
}