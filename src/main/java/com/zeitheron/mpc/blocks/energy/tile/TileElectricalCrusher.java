package com.zeitheron.mpc.blocks.energy.tile;

import com.zeitheron.mpc.api.EnumMachineType;
import com.zeitheron.mpc.api.recipes.crusher.RecipesCrusher;

import net.minecraft.item.ItemStack;

public class TileElectricalCrusher extends TileElectricalMachineBase
{
	public TileElectricalCrusher()
	{
		this.neededAmperes = 24.0;
	}
	
	@Override
	public ItemStack getOutput(ItemStack input)
	{
		return RecipesCrusher.get(EnumMachineType.ELECTRICAL).get(input);
	}
	
	@Override
	public void onItemProcessed(ItemStack itemstack)
	{
	}
	
	@Override
	public String getName()
	{
		return "Electrical Crusher";
	}
}