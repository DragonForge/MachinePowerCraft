package com.zeitheron.mpc.blocks.energy.tile;

import com.zeitheron.mpc.api.EnumMachineType;
import com.zeitheron.mpc.api.recipes.extractor.RecipesExtractor;

import net.minecraft.item.ItemStack;

public class TileExtractor extends TileElectricalMachineBase
{
	public TileExtractor()
	{
		this.neededAmperes = 16.0;
	}
	
	@Override
	public ItemStack getOutput(ItemStack input)
	{
		return RecipesExtractor.get(EnumMachineType.ELECTRICAL).get(input);
	}
	
	@Override
	public String getName()
	{
		return "Electrical Extractor";
	}
}
