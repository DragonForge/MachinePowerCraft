package com.zeitheron.mpc.blocks.energy;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.zeitheron.hammercore.tile.TileSyncable;
import com.zeitheron.hammercore.utils.EnumRotation;
import com.zeitheron.hammercore.utils.wrench.IWrenchItem;
import com.zeitheron.mpc.blocks.BlockMachineBaseUnrotateable;

import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public abstract class BlockPoweredBase<T extends TileSyncable> extends BlockMachineBaseUnrotateable<T>
{
	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		EnumPowered powered = EnumPowered.OFF;
		if(meta >= EnumRotation.values().length)
		{
			meta -= EnumRotation.values().length;
			powered = EnumPowered.ON;
		}
		EnumRotation rot = EnumRotation.values()[meta];
		return this.getDefaultState().withProperty((IProperty) EnumRotation.FACING, (Comparable) rot).withProperty(EnumPowered.POWER, powered);
	}
	
	@Override
	public int getMetaFromState(IBlockState state)
	{
		int meta = ((EnumRotation) state.getValue((IProperty) EnumRotation.FACING)).ordinal();
		boolean powered = state.getValue(EnumPowered.POWER) == EnumPowered.ON;
		return meta * (powered ? 2 : 1);
	}
	
	@Override
	protected BlockStateContainer createBlockState()
	{
		return new BlockStateContainer(this, EnumRotation.FACING, EnumPowered.POWER);
	}
	
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		boolean isNormal;
		IWrenchItem wrench = hasUseableWrench(playerIn, hand);
		boolean bl = isNormal = wrench != null;
		if(isNormal && playerIn.isSneaking() && !worldIn.isRemote)
		{
			worldIn.destroyBlock(pos, true);
			return true;
		}
		if(hasUseableWrench(playerIn, hand) != null && facing.getAxis() == EnumFacing.Axis.Y && !worldIn.isRemote && this.rotateBlock(worldIn, pos, facing))
			return true;
		return super.onBlockActivated(worldIn, pos, state, playerIn, hand, facing, hitX, hitY, hitZ);
	}
	
	@Nullable
	public static IWrenchItem hasUseableWrench(EntityPlayer player, EnumHand hand)
	{
		ItemStack stack = player.getHeldItem(hand);
		if(!stack.isEmpty() && stack.getItem() instanceof IWrenchItem)
			return (IWrenchItem) stack.getItem();
		return null;
	}
	
	@Override
	public boolean rotateBlock(World w, BlockPos p, EnumFacing s)
	{
		IBlockState state;
		if(s.getAxis() == EnumFacing.Axis.Y && (state = w.getBlockState(p)).getBlock() == this)
		{
			EnumFacing facing = EnumFacing.byName(((EnumRotation) state.getValue((IProperty) EnumRotation.FACING)).getName());
			facing = facing.rotateY();
			state = state.withProperty((IProperty) EnumRotation.FACING, (Comparable) EnumRotation.valueOf(facing.name()));
			TileEntity te = w.getTileEntity(p);
			w.setBlockState(p, state, 3);
			te.validate();
			w.setTileEntity(p, te);
			return true;
		}
		return false;
	}
	
	@Override
	public void spawnDrops(T tile, World w, BlockPos p, IBlockState s)
	{
	}
	
	@Nonnull
	public static IBlockState setPowered(IBlockState state, boolean powered)
	{
		return state.withProperty(EnumPowered.POWER, powered ? EnumPowered.ON : EnumPowered.OFF);
	}
	
	public static boolean isPowered(IBlockState state)
	{
		return state.getValue(EnumPowered.POWER) == EnumPowered.ON;
	}
	
	public static IBlockState setFacing(IBlockState state, EnumFacing facing)
	{
		if(facing == EnumFacing.UP || facing == EnumFacing.DOWN || facing == null)
			return state;
		return state.withProperty(EnumRotation.FACING, EnumRotation.valueOf(facing.name()));
	}
	
	public static EnumFacing getFacing(IBlockState state)
	{
		return EnumFacing.valueOf(state.getValue(EnumRotation.FACING).name());
	}
	
	public boolean shouldEmitLightIfPowered()
	{
		return true;
	}
	
	@Override
	public int getLightValue(IBlockState state, IBlockAccess world, BlockPos pos)
	{
		return this.shouldEmitLightIfPowered() && BlockPoweredBase.isPowered(state) ? 7 : super.getLightValue(state, world, pos);
	}
	
	public static enum EnumPowered implements IStringSerializable
	{
		ON, OFF;
		
		public static final PropertyEnum<EnumPowered> POWER;
		
		private EnumPowered()
		{
		}
		
		@Override
		@Nonnull
		public String getName()
		{
			return this.name().toLowerCase();
		}
		
		static
		{
			POWER = PropertyEnum.create("power", EnumPowered.class);
		}
	}
}