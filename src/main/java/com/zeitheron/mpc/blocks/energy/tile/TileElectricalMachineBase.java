package com.zeitheron.mpc.blocks.energy.tile;

import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.utils.InterItemStack;
import com.zeitheron.hammercore.utils.inventory.InventoryDummy;
import com.zeitheron.mpc.api.energy.EnergyPacket;
import com.zeitheron.mpc.api.energy.IEnergyItem;
import com.zeitheron.mpc.api.energy.impl.TileAmpereReceiverBase;
import com.zeitheron.mpc.blocks.energy.BlockPoweredBase;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.ITickable;

public abstract class TileElectricalMachineBase extends TileAmpereReceiverBase implements ITickable, ISidedInventory
{
	public int smokeTicks = 0;
	public final InventoryDummy inv = new InventoryDummy(3);
	public double amperesCharge;
	public double amperesChargeMax = 100D;
	public double amperesStored, amperesStoredLast;
	public double neededAmperes = 1D;
	private final int[] slots = new int[] { 0, 1 };
	private final int[] empty = new int[0];
	
	@Override
	public void update()
	{
		amperesStoredLast = amperesStored;
		
		if(canProcess())
		{
			double acceptance = Math.min(neededAmperes - amperesStored, amperesCharge);
			acceptance = Math.min(acceptance, neededAmperes / 120);
			
			amperesCharge -= acceptance;
			amperesStored += acceptance;
			
			if(Double.isInfinite(amperesStored) || Double.isNaN(amperesStored))
				amperesStored = 0.0;
			
			if(amperesStored >= neededAmperes)
			{
				amperesStored -= neededAmperes;
				if(!world.isRemote)
					processItem();
			}
			
			if(atTickRate(5))
				sendChangesToNearby();
		}
		
		IBlockState state = world.getBlockState(pos);
		boolean isPowered = BlockPoweredBase.isPowered(state);
		if(Double.isInfinite(amperesStored) || Double.isNaN(amperesStored))
			amperesStored = 0.0;
		if(!world.isRemote)
		{
			boolean shouldGetPower = amperesStored > 0.0 && canProcess();
			if(isPowered != shouldGetPower)
			{
				world.setBlockState(pos, BlockPoweredBase.setPowered(state, shouldGetPower), 2);
				validate();
				world.setTileEntity(pos, this);
				sendChangesToNearby();
			}
		}
		if(!canProcess())
			amperesStored /= 2.0;
		if(!world.isRemote && amperesCharge + amperesStored > 0 && (world.isRainingAt(pos) || world.isRainingAt(pos.up())))
		{
			++smokeTicks;
			HCNet.spawnParticle(world, EnumParticleTypes.SMOKE_LARGE, pos.getX() + world.rand.nextFloat() * .5 + .25, pos.getY() + .9, pos.getZ() + world.rand.nextFloat() * .5 + .25, 0, -.01, 0);
			if(smokeTicks >= 80)
				explodeOnVoltageOverload(EnergyPacket.create(2.147483647E9));
		} else
			smokeTicks = 0;
	}
	
	public boolean canProcess()
	{
		if(getStackInSlot(0).isEmpty())
			return false;
		ItemStack itemstack = getOutput(getStackInSlot(0));
		if(itemstack.isEmpty())
			return false;
		ItemStack itemstack1 = getStackInSlot(1);
		if(itemstack1.isEmpty())
			return true;
		if(!itemstack1.isItemEqual(itemstack))
			return false;
		int result = itemstack1.getCount() + itemstack.getCount();
		return result <= getInventoryStackLimit() && result <= itemstack1.getMaxStackSize();
	}
	
	public void processItem()
	{
		if(canProcess())
		{
			ItemStack itemstack = getStackInSlot(0);
			ItemStack itemstack1 = getOutput(itemstack);
			ItemStack itemstack2 = getStackInSlot(1);
			if(itemstack2.isEmpty())
			{
				setInventorySlotContents(1, itemstack1.copy());
			} else if(itemstack2.getItem() == itemstack1.getItem())
			{
				itemstack2.grow(itemstack1.getCount());
			}
			onItemProcessed(itemstack);
			itemstack.shrink(1);
		}
	}
	
	public abstract ItemStack getOutput(ItemStack var1);
	
	public void onItemProcessed(ItemStack input)
	{
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		nbt.setDouble("AmperesStored", amperesStored);
		nbt.setDouble("MaxAmperes", neededAmperes);
		nbt.setDouble("Battery", amperesCharge);
		inv.writeToNBT(nbt);
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		amperesStored = nbt.getDouble("AmperesStored");
		neededAmperes = nbt.getDouble("MaxAmperes");
		amperesCharge = nbt.getDouble("Battery");
		inv.readFromNBT(nbt);
		if(Double.isInfinite(amperesStored) || Double.isNaN(amperesStored))
			amperesStored = 0.0;
	}
	
	@Override
	public boolean canConnectTo(EnumFacing facing)
	{
		return true;
	}
	
	@Override
	public void acceptPacket(EnergyPacket packet, EnumFacing from)
	{
		if(!world.isRemote)
		{
			amperesCharge += Math.min(packet.amperes, amperesChargeMax - amperesCharge);
			sendChangesToNearby();
		}
	}
	
	@Override
	public boolean canExplode(EnergyPacket packet)
	{
		return packet.getVoltsScaled(getResistance(packet)) > getMaxVoltage(packet);
	}
	
	@Override
	public void explodeOnVoltageOverload(EnergyPacket packet)
	{
		if(world.isRemote)
			return;
		world.setBlockToAir(pos);
		world.createExplosion(null, pos.getX() + 0.5, pos.getY() + 0.5, pos.getZ() + 0.5, 1.2f, world.getGameRules().getBoolean("mobGriefing"));
	}
	
	@Override
	public void damageNearby(EnergyPacket packet)
	{
	}
	
	@Override
	public double getMaxVoltage(EnergyPacket packet)
	{
		return 220.0;
	}
	
	@Override
	public double getResistance(EnergyPacket packet)
	{
		return 2.5;
	}
	
	@Override
	public int getSizeInventory()
	{
		return inv.getSizeInventory();
	}
	
	@Override
	public boolean isEmpty()
	{
		return inv.isEmpty();
	}
	
	@Override
	public ItemStack getStackInSlot(int index)
	{
		return inv.getStackInSlot(index);
	}
	
	@Override
	public ItemStack decrStackSize(int index, int count)
	{
		return inv.decrStackSize(index, count);
	}
	
	@Override
	public ItemStack removeStackFromSlot(int index)
	{
		return inv.removeStackFromSlot(index);
	}
	
	@Override
	public void setInventorySlotContents(int index, ItemStack stack)
	{
		inv.setInventorySlotContents(index, stack);
	}
	
	@Override
	public int getInventoryStackLimit()
	{
		return inv.getInventoryStackLimit();
	}
	
	@Override
	public boolean isUsableByPlayer(EntityPlayer player)
	{
		return inv.isUsableByPlayer(player, pos);
	}
	
	@Override
	public void openInventory(EntityPlayer player)
	{
	}
	
	@Override
	public void closeInventory(EntityPlayer player)
	{
	}
	
	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack)
	{
		return index == 0 && !InterItemStack.isStackNull(getOutput(stack)) || index == 2 && stack.getItem() instanceof IEnergyItem;
	}
	
	@Override
	public int getField(int id)
	{
		return 0;
	}
	
	@Override
	public void setField(int id, int value)
	{
	}
	
	@Override
	public int getFieldCount()
	{
		return 0;
	}
	
	@Override
	public void clear()
	{
		inv.clear();
	}
	
	@Override
	public boolean hasCustomName()
	{
		return false;
	}
	
	@Override
	public int[] getSlotsForFace(EnumFacing side)
	{
		return side != EnumFacing.UP ? slots : empty;
	}
	
	@Override
	public boolean canInsertItem(int index, ItemStack itemStackIn, EnumFacing direction)
	{
		return index == 0 && isItemValidForSlot(index, itemStackIn);
	}
	
	@Override
	public boolean canExtractItem(int index, ItemStack stack, EnumFacing direction)
	{
		return index == 1;
	}
}