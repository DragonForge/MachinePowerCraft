package com.zeitheron.mpc.items.blocks;

import com.zeitheron.hammercore.api.multipart.IMultipartProvider;
import com.zeitheron.hammercore.api.multipart.ItemBlockMultipartProvider;
import com.zeitheron.hammercore.api.multipart.MultipartSignature;
import com.zeitheron.mpc.items.multipart.MultipartSteamPipe;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class ItemSteamPipe extends ItemBlockMultipartProvider implements IMultipartProvider
{
	public ItemSteamPipe()
	{
		this.setUnlocalizedName("steam_pipe");
	}
	
	@Override
	public MultipartSignature createSignature(int signatureIndex, ItemStack stack, EntityPlayer player, World world, BlockPos pos, EnumFacing side, float hitX, float hitY, float hitZ)
	{
		return new MultipartSteamPipe();
	}
}