package com.zeitheron.mpc.items;

import java.security.SecureRandom;
import java.util.UUID;

import com.zeitheron.mpc.utils.GemImage;
import com.zeitheron.mpc.utils.namer.NamePump;

import net.minecraft.entity.Entity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class ItemGem extends Item
{
	public ItemGem()
	{
		this.setUnlocalizedName("gem");
	}
	
	@Override
	public String getItemStackDisplayName(ItemStack stack)
	{
		String type = "Unnamed";
		if(stack.getTagCompound() != null && stack.getTagCompound().hasKey("Name", 8))
		{
			type = stack.getTagCompound().getString("Name");
		}
		return type + " " + super.getItemStackDisplayName(stack);
	}
	
	@Override
	public void onUpdate(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected)
	{
		NBTTagCompound nbt = stack.getTagCompound();
		if(!worldIn.isRemote && (nbt == null || !nbt.hasKey("Seed", 7) || !nbt.hasKey("Icon", 11) || !nbt.hasKey("Name", 8)))
			stack.setTagCompound(this.generateRandom(UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID()).getTagCompound());
		super.onUpdate(stack, worldIn, entityIn, itemSlot, isSelected);
	}
	
	public ItemStack generateRandom(UUID u1, UUID u2, UUID u3, UUID u4)
	{
		ItemStack stack = new ItemStack(this);
		stack.setTagCompound(new NBTTagCompound());
		NBTTagCompound nbt = stack.getTagCompound();
		byte[] seed = (u1 + "-" + u2 + "-" + u3 + "-" + u4).getBytes();
		nbt.setByteArray("Seed", seed);
		NamePump pump = new NamePump(seed, 2);
		nbt.setString("Name", pump.supplyNext());
		SecureRandom rand = new SecureRandom(seed);
		int baseColor = -16777216 | rand.nextInt(256) << 16 | rand.nextInt(256) << 8 | rand.nextInt(256);
		nbt.setIntArray("Icon", new GemImage(seed, baseColor, 16).getPixelData());
		nbt.setInteger("Color", baseColor);
		return stack;
	}
}
