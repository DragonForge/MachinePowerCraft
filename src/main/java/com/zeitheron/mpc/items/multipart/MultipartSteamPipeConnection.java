package com.zeitheron.mpc.items.multipart;

import com.zeitheron.hammercore.api.multipart.MultipartSignature;
import com.zeitheron.hammercore.internal.blocks.multipart.TileMultipart;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.hammercore.utils.wrench.IWrenchItem;
import com.zeitheron.mpc.blocks.energy.BlockPoweredBase;
import com.zeitheron.mpc.init.BlocksMPC;
import com.zeitheron.mpc.init.ItemsMPC;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class MultipartSteamPipeConnection extends MultipartSignature implements ITickable
{
	private static final double FPIX = 0.25;
	private static final double TPIX = 0.75;
	public boolean removeCoreOnRemoval = true;
	public static final AxisAlignedBB AABB_DOWN = new AxisAlignedBB(0.25, 0.0, 0.25, 0.75, 0.25, 0.75);
	public static final AxisAlignedBB AABB_UP = new AxisAlignedBB(0.25, 0.75, 0.25, 0.75, 1.0, 0.75);
	public static final AxisAlignedBB AABB_NORTH = new AxisAlignedBB(0.25, 0.25, 0.0, 0.75, 0.75, 0.25);
	public static final AxisAlignedBB AABB_SOUTH = new AxisAlignedBB(0.25, 0.25, 0.75, 0.75, 0.75, 1.0);
	public static final AxisAlignedBB AABB_WEST = new AxisAlignedBB(0.0, 0.25, 0.25, 0.25, 0.75, 0.75);
	public static final AxisAlignedBB AABB_EAST = new AxisAlignedBB(0.75, 0.25, 0.25, 1.0, 0.75, 0.75);
	public static final Vec3d POS_DOWN = AABB_DOWN.getCenter();
	public static final Vec3d POS_UP = AABB_UP.getCenter();
	public static final Vec3d POS_NORTH = AABB_NORTH.getCenter();
	public static final Vec3d POS_SOUTH = AABB_SOUTH.getCenter();
	public static final Vec3d POS_WEST = AABB_WEST.getCenter();
	public static final Vec3d POS_EAST = AABB_EAST.getCenter();
	private static final Vec3d[] POS_V3D = new Vec3d[] { POS_DOWN, POS_UP, POS_NORTH, POS_SOUTH, POS_WEST, POS_EAST };
	private static final AxisAlignedBB[] VERTEX_AABB = new AxisAlignedBB[] { AABB_DOWN, AABB_UP, AABB_NORTH, AABB_SOUTH, AABB_WEST, AABB_EAST };
	public EnumFacing facing = EnumFacing.DOWN;
	
	public static Vec3d getPosition(EnumFacing face)
	{
		return POS_V3D[face.getIndex()];
	}
	
	public static AxisAlignedBB getBox(EnumFacing face)
	{
		return VERTEX_AABB[face.getIndex()];
	}
	
	@Override
	public AxisAlignedBB getBoundingBox()
	{
		return MultipartSteamPipeConnection.getBox(this.facing);
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setString("Facing", this.facing.getName());
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		this.facing = EnumFacing.byName(nbt.getString("Facing"));
	}
	
	@Override
	public void onRemoved(boolean spawnDrop)
	{
		if(this.removeCoreOnRemoval && this.owner != null && this.owner.getSignature(MultipartSteamPipe.MAIN_POS) instanceof MultipartSteamPipe)
			this.owner.removeMultipart(this.owner.getSignature(MultipartSteamPipe.MAIN_POS), spawnDrop);
	}
	
	@Override
	protected float getMultipartHardness(EntityPlayer player)
	{
		return 4.0f;
	}
	
	@Override
	public boolean onSignatureActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		MultipartSteamPipe pipe;
		IWrenchItem wrench = BlockPoweredBase.hasUseableWrench(playerIn, hand);
		TileMultipart owner = this.owner;
		if(wrench != null && playerIn.isSneaking() && (pipe = WorldUtil.cast((Object) owner.getSignature(MultipartSteamPipe.MAIN_POS), MultipartSteamPipe.class)) != null)
		{
			owner.removeMultipart(pipe, true);
			for(EnumFacing connect : EnumFacing.VALUES)
			{
				MultipartSteamPipeConnection c;
				if(!pipe.hasConnection(connect) || (c = (WorldUtil.cast((Object) owner.getSignature(MultipartSteamPipeConnection.getPosition(connect)), MultipartSteamPipeConnection.class))) == null)
					continue;
				c.removeCoreOnRemoval = false;
				owner.removeMultipart(c, false);
			}
			return true;
		}
		return false;
	}
	
	@Override
	public void update()
	{
		MultipartSignature sign = this.owner.getSignature(MultipartSteamPipe.MAIN_POS);
		if(this.world.isRemote)
		{
			return;
		}
		if(sign instanceof MultipartSteamPipe && ((MultipartSteamPipe) sign).hasConnection(this.facing))
		{
			return;
		}
		this.owner.removeMultipart(this, false);
	}
	
	@Override
	public float getHardness(EntityPlayer player)
	{
		float hardness = this.getMultipartHardness(player);
		if(hardness < 0.0f)
		{
			return 0.0f;
		}
		if(player.getHeldItemMainhand().getItem().getHarvestLevel(player.getHeldItemMainhand(), "pickaxe", player, this.getState()) < 2)
		{
			return player.getDigSpeed(this.getState(), this.pos) / hardness / 100.0f;
		}
		return player.getDigSpeed(BlocksMPC.STEAM_BOILER.getDefaultState(), this.pos) / hardness / 30.0f;
	}
	
	@Override
	public ItemStack getPickBlock(EntityPlayer player)
	{
		return new ItemStack(ItemsMPC.STEAM_PIPE);
	}
}
