package com.zeitheron.mpc.items.multipart;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zeitheron.hammercore.api.handlers.ITileHandler;
import com.zeitheron.hammercore.api.multipart.IMultipartHandlerProvider;
import com.zeitheron.hammercore.api.multipart.MultipartSignature;
import com.zeitheron.hammercore.internal.blocks.multipart.TileMultipart;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.hammercore.utils.wrench.IWrenchItem;
import com.zeitheron.mpc.api.tile.ISteamAcceptor;
import com.zeitheron.mpc.api.tile.ISteamSender;
import com.zeitheron.mpc.api.tile.SteamAPI;
import com.zeitheron.mpc.blocks.energy.BlockPoweredBase;
import com.zeitheron.mpc.init.BlocksMPC;
import com.zeitheron.mpc.init.ItemsMPC;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class MultipartSteamPipe extends MultipartSignature implements ITickable, IMultipartHandlerProvider, ISteamSender, ISteamAcceptor
{
	public static final AxisAlignedBB MAIN_AABB = Block.FULL_BLOCK_AABB.shrink(0.25);
	public static final Vec3d MAIN_POS = MAIN_AABB.getCenter();
	protected Map<EnumFacing, Boolean> CONNECTS = new HashMap<EnumFacing, Boolean>();
	public double steamStored = 0.0;
	private int ticksExisted = 0;
	protected double maxSteamStored = 1.0;
	
	@Override
	public <T extends ITileHandler> T getHandler(EnumFacing facing, Class<T> handler, Object... params)
	{
		return (T) (this.hasHandler(facing, handler, params) ? this : null);
	}
	
	@Override
	public <T extends ITileHandler> boolean hasHandler(EnumFacing facing, Class<T> handler, Object... params)
	{
		return (handler == ISteamAcceptor.class || handler == ISteamSender.class) && this.hasConnection(facing) && this.owner != null;
	}
	
	@Override
	public AxisAlignedBB getBoundingBox()
	{
		return MAIN_AABB;
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setDouble("SteamStored", this.steamStored);
		Arrays.stream(EnumFacing.values()).forEach(ef -> nbt.setBoolean("Facing" + ef, this.CONNECTS.get(ef) == Boolean.TRUE));
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		this.steamStored = nbt.getDouble("SteamStored");
		
		Arrays.stream(EnumFacing.values()).forEach(ef -> CONNECTS.put(ef, nbt.getBoolean("Facing" + ef)));
	}
	
	@Override
	public void update()
	{
		if(this.owner == null)
			return;
		try
		{
			if(!world.isRemote && this.ticksExisted % 20 == 0)
				this.updateConnections();
			this.dispenseSteam();
			this.steamStored = Math.max(0.0, Math.min(this.steamStored, this.maxSteamStored));
			if(this.steamStored < 1.0E-5)
			{
				this.steamStored = 0.0;
			}
			++this.ticksExisted;
		} catch(NullPointerException nullPointerException)
		{
			// empty catch block
		}
	}
	
	@Override
	public double acceptSteam(double amt)
	{
		if(this.steamStored > 0.75)
		{
			if(!this.world.isRemote)
			{
				this.world.createExplosion(null, this.pos.getX(), this.pos.getY(), this.pos.getZ(), 1.0f, false);
				if(this.owner != null)
					this.owner.removeMultipart(this, true);
			}
			return amt;
		}
		double canAccept = Math.min(this.maxSteamStored - this.steamStored, amt);
		this.steamStored += canAccept;
		return canAccept;
	}
	
	public void dispenseSteam()
	{
		TileEntity te;
		ISteamAcceptor acc;
		BlockPos ppos;
		steamStored = Math.max(0, steamStored);
		if(steamStored < 1.0E-5)
			steamStored = 0;
		if(steamStored == 0)
			return;
		
		List<EnumFacing> skip = new ArrayList<>();
		List<MultipartSteamPipe> equalise = new ArrayList<>();
		equalise.add(this);
		int conns = 0;
		for(EnumFacing ef : EnumFacing.VALUES)
			if(this.CONNECTS.get(ef) == Boolean.TRUE)
			{
				TileEntity te2 = world.getTileEntity(pos.offset(ef));
				ISteamAcceptor acc2 = SteamAPI.getSteamAcceptor(te2, ef.getOpposite());
				
				if(acc2 instanceof MultipartSteamPipe)
				{
					equalise.add((MultipartSteamPipe) acc2);
					skip.add(ef);
				}
				
				++conns;
			}
		
		SteamAPI.equalize(equalise);
		
		if(this.steamStored > 0.0)
			for(EnumFacing face1 : EnumFacing.VALUES)
			{
				if(skip.contains(face1))
					continue;
				boolean isConnected = this.CONNECTS.get(face1) == Boolean.TRUE;
				if(!isConnected)
					continue;
				
				TileEntity te2 = this.world.getTileEntity(this.pos.offset(face1));
				
				ISteamAcceptor acc2 = SteamAPI.getSteamAcceptor(te2, face1.getOpposite());
				
				if(acc2 == null && SteamAPI.getSteamSender(te2, face1.getOpposite()) != null)
					continue;
				
				if(acc2 != null && acc2.canConnectTo(face1.getOpposite()))
				{
					this.steamStored -= acc2.acceptSteam(this.steamStored / conns);
					continue;
				}
				
				if(!world.isRemote)
				{
					int fox = face1.getFrontOffsetX();
					int foy = face1.getFrontOffsetY();
					int foz = face1.getFrontOffsetZ();
					
					BlockPos ppos2 = this.pos;
					HCNet.spawnParticle(world, EnumParticleTypes.SMOKE_NORMAL, ppos2.getX() + 0.5 + fox * .25, ppos2.getY() + 0.5 + foy * .25, ppos2.getZ() + 0.5 + foz * .25, fox * 0.15, foy * 0.15, foz * 0.15);
					this.steamStored = 0.0;
				}
			}
	}
	
	public void updateConnections()
	{
		String prev = this.CONNECTS.toString();
		this.CONNECTS.clear();
		int conns = 0;
		EnumFacing last = null;
		for(EnumFacing face : EnumFacing.VALUES)
		{
			TileMultipart tmp = WorldUtil.cast(world.getTileEntity(pos.offset(face)), TileMultipart.class);
			
			CONNECTS.put(face, false);
			BlockPos pos = this.pos.offset(face);
			{
				TileEntity te = this.world.getTileEntity(pos);
				ISteamAcceptor acceptor = SteamAPI.getSteamAcceptor(te, face.getOpposite());
				ISteamSender sender = SteamAPI.getSteamSender(te, face.getOpposite());
				if(tmp != null && tmp.getSignature(MAIN_POS) instanceof MultipartSteamPipe)
				{
					MultipartSignature sign = tmp.getSignature(MultipartSteamPipeConnection.getPosition(face.getOpposite()));
					MultipartSignature main = tmp.getSignature(MAIN_POS);
					if(main instanceof MultipartSteamPipe && (sign == null || sign instanceof MultipartSteamPipeConnection))
					{
						CONNECTS.put(face, true);
						((MultipartSteamPipe) main).CONNECTS.put(face.getOpposite(), true);
					}
				} else if(acceptor != null && acceptor.canConnectTo(face.getOpposite()))
					this.CONNECTS.put(face, true);
				else if(sender != null && sender.canConnectTo(face.getOpposite()))
					this.CONNECTS.put(face, true);
			}
			if(this.CONNECTS.get(face) != Boolean.TRUE)
				continue;
			++conns;
			last = face;
		}
		
		if(conns == 1 && last != null)
			this.CONNECTS.put(last.getOpposite(), true);
		
		for(EnumFacing facing : EnumFacing.VALUES)
		{
			MultipartSignature sign;
			boolean connected = this.hasConnection(facing);
			
			if(connected)
			{
				if(owner.getSignature(MultipartSteamPipeConnection.getPosition(facing)) instanceof MultipartSteamPipeConnection)
					continue;
				MultipartSteamPipeConnection conn = new MultipartSteamPipeConnection();
				conn.removeCoreOnRemoval = false;
				conn.facing = facing;
				this.owner.addMultipart(conn);
				continue;
			}
			
			sign = this.owner.getSignature(MultipartSteamPipeConnection.getPosition(facing));
			
			if(sign instanceof MultipartSteamPipeConnection)
			{
				MultipartSteamPipeConnection conn = (MultipartSteamPipeConnection) sign;
				conn.removeCoreOnRemoval = false;
				owner.removeMultipart(sign, false);
			}
		}
		
		if(prev != this.CONNECTS.toString())
			this.requestSync();
	}
	
	@Override
	public boolean onSignatureActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		if(this.owner == null)
			return false;
		IWrenchItem wrench = BlockPoweredBase.hasUseableWrench(playerIn, hand);
		if(wrench != null && playerIn.isSneaking())
		{
			this.owner.removeMultipart(this, true);
			for(EnumFacing connect : EnumFacing.VALUES)
			{
				if(!this.hasConnection(connect))
					continue;
				try
				{
					MultipartSteamPipeConnection c = (WorldUtil.cast((Object) this.owner.getSignature(MultipartSteamPipeConnection.getPosition(connect)), MultipartSteamPipeConnection.class));
					if(c == null)
						continue;
					c.removeCoreOnRemoval = false;
					this.owner.removeMultipart(c, false);
				} catch(NullPointerException c)
				{
					// empty catch block
				}
			}
			return true;
		}
		return false;
	}
	
	@Override
	public boolean canConnectTo(EnumFacing facing)
	{
		return true;
	}
	
	public boolean hasConnection(EnumFacing to)
	{
		return this.CONNECTS.get(to) == Boolean.TRUE;
	}
	
	@Override
	public ItemStack getPickBlock(EntityPlayer player)
	{
		return new ItemStack(ItemsMPC.STEAM_PIPE);
	}
	
	@Override
	protected float getMultipartHardness(EntityPlayer player)
	{
		return 4.0f;
	}
	
	@Override
	public float getHardness(EntityPlayer player)
	{
		float hardness = this.getMultipartHardness(player);
		if(hardness < 0.0f)
			return 0.0f;
		if(player.getHeldItemMainhand().getItem().getHarvestLevel(player.getHeldItemMainhand(), "pickaxe", player, this.getState()) < 2)
			return player.getDigSpeed(this.getState(), this.pos) / hardness / 100.0f;
		return player.getDigSpeed(BlocksMPC.STEAM_BOILER.getDefaultState(), this.pos) / hardness / 30.0f;
	}
	
	@Override
	public void onRemoved(boolean spawnDrop)
	{
		if(spawnDrop)
		{
			WorldUtil.spawnItemStack(this.world, this.pos, new ItemStack(ItemsMPC.STEAM_PIPE));
			for(EnumFacing f : EnumFacing.VALUES)
				if(hasConnection(f))
				{
					MultipartSteamPipeConnection conn = owner == null ? null : WorldUtil.cast(owner.getSignature(MultipartSteamPipeConnection.getPosition(f)), MultipartSteamPipeConnection.class);
					if(conn != null)
						owner.removeMultipart(conn, false);
				}
		}
	}
}
