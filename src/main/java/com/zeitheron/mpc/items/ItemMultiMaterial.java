package com.zeitheron.mpc.items;

import com.zeitheron.hammercore.utils.IRegisterListener;
import com.zeitheron.hammercore.utils.InterItemStack;
import com.zeitheron.mpc.MachinePowerCraft;
import com.zeitheron.mpc.init.ItemsMPC;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.oredict.OreDictionary;

public class ItemMultiMaterial extends Item implements IRegisterListener
{
	public final EnumMultiMaterialType type;
	
	public ItemMultiMaterial(EnumMultiMaterialType type)
	{
		this.type = type;
		setUnlocalizedName(type.name().toLowerCase());
	}
	
	@Override
	public void onRegistered()
	{
		if(type.oredict == null || type.oredict.isEmpty())
			return;
		OreDictionary.registerOre(type.oredict, type.stack());
	}
	
	@Override
	public boolean onEntityItemUpdate(EntityItem entityItem)
	{
		ItemStack item = entityItem.getItem();
		if(EnumMultiMaterialType.CESIUM_DUST.isThisMaterial(item) && !entityItem.world.isRemote)
		{
			NBTTagCompound nbt = item.getTagCompound();
			if(nbt == null)
			{
				nbt = new NBTTagCompound();
				item.setTagCompound(nbt);
			}
			if(!nbt.hasKey("Mass"))
			{
				nbt.setInteger("Mass", 10);
			}
			if(entityItem.isInWater())
			{
				nbt.setInteger("Mass", nbt.getInteger("Mass") - 1);
				entityItem.world.setBlockToAir(entityItem.getPosition());
				entityItem.world.createExplosion(entityItem, entityItem.posX, entityItem.posY, entityItem.posZ, 2.0f, true);
			}
			if(nbt.getInteger("Mass") == 0)
			{
				item.shrink(1);
				nbt.setInteger("Mass", 30);
			}
		}
		return false;
	}
	
	public static enum EnumMultiMaterialType
	{
		URANIUM_INGOT("ingotUranium"), //
		REFINED_BRICK("brickRefined"), //
		PEAT("itemPeat"), //
		FLOUR("itemFlour"), //
		CESIUM_DUST("dustCesium"), //
		TIN_COIL("coilTin"), //
		RUBBER("itemRubber"), //
		RESIN("itemResin"), //
		SILICON("itemSilicon"), //
		FABRIC("itemFabric"), //
		WINDMILL_BLADE, //
		WATER_WHEEL, //
		WIND_MILL, //
		CIRCUIT_SIMPLE("circuitBasic"), //
		CIRCUIT_ADVANCED("circuitAdvanced"), //
		CIRCUIT_MICRO("circuitElite"), //
		CIRCUIT_NANO("circuitUltimate");
		
		private final String oredict;
		public final String mod;
		public final CreativeTabs tab;
		
		private EnumMultiMaterialType()
		{
			this.oredict = null;
			this.tab = MachinePowerCraft.tab;
			this.mod = "mpc";
		}
		
		private EnumMultiMaterialType(String oredict)
		{
			this.oredict = oredict;
			this.tab = MachinePowerCraft.tab;
			this.mod = "mpc";
		}
		
		private EnumMultiMaterialType(String oredict, String mod, CreativeTabs tab)
		{
			this.oredict = oredict;
			this.mod = mod;
			this.tab = tab;
		}
		
		public boolean isEqualByOredict(ItemStack stack)
		{
			int[] ids;
			if(this.oredict == null || InterItemStack.isStackNull(stack))
				return false;
			if(stack.isItemEqual(this.stack(stack.getCount())))
				return true;
			for(int id : ids = OreDictionary.getOreIDs(stack))
			{
				String dict = OreDictionary.getOreName(id);
				if(!dict.equals(this.oredict))
					continue;
				return true;
			}
			return false;
		}
		
		public boolean isThisMaterial(ItemStack stack)
		{
			return !InterItemStack.isStackNull(stack) && stack.getItem() == ItemsMPC.MULTI_MATERIALS[ordinal()];
		}
		
		public ItemStack stack()
		{
			return this.stack(1);
		}
		
		public ItemStack stack(int count)
		{
			return new ItemStack(ItemsMPC.MULTI_MATERIALS[ordinal()], count);
		}
		
		@Override
		public String toString()
		{
			return name().toLowerCase();
		}
	}
}