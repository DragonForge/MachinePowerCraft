package com.zeitheron.mpc.items.wires;

import com.zeitheron.hammercore.api.multipart.IMultipartProvider;
import com.zeitheron.hammercore.api.multipart.ItemBlockMultipartProvider;
import com.zeitheron.hammercore.api.multipart.MultipartSignature;
import com.zeitheron.hammercore.internal.blocks.multipart.TileMultipart;
import com.zeitheron.mpc.api.IWire;
import com.zeitheron.mpc.api.energy.impl.wire.MultipartAmpereWireBase;
import com.zeitheron.mpc.items.wires.multipart.MultipartTinWireInsulated;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class ItemTinWireInsulated extends ItemBlockMultipartProvider implements IMultipartProvider
{
	public ItemTinWireInsulated()
	{
		this.setUnlocalizedName("tin_wire_insulated");
	}
	
	@Override
	public boolean canPlaceInto(TileMultipart multipart, ItemStack stack, EntityPlayer player, World world, BlockPos pos, EnumFacing side, float hitX, float hitY, float hitZ)
	{
		if(!world.isBlockLoaded(pos.offset(side = side.getOpposite())) || !world.getBlockState(pos.offset(side)).isSideSolid(world, pos.offset(side), side.getOpposite()))
		{
			return false;
		}
		for(EnumFacing ef : EnumFacing.VALUES)
		{
			if(ef == side || ef.getOpposite() == side || !(multipart.getSignature(MultipartAmpereWireBase.getPosition(ef)) instanceof IWire) || multipart.getSignature(MultipartAmpereWireBase.getPosition(side)) != null)
				continue;
			return true;
		}
		return true;
	}
	
	@Override
	public MultipartSignature createSignature(int signatureIndex, ItemStack stack, EntityPlayer player, World world, BlockPos pos, EnumFacing side, float hitX, float hitY, float hitZ)
	{
		MultipartTinWireInsulated wire = new MultipartTinWireInsulated();
		wire.facing = side.getOpposite();
		return wire;
	}
}
