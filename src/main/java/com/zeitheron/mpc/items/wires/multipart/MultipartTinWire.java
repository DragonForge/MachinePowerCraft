package com.zeitheron.mpc.items.wires.multipart;

import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.mpc.api.energy.EnergyPacket;
import com.zeitheron.mpc.api.energy.SetupWireRender;
import com.zeitheron.mpc.api.energy.impl.wire.MultipartAmpereWireBase;
import com.zeitheron.mpc.init.ItemsMPC;

import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.Vec3d;

@SetupWireRender
public class MultipartTinWire extends MultipartAmpereWireBase
{
	public final ResourceLocation location = new ResourceLocation("mpc", "textures/models/tin_wire_uninsulated.png");
	
	@Override
	public boolean canExplode(EnergyPacket packet)
	{
		return packet != null && packet.amperes > this.getMaxVoltage(packet) / this.getResistance(packet);
	}
	
	@Override
	public ItemStack getWireStack()
	{
		return new ItemStack(ItemsMPC.TIN_WIRE);
	}
	
	@Override
	public void explodeOnVoltageOverload(EnergyPacket packet)
	{
		if(this.owner != null)
		{
			Vec3d pos = getPosition(facing).add(new Vec3d(owner.getPos()));
			HCNet.spawnParticle(world, EnumParticleTypes.SMOKE_LARGE, pos.x, pos.y, pos.z, 0, 0, 0);
			this.owner.removeMultipart(this, true);
		}
	}
	
	@Override
	public double getResistance(EnergyPacket packet)
	{
		return 8.53125;
	}
	
	@Override
	public double getMaxVoltage(EnergyPacket packet)
	{
		return 36.0;
	}
	
	@Override
	public boolean isInsulated()
	{
		return false;
	}
	
	@Override
	public ResourceLocation getModel()
	{
		return this.location;
	}
}