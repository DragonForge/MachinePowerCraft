package com.zeitheron.mpc.items;

import com.zeitheron.hammercore.utils.SoundUtil;
import com.zeitheron.mpc.api.TreetapUser;
import com.zeitheron.mpc.blocks.rubber.BlockRubberLog;
import com.zeitheron.mpc.init.BlocksMPC;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;

public class ItemTreeTap extends Item
{
	public ItemTreeTap()
	{
		this.setUnlocalizedName("tree_tap");
		this.setMaxStackSize(1);
		this.setMaxDamage(20);
	}
	
	@Override
	public int getRGBDurabilityForDisplay(ItemStack stack)
	{
		float damage = Math.max(0.0f, (float) (stack.getMaxDamage() - stack.getItemDamage()) / (float) stack.getMaxDamage());
		int color = 16765007;
		int target = (int) (damage * 255.0f);
		target = target << 16 | target << 8 | target;
		return MathHelper.multiplyColor(target, color);
	}
	
	@Override
	public EnumActionResult onItemUse(EntityPlayer player, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		EnumActionResult res = TreetapUser.tryUse(player, worldIn, pos, hand, facing, hitX, hitY, hitZ);
		if(res == EnumActionResult.SUCCESS)
		{
			player.getHeldItem(hand).damageItem(1, player);
			if(!worldIn.isRemote)
				SoundUtil.playSoundEffect(worldIn, "entity.item.pickup", player.posX, player.posY, player.posZ, 0.1f, 0.0f, SoundCategory.PLAYERS);
		}
		return res;
	}
	
	static
	{
		new TreetapUser()
		{
			@Override
			public boolean canUse(EntityPlayer player, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
			{
				IBlockState state = worldIn.getBlockState(pos);
				if(state.getBlock() == BlocksMPC.RUBBER_LOG)
				{
					BlockRubberLog.EnumRubberState rubber = state.getValue(BlockRubberLog.state);
					return rubber.getFacing() == facing && rubber.getResinCount() > 0;
				}
				return false;
			}
			
			@Override
			public EnumActionResult use(EntityPlayer player, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
			{
				BlockRubberLog.EnumRubberState rubber;
				IBlockState state = worldIn.getBlockState(pos);
				if(state.getBlock() == BlocksMPC.RUBBER_LOG && (rubber = state.getValue(BlockRubberLog.state)).getFacing() == facing && rubber.getResinCount() > 0)
				{
					state = state.withProperty(BlockRubberLog.state, rubber.setResinCount(rubber.getResinCount() - 1));
					worldIn.setBlockState(pos, state, 3);
					ItemStack resin = ItemMultiMaterial.EnumMultiMaterialType.RESIN.stack();
					if(!worldIn.isRemote && !player.inventory.addItemStackToInventory(resin))
						player.dropItem(resin, false, false);
					return EnumActionResult.SUCCESS;
				}
				return EnumActionResult.PASS;
			}
		};
	}
}