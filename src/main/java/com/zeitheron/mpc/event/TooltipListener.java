package com.zeitheron.mpc.event;

import java.util.List;

import com.zeitheron.mpc.MachinePowerCraft;
import com.zeitheron.mpc.matreg.MaterialRegistry;
import com.zeitheron.mpc.utils.PhysicsUtil;

import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class TooltipListener
{
	@SubscribeEvent(priority = EventPriority.HIGHEST)
	public void addTooltip(ItemTooltipEvent evt)
	{
		List tip = evt.getToolTip();
		ItemStack stack = evt.getItemStack();
		PhysicsUtil.Material mat = MaterialRegistry.getMaterial(stack);
		if(mat != null)
		{
			if(MachinePowerCraft.proxy.isShiftKeyDown_forTooltip())
			{
				mat.addTooltip(tip, stack);
			} else
			{
				tip.add(TextFormatting.GRAY + "[Press " + TextFormatting.YELLOW + "SHIFT" + TextFormatting.GRAY + " for Material Details]" + TextFormatting.RESET);
			}
		}
	}
}
