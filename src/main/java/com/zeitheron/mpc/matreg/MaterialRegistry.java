package com.zeitheron.mpc.matreg;

import java.util.HashMap;
import java.util.Map;

import com.zeitheron.hammercore.utils.match.item.ItemContainer;
import com.zeitheron.hammercore.utils.match.item.ItemMatchParams;
import com.zeitheron.mpc.utils.PhysicsUtil;

import net.minecraft.item.ItemStack;

public class MaterialRegistry
{
	private static final Map<ItemContainer, PhysicsUtil.Material> mats = new HashMap<ItemContainer, PhysicsUtil.Material>();
	private static final ItemMatchParams pars = new ItemMatchParams().setUseOredict(true).setUseDamage(true);
	
	public static void registerMaterial(ItemStack item, PhysicsUtil.Material mat)
	{
		if(item.isEmpty())
			return;
		mats.put(ItemContainer.create(item.copy()), mat);
	}
	
	public static void registerMaterial(String od, PhysicsUtil.Material mat)
	{
		mats.put(ItemContainer.create(od), mat);
	}
	
	public static PhysicsUtil.Material getMaterial(ItemStack stack)
	{
		if(stack.isEmpty())
			return null;
		ItemContainer cc = ItemContainer.create(stack);
		for(ItemContainer c : mats.keySet())
		{
			if(!c.matches(cc, pars.setUseOredict(false)))
				continue;
			return mats.get(c);
		}
		return null;
	}
	
	public static PhysicsUtil.Material getMaterial(String od)
	{
		if(od == null || od.isEmpty())
		{
			return null;
		}
		ItemContainer cc = ItemContainer.create(od);
		for(ItemContainer c : mats.keySet())
		{
			if(!c.matches(cc, pars))
				continue;
			return mats.get(c);
		}
		return null;
	}
}
