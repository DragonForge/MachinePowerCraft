package com.zeitheron.mpc.init;

import com.zeitheron.hammercore.utils.recipes.helper.RecipeRegistry;
import com.zeitheron.hammercore.utils.recipes.helper.RegisterRecipes;
import com.zeitheron.hammermetals.api.MetalRegistry;
import com.zeitheron.hammermetals.api.parts.EnumBlockMetalPart;
import com.zeitheron.mpc.items.ItemMultiMaterial;
import com.zeitheron.mpc.items.ItemMultiMaterial.EnumMultiMaterialType;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraftforge.oredict.OreDictionary;

@RegisterRecipes(modid = "mpc")
public class RecipesMPC extends RecipeRegistry
{
	@Override
	public void smelting()
	{
		OreDictionary.registerOre("fenceWood", Blocks.OAK_FENCE);
		OreDictionary.registerOre("fenceWood", Blocks.BIRCH_FENCE);
		OreDictionary.registerOre("fenceWood", Blocks.SPRUCE_FENCE);
		OreDictionary.registerOre("fenceWood", Blocks.JUNGLE_FENCE);
		OreDictionary.registerOre("fenceWood", Blocks.ACACIA_FENCE);
		OreDictionary.registerOre("fenceWood", Blocks.DARK_OAK_FENCE);
		
		smelting(Items.BRICK, ItemMultiMaterial.EnumMultiMaterialType.REFINED_BRICK.stack(), 0.0f);
		smelting(ItemMultiMaterial.EnumMultiMaterialType.RESIN.stack(), ItemMultiMaterial.EnumMultiMaterialType.RUBBER.stack(), 0.0f);
		smelting(ItemMultiMaterial.EnumMultiMaterialType.FLOUR.stack(), new ItemStack(Items.BREAD), 0.0f);
	}
	
	@Override
	public void crafting()
	{
		shaped(ItemsMPC.WRENCH, " ib", " si", "s  ", 's', "stickWood", 'i', "ingotIron", 'b', "plateBrass");
		shaped(new ItemStack(ItemsMPC.STEAM_PIPE, 3), "p p", "p p", "p p", 'p', "plateBrass");
		shaped(new ItemStack(BlocksMPC.STEAM_BOILER), "ppp", "prp", "bbb", 'p', "plateBrass", 'b', "brickRefined", 'r', "blockCoal");
		shaped(new ItemStack(BlocksMPC.STEAM_GRINDER), "pb ", "pb ", "pb ", 'p', "plateBrass", 'b', Blocks.IRON_BARS);
		shaped(new ItemStack(BlocksMPC.STEAM_OVEN), "ppp", "pfp", "bbb", 'p', "plateBrass", 'f', Blocks.FURNACE, 'b', "brickRefined");
		shaped(new ItemStack(BlocksMPC.STEAM_ENGINE), "psp", "pnp", "ptp", 'p', "plateBrass", 's', BlocksMPC.KINETIC_SHAFT, 'n', Blocks.PISTON, 't', ItemsMPC.STEAM_PIPE);
		shaped(new ItemStack(BlocksMPC.KINETIC_SHAFT, 2), "i i", "iri", "i i", 'i', "ingotIron", 'r', "dustRedstone");
		shaped(new ItemStack(BlocksMPC.MECHANICAL_CRUSHER), "pfp", "fpf", "rbr", 'r', "brickRefined", 'p', "plateBrass", 'b', BlocksMPC.KINETIC_SHAFT, 'f', Items.FLINT);
		shaped(new ItemStack(BlocksMPC.MECHANICAL_COMPRESSOR), "ppp", "psp", "rbr", 'r', "brickRefined", 'p', "plateBrass", 'b', BlocksMPC.KINETIC_SHAFT, 's', Blocks.PISTON);
		shaped(new ItemStack(BlocksMPC.MECHANICAL_LEVER), "psp", "sbs", "psp", 'b', "plateBrass", 's', BlocksMPC.KINETIC_SHAFT, 'p', "plateIron");
		shaped(BlocksMPC.MANUAL_CRANK, "iss", "s  ", "s  ", 's', "stickWood", 'i', "ingotCopper");
		shaped(new ItemStack(ItemsMPC.TIN_WIRE, 8), " t ", "ttt", " t ", 't', "ingotTin");
		shaped(ItemMultiMaterial.EnumMultiMaterialType.TIN_COIL.stack(), " t ", "trt", " t ", 't', ItemsMPC.TIN_WIRE, 'r', "dustRedstone");
		shaped(BlocksMPC.KINETIC_GENERATOR, "bcb", "bsb", "bbb", 'b', "brickRefined", 'c', "coilTin", 's', BlocksMPC.KINETIC_SHAFT);
		
		Block ironCasing = MetalRegistry.getMetal("iron").get(EnumBlockMetalPart.CASING);
		
		shaped(BlocksMPC.ELECTRICAL_FURNACE, "pfp", "tct", "pwp", 'c', ironCasing, 'w', ItemsMPC.TIN_WIRE, 't', "coilTin", 'f', Blocks.FURNACE, 'p', "plateCopper");
		shaped(BlocksMPC.EXTRACTOR, "rrr", "tct", "pwp", 'c', ironCasing, 'w', ItemsMPC.TIN_WIRE, 't', "coilTin", 'r', ItemsMPC.TREE_TAP, 'p', "plateTin");
		shaped(BlocksMPC.ELECTRICAL_CRUSHER, "fff", "tct", "pwp", 'c', ironCasing, 'w', ItemsMPC.TIN_WIRE, 't', "coilTin", 'f', Items.FLINT, 'p', "plateZinc");
		
		shaped(ItemsMPC.TREE_TAP, " r ", "ppp", "  p", 'p', "plankWood", 'r', "itemResin");
		shaped(EnumMultiMaterialType.WATER_WHEEL.stack(), "sps", "pgp", "sps", 's', "stickWood", 'p', "plankWood", 'g', "gearCopper");
		shaped(EnumMultiMaterialType.FABRIC.stack(), "scs", "csc", "scs", 'c', new ItemStack(Blocks.CARPET, 1, OreDictionary.WILDCARD_VALUE), 's', "string");
		shaped(EnumMultiMaterialType.WINDMILL_BLADE.stack(), "fc", "fc", "f ", 'f', "fenceWood", 'c', "itemFabric");
		shaped(EnumMultiMaterialType.WIND_MILL.stack(), " b ", "bgb", " b ", 'b', EnumMultiMaterialType.WINDMILL_BLADE.stack(), 'g', "gearIron");
		
		shapeless(new ItemStack(ItemsMPC.TIN_WIRE_INSULATED), new ItemStack(ItemsMPC.TIN_WIRE), "itemRubber");
	}
	
	public static boolean hasOres(String name)
	{
		NonNullList<ItemStack> stacks = OreDictionary.getOres(name);
		return stacks != null && !stacks.isEmpty();
	}
}