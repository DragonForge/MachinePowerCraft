package com.zeitheron.mpc.init;

import com.zeitheron.hammercore.bookAPI.fancy.ManualCategories;
import com.zeitheron.hammercore.bookAPI.fancy.ManualEntry;
import com.zeitheron.hammercore.bookAPI.fancy.ManualEntry.eEntryShape;
import com.zeitheron.hammercore.bookAPI.fancy.ManualPage;
import com.zeitheron.hammercore.bookAPI.fancy.ManualPage.PageType;
import com.zeitheron.mpc.items.ItemMultiMaterial.EnumMultiMaterialType;

import net.minecraft.client.resources.I18n;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class ManualMPC
{
	public static void register()
	{
		ManualCategories.registerCategory("mpc", new ResourceLocation("mpc", "textures/items/wind_mill.png"), new ResourceLocation("mpc", "textures/gui/manual_back.jpg"));
		
		new ME("steam_pipe", -1, -3, new ItemStack(ItemsMPC.STEAM_PIPE)).setPages(text("steam_pipe"), crafting(new ItemStack(ItemsMPC.STEAM_PIPE))).registerEntry();
		new ME("steam_oven", -3, -3, new ItemStack(BlocksMPC.STEAM_OVEN)).setPages(text("steam_oven"), crafting(new ItemStack(BlocksMPC.STEAM_OVEN))).setParents("steam_pipe").registerEntry();
		new ME("steam_grinder", -2, -4, new ItemStack(BlocksMPC.STEAM_GRINDER)).setPages(text("steam_grinder"), crafting(new ItemStack(BlocksMPC.STEAM_GRINDER))).setParents("steam_pipe").registerEntry();
		new ME("steam_boiler", 0, -4, new ItemStack(BlocksMPC.STEAM_BOILER)).setPages(text("steam_boiler"), crafting(new ItemStack(BlocksMPC.STEAM_BOILER))).setParents("steam_pipe").setShape(eEntryShape.ROUND).registerEntry();
		new ME("steam_engine", 1, -3, new ItemStack(BlocksMPC.STEAM_ENGINE)).setPages(text("steam_engine"), crafting(new ItemStack(BlocksMPC.STEAM_ENGINE))).setParents("steam_pipe").setShape(eEntryShape.ROUND).registerEntry();
		new ME("manual_crank", 3, -3, new ItemStack(BlocksMPC.MANUAL_CRANK)).setPages(text("manual_crank"), crafting(new ItemStack(BlocksMPC.MANUAL_CRANK))).registerEntry();
		new ME("kinetic_shaft", 2, -2, new ItemStack(BlocksMPC.KINETIC_SHAFT)).setPages(text("kinetic_shaft"), crafting(new ItemStack(BlocksMPC.KINETIC_SHAFT))).setParents("steam_engine", "manual_crank").setShape(eEntryShape.HEX).registerEntry();
		new ME("mechanical_lever", 2, -4, new ItemStack(BlocksMPC.MECHANICAL_LEVER)).setPages(text("mechanical_lever"), crafting(new ItemStack(BlocksMPC.MECHANICAL_LEVER))).setParents("kinetic_shaft").registerEntry();
		new ME("mechanical_crusher", 1, -1, new ItemStack(BlocksMPC.MECHANICAL_CRUSHER)).setPages(text("mechanical_crusher"), crafting(new ItemStack(BlocksMPC.MECHANICAL_CRUSHER))).setParents("kinetic_shaft").registerEntry();
		new ME("mechanical_compressor", 3, -1, new ItemStack(BlocksMPC.MECHANICAL_COMPRESSOR)).setPages(text("mechanical_compressor"), crafting(new ItemStack(BlocksMPC.MECHANICAL_COMPRESSOR))).setParents("kinetic_shaft").registerEntry();
		new ME("kinetic_generator", 2, 0, new ItemStack(BlocksMPC.KINETIC_GENERATOR)).setPages(text("kinetic_generator"), crafting(new ItemStack(BlocksMPC.KINETIC_GENERATOR))).setParents("kinetic_shaft").setShape(eEntryShape.HEX).registerEntry();
		new ME("wire", 1, 1, new ItemStack(ItemsMPC.TIN_WIRE)).setPages(text("wire"), crafting(new ItemStack(ItemsMPC.TIN_WIRE))).setParents("kinetic_generator").setShape(eEntryShape.ROUND).registerEntry();
		new ME("rubber_stuff", -3, 1, new ItemStack(BlocksMPC.RUBBER_SAPLING)).setPages(text("rubber.1"), text("rubber.2"), crafting(new ItemStack(ItemsMPC.TREE_TAP)), smelting(EnumMultiMaterialType.RESIN.stack())).registerEntry();
		new ME("wire_insulated", -1, 1, new ItemStack(ItemsMPC.TIN_WIRE_INSULATED)).setPages(text("wire_insulated"), crafting(new ItemStack(ItemsMPC.TIN_WIRE_INSULATED))).setParents("rubber_stuff", "wire").registerEntry();
		new ME("electrical_furnace", 0, 3, new ItemStack(BlocksMPC.ELECTRICAL_FURNACE)).setPages(text("electrical_furnace"), crafting(new ItemStack(BlocksMPC.ELECTRICAL_FURNACE))).setParents("wire").registerEntry();
		new ME("electrical_crusher", 1, 3, new ItemStack(BlocksMPC.ELECTRICAL_CRUSHER)).setPages(text("electrical_crusher"), crafting(new ItemStack(BlocksMPC.ELECTRICAL_CRUSHER))).setParents("wire").registerEntry();
		new ME("electrical_extractor", 2, 3, new ItemStack(BlocksMPC.EXTRACTOR)).setPages(text("electrical_extractor"), crafting(new ItemStack(BlocksMPC.EXTRACTOR))).setParents("wire").registerEntry();
	}
	
	private static ManualPage smelting(ItemStack in)
	{
		return new ManualPage(PageType.SMELTING, in);
	}
	
	private static ManualPage crafting(ItemStack out)
	{
		return new ManualPage(PageType.NORMAL_CRAFTING, out);
	}
	
	private static ManualPage text(String txt)
	{
		return new ManualPage("mpc.manual." + txt + ".desc");
	}
	
	private static class ME extends ManualEntry
	{
		public ME(String key, int col, int row, ItemStack icon)
		{
			super("mpc:" + key, "mpc", col, row, icon);
		}
		
		@Override
		public String getName()
		{
			return I18n.format("mpc.manual." + key.substring(4) + ".name");
		}
		
		@Override
		public String getText()
		{
			return I18n.format("mpc.manual." + key.substring(4) + ".text");
		}
		
		@Override
		public ManualEntry setParents(String... par)
		{
			for(int i = 0; i < par.length; ++i)
				par[i] = "mpc:" + par[i];
			return super.setParents(par);
		}
	}
}