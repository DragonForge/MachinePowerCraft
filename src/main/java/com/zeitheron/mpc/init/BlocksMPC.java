package com.zeitheron.mpc.init;

import com.zeitheron.mpc.blocks.BlastCasing;
import com.zeitheron.mpc.blocks.BlockPeat;
import com.zeitheron.mpc.blocks.energy.BlockElectricalCrusher;
import com.zeitheron.mpc.blocks.energy.BlockElectricalFurnace;
import com.zeitheron.mpc.blocks.energy.BlockExtractor;
import com.zeitheron.mpc.blocks.kinetic.BlockKineticGenerator;
import com.zeitheron.mpc.blocks.kinetic.BlockKineticShaft;
import com.zeitheron.mpc.blocks.kinetic.BlockManualCrank;
import com.zeitheron.mpc.blocks.kinetic.BlockMechanicalCompressor;
import com.zeitheron.mpc.blocks.kinetic.BlockMechanicalCrusher;
import com.zeitheron.mpc.blocks.kinetic.BlockMechanicalLever;
import com.zeitheron.mpc.blocks.rubber.BlockRubberLeaves;
import com.zeitheron.mpc.blocks.rubber.BlockRubberLog;
import com.zeitheron.mpc.blocks.rubber.BlockRubberSapling;
import com.zeitheron.mpc.blocks.steam.BlockSteamBoiler;
import com.zeitheron.mpc.blocks.steam.BlockSteamEngine;
import com.zeitheron.mpc.blocks.steam.BlockSteamGrinder;
import com.zeitheron.mpc.blocks.steam.BlockSteamOven;
import com.zeitheron.mpc.utils.PhysicsUtil;

import net.minecraft.block.Block;

public class BlocksMPC
{
	public static final Block STEAM_BOILER = new BlockSteamBoiler();
	public static final Block PEAT_BLOCK = new BlockPeat();
	public static final Block STEAM_OVEN = new BlockSteamOven();
	public static final Block STEAM_ENGINE = new BlockSteamEngine();
	public static final Block STEAM_GRINDER = new BlockSteamGrinder();
	public static final Block KINETIC_SHAFT = new BlockKineticShaft();
	public static final Block MECHANICAL_CRUSHER = new BlockMechanicalCrusher();
	public static final Block MECHANICAL_COMPRESSOR = new BlockMechanicalCompressor();
	public static final Block MECHANICAL_LEVER = new BlockMechanicalLever();
	public static final Block MANUAL_CRANK = new BlockManualCrank();
	public static final Block KINETIC_GENERATOR = new BlockKineticGenerator();
	public static final Block ELECTRICAL_FURNACE = new BlockElectricalFurnace();
	public static final Block EXTRACTOR = new BlockExtractor();
	public static final Block ELECTRICAL_CRUSHER = new BlockElectricalCrusher();
	public static final Block BLAST_CASING_IRON = new BlastCasing("iron", PhysicsUtil.Material.IRON);
	public static final Block RUBBER_LOG = new BlockRubberLog();
	public static final Block RUBBER_LEAVES = new BlockRubberLeaves();
	public static final Block RUBBER_SAPLING = new BlockRubberSapling();
}