package com.zeitheron.mpc.init;

import com.zeitheron.hammercore.utils.match.item.ItemContainer;
import com.zeitheron.mpc.api.armor.ElectrialConductorArmorRegistry;
import com.zeitheron.mpc.items.ItemMultiMaterial;
import com.zeitheron.mpc.matreg.MaterialRegistry;
import com.zeitheron.mpc.utils.PhysicsUtil;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

public class MaterialsMPC
{
	static
	{
		MaterialRegistry.registerMaterial(new ItemStack(Items.COAL, 1, 1), PhysicsUtil.Material.CHARCOAL);
		MaterialRegistry.registerMaterial(new ItemStack(Items.COAL), PhysicsUtil.Material.ANTHRACITE_COAL);
		MaterialRegistry.registerMaterial(new ItemStack(Blocks.LOG, 1, 0), PhysicsUtil.Material.DRY_OAK_WOOD);
		MaterialRegistry.registerMaterial(ItemMultiMaterial.EnumMultiMaterialType.PEAT.stack(), PhysicsUtil.Material.PEAT);
		MaterialRegistry.registerMaterial(new ItemStack(Items.IRON_INGOT), PhysicsUtil.Material.IRON);
		ElectrialConductorArmorRegistry.addEntry(ItemContainer.create(new ItemStack(Items.IRON_HELMET)), 1.2f, 1.5f);
		ElectrialConductorArmorRegistry.addEntry(ItemContainer.create(new ItemStack(Items.IRON_CHESTPLATE)), 1.5f, 2.0f);
		ElectrialConductorArmorRegistry.addEntry(ItemContainer.create(new ItemStack(Items.IRON_LEGGINGS)), 1.3f, 1.75f);
		ElectrialConductorArmorRegistry.addEntry(ItemContainer.create(new ItemStack(Items.IRON_BOOTS)), 1.15f, 1.75f);
		ElectrialConductorArmorRegistry.addEntry(ItemContainer.create(new ItemStack(Items.GOLDEN_HELMET)), 2.2f, 3.5f);
		ElectrialConductorArmorRegistry.addEntry(ItemContainer.create(new ItemStack(Items.GOLDEN_CHESTPLATE)), 2.5f, 4.0f);
		ElectrialConductorArmorRegistry.addEntry(ItemContainer.create(new ItemStack(Items.GOLDEN_LEGGINGS)), 2.3f, 3.75f);
		ElectrialConductorArmorRegistry.addEntry(ItemContainer.create(new ItemStack(Items.GOLDEN_BOOTS)), 2.15f, 3.75f);
		ElectrialConductorArmorRegistry.addEntry(ItemContainer.create(new ItemStack(Items.LEATHER_HELMET)), 0.8f, 0.0f);
		ElectrialConductorArmorRegistry.addEntry(ItemContainer.create(new ItemStack(Items.LEATHER_CHESTPLATE)), 0.6f, 0.0f);
		ElectrialConductorArmorRegistry.addEntry(ItemContainer.create(new ItemStack(Items.LEATHER_LEGGINGS)), 0.7f, 0.0f);
		ElectrialConductorArmorRegistry.addEntry(ItemContainer.create(new ItemStack(Items.LEATHER_BOOTS)), 0.9f, 0.0f);
	}
}
