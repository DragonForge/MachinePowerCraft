package com.zeitheron.mpc.init;

import com.zeitheron.mpc.api.EnumMachineType;
import com.zeitheron.mpc.api.recipes.compressor.RecipesCompressor;

import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraftforge.oredict.OreDictionary;

public class CompressorMPC
{
	static
	{
		for(String o : OreDictionary.getOreNames())
		{
			if(!o.startsWith("ingot"))
				continue;
			String mat = o.substring(5);
			String dust = "plate" + mat;
			NonNullList<ItemStack> dusts = OreDictionary.getOres(dust);
			NonNullList<ItemStack> ores = OreDictionary.getOres(o);
			if(dusts.size() <= 0 || ores.size() <= 0)
				continue;
			ItemStack copy0 = ores.get(0).copy();
			ItemStack copy1 = dusts.get(0).copy();
			
			RecipesCompressor.get(EnumMachineType.MECHANICAL).add(copy0, copy1);
		}
	}
}
