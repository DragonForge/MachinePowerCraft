package com.zeitheron.mpc.init;

import java.util.ArrayList;
import java.util.List;

import com.zeitheron.hammermetals.items.ItemHammer;
import com.zeitheron.mpc.items.ItemMultiMaterial;
import com.zeitheron.mpc.items.ItemMultiMaterial.EnumMultiMaterialType;
import com.zeitheron.mpc.items.ItemTreeTap;
import com.zeitheron.mpc.items.ItemWrench;
import com.zeitheron.mpc.items.blocks.ItemSteamPipe;
import com.zeitheron.mpc.items.wires.ItemTinWire;
import com.zeitheron.mpc.items.wires.ItemTinWireInsulated;

import net.minecraft.item.Item;

public class ItemsMPC
{
	public static final List<ItemMultiMaterial> MATERIAL_LIST = new ArrayList<>();
	public static final ItemMultiMaterial[] MULTI_MATERIALS = new ItemMultiMaterial[EnumMultiMaterialType.values().length];
	
	public static void initMaterials()
	{
		for(int i = 0; i < EnumMultiMaterialType.values().length; ++i)
			MATERIAL_LIST.add(null);
		for(EnumMultiMaterialType type : EnumMultiMaterialType.values())
			MATERIAL_LIST.set(type.ordinal(), MULTI_MATERIALS[type.ordinal()] = new ItemMultiMaterial(type));
	}
	
	public static final Item WRENCH = new ItemWrench();
	public static final Item TIN_WIRE = new ItemTinWire();
	public static final Item TIN_WIRE_INSULATED = new ItemTinWireInsulated();
	public static final Item TREE_TAP = new ItemTreeTap();
	public static final Item STEAM_PIPE = new ItemSteamPipe();
	// public static final Item GEM = new ItemGem();
}
