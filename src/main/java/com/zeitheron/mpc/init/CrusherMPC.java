package com.zeitheron.mpc.init;

import com.zeitheron.mpc.api.EnumMachineType;
import com.zeitheron.mpc.api.recipes.crusher.RecipesCrusher;
import com.zeitheron.mpc.items.ItemMultiMaterial;
import com.zeitheron.mpc.items.ItemMultiMaterial.EnumMultiMaterialType;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.oredict.OreDictionary;

public class CrusherMPC
{
	static
	{
		RecipesCrusher.get(EnumMachineType.MECHANICAL).add(new ItemStack(Items.WHEAT), ItemMultiMaterial.EnumMultiMaterialType.FLOUR.stack());
		RecipesCrusher.get(EnumMachineType.MECHANICAL).add(new ItemStack(Blocks.REDSTONE_ORE), new ItemStack(Items.REDSTONE, 15));
		RecipesCrusher.get(EnumMachineType.MECHANICAL).add(new ItemStack(Blocks.LAPIS_ORE), new ItemStack(Items.DYE, 15, 4));
		
		for(String o : OreDictionary.getOreNames())
		{
			String dust;
			String mat;
			if(o.startsWith("ore"))
			{
				ItemStack copy1;
				ItemStack copy0;
				mat = o.substring(3);
				if(mat.equalsIgnoreCase("redstone") || mat.equalsIgnoreCase("lapis"))
					continue;
				dust = "dust" + mat;
				String gem = "gem" + mat;
				NonNullList<ItemStack> dusts = OreDictionary.getOres(dust);
				NonNullList<ItemStack> ores = OreDictionary.getOres(o);
				NonNullList<ItemStack> gems = OreDictionary.getOres(gem);
				if(dusts.size() > 0 && ores.size() > 0)
				{
					copy0 = ores.get(0).copy();
					copy1 = dusts.get(0).copy();
					copy1.grow(2);
					RecipesCrusher.get(EnumMachineType.MECHANICAL).add(copy0, copy1);
				} else if(gems.size() > 0 && ores.size() > 0)
				{
					copy0 = ores.get(0).copy();
					copy1 = gems.get(0).copy();
					copy1.grow(2);
					RecipesCrusher.get(EnumMachineType.ELECTRICAL).add(copy0, copy1);
				}
			}
			if(!o.startsWith("ingot") && !o.startsWith("plate"))
				continue;
			mat = o.substring(5);
			dust = "dust" + mat;
			NonNullList<ItemStack> dusts = OreDictionary.getOres(dust);
			
			//Add coal -> coal dust
			if(mat.equalsIgnoreCase("coal"))
				RecipesCrusher.get(EnumMachineType.MECHANICAL).add(new ItemStack(Items.COAL), dusts.get(0).copy());
			
			NonNullList<ItemStack> ores = OreDictionary.getOres(o);
			if(dusts.size() <= 0 || ores.size() <= 0)
				continue;
			ItemStack copy0 = ores.get(0).copy();
			ItemStack copy1 = dusts.get(0).copy();
			RecipesCrusher.get(EnumMachineType.MECHANICAL).add(copy0, copy1);
		}
		
		RecipesCrusher.get(EnumMachineType.MECHANICAL).add(new ItemStack(Blocks.STONE), new ItemStack(Blocks.COBBLESTONE));
		RecipesCrusher.get(EnumMachineType.MECHANICAL).add(new ItemStack(Blocks.COBBLESTONE), new ItemStack(Blocks.GRAVEL));
		RecipesCrusher.get(EnumMachineType.MECHANICAL).add(new ItemStack(Blocks.GRAVEL), new ItemStack(Blocks.SAND));
		
		addExNihilioCompat();
	}
	
	private static void addExNihilioCompat()
	{
		if(Loader.isModLoaded("exnihilocreatio"))
		{
			Item dust = Item.REGISTRY.getObject(new ResourceLocation("exnihilocreatio", "block_dust"));
			
			RecipesCrusher.get(EnumMachineType.MECHANICAL).add(new ItemStack(Blocks.SAND), new ItemStack(dust));
			RecipesCrusher.get(EnumMachineType.MECHANICAL).add(new ItemStack(dust), EnumMultiMaterialType.SILICON.stack());
		} else
		{
			RecipesCrusher.get(EnumMachineType.MECHANICAL).add(new ItemStack(Blocks.SAND), EnumMultiMaterialType.SILICON.stack());
		}
	}
}