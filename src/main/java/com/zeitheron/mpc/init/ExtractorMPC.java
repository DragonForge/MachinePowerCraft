package com.zeitheron.mpc.init;

import com.zeitheron.mpc.api.EnumMachineType;
import com.zeitheron.mpc.api.recipes.extractor.RecipesExtractor;
import com.zeitheron.mpc.items.ItemMultiMaterial;

import net.minecraft.item.ItemStack;

public class ExtractorMPC
{
	static
	{
		extractor(ItemMultiMaterial.EnumMultiMaterialType.RESIN.stack(), ItemMultiMaterial.EnumMultiMaterialType.RUBBER.stack(3));
		extractor(new ItemStack(BlocksMPC.RUBBER_LOG), ItemMultiMaterial.EnumMultiMaterialType.RUBBER.stack());
		extractor(new ItemStack(BlocksMPC.RUBBER_SAPLING), ItemMultiMaterial.EnumMultiMaterialType.RUBBER.stack());
	}
	
	private static void extractor(ItemStack in, ItemStack out)
	{
		RecipesExtractor.get(EnumMachineType.ELECTRICAL).add(in, out);
	}
}