package com.zeitheron.mpc.init;

import com.zeitheron.mpc.api.armor.ElectrialConductorArmorEntry;
import com.zeitheron.mpc.api.armor.ElectrialConductorArmorRegistry;
import com.zeitheron.mpc.api.energy.EnergyPacket;
import com.zeitheron.mpc.intr.is3.ImprovableSkillsAPI;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;

public class DamageSourceMPC extends DamageSource
{
	public static final DamageSource STEAM_GRINDER = new DamageSourceMPC("mpc:dmg.steam_grinder").setDamageBypassesArmor();
	public static final DamageSource ELECTROCUTION = new DamageSourceMPC("mpc:dmg.electrocution").setDamageBypassesArmor();
	
	public DamageSourceMPC(String damageTypeIn)
	{
		super(damageTypeIn);
	}
	
	public static void dealElectricalDamage(Entity ent, EnergyPacket packet)
	{
		double damage;
		double newDmg = damage = packet.amperes / 16.0 * 8.0;
		double additionalDmg = 0.0;
		for(ItemStack armor : ent.getArmorInventoryList())
		{
			ElectrialConductorArmorEntry e = ElectrialConductorArmorRegistry.getEntry(armor);
			if(e == null)
				continue;
			newDmg *= e.getDamageScale(ent, armor, packet, damage);
			additionalDmg += e.getAdditionalDamage(ent, armor, packet, damage);
			e.doAdditionalDamage(ent, armor, packet, damage);
		}
		float dmg = (float) (newDmg + additionalDmg);
		if(ent instanceof EntityPlayer)
		{
			int lvl = ImprovableSkillsAPI.getSkills().getSkillLvl((EntityPlayer) ent, ImprovableSkillsAPI.ELECTRICAL_DEFLECTOR);
			if(lvl > 0)
				dmg /= lvl;
		}
		ent.attackEntityFrom(ELECTROCUTION, dmg * (ent.world.isRainingAt(ent.getPosition()) ? 2 : 1));
	}
}