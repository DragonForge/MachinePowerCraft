/* Decompiled with CFR 0_123. */
package com.zeitheron.mpc.utils;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.security.SecureRandom;
import java.util.Arrays;

public class GemImage
{
	public final BufferedImage image;
	private final int color;
	private byte[] seed;
	private SecureRandom rand;
	private int[] corePos;
	private int minPix;
	
	public GemImage(byte[] randSeed, int color, int quality)
	{
		this.image = new BufferedImage(quality, quality, 2);
		this.color = color;
		this.setSeed(randSeed);
	}
	
	public void setSeed(byte[] seed)
	{
		this.seed = seed;
		this.regen();
	}
	
	public void regen()
	{
		this.reset();
		this.prepare();
		this.draw();
	}
	
	public void reset()
	{
		Graphics2D g = this.image.createGraphics();
		Arrays.fill(this.getPixelData(), 16777215);
	}
	
	public void prepare()
	{
		this.rand = new SecureRandom(this.seed);
		this.corePos = this.choosePos();
		this.minPix = this.rand.nextInt(9 * this.image.getHeight()) + (int) (this.image.getHeight() * 1.5);
	}
	
	public void draw()
	{
		--this.minPix;
		this.image.setRGB(this.corePos[0], this.corePos[1], this.color);
		while(this.minPix > 0)
		{
			this.grow(this.corePos[0], this.corePos[1], 0);
		}
	}
	
	private void grow(int x, int y, int depth)
	{
		if(x < 0 || y < 0 || x >= this.image.getWidth() || y >= this.image.getHeight() || depth > this.image.getHeight())
		{
			return;
		}
		this.image.setRGB(x, y, this.makeLoudness(this.color));
		--this.minPix;
		if(this.rand.nextInt(4) <= 2 && this.canDrawAt(x, y - 1))
		{
			this.grow(x, y - 1, depth + 2);
		}
		if(this.rand.nextInt(4) <= 2 && this.canDrawAt(x, y + 1))
		{
			this.grow(x, y + 1, depth + 2);
		}
		if(this.rand.nextInt(4) <= 1 && this.canDrawAt(x - 1, y))
		{
			this.grow(x + 1, y, depth + 4);
		}
		if(this.rand.nextInt(4) <= 1 && this.canDrawAt(x + 1, y))
		{
			this.grow(x + 1, y, depth + 4);
		}
	}
	
	private int makeLoudness(int color)
	{
		int r = color >> 16 & 255;
		int g = color >> 8 & 255;
		int b = color >> 0 & 255;
		int delta = this.rand.nextInt(8) - this.rand.nextInt(8);
		r += delta;
		g += delta;
		b += delta;
		r = Math.max(0, Math.min(r, 255));
		g = Math.max(0, Math.min(g, 255));
		b = Math.max(0, Math.min(b, 255));
		return -16777216 | r << 16 | g << 8 | b;
	}
	
	private boolean canDrawAt(int x, int y)
	{
		return this.isInBounds(x, y) && this.image.getRGB(x, y) == 16777215;
	}
	
	private boolean isInBounds(int x, int y)
	{
		return x >= 0 && y >= 0 && x < this.image.getWidth() && y < this.image.getHeight();
	}
	
	private int[] choosePos()
	{
		return new int[] { this.image.getWidth() - 10 + this.rand.nextInt((int) Math.sqrt(this.image.getWidth())), this.image.getWidth() - 10 + this.rand.nextInt((int) Math.sqrt(this.image.getWidth())) };
	}
	
	public int[] getPixelData()
	{
		return ((DataBufferInt) this.image.getRaster().getDataBuffer()).getData();
	}
}
