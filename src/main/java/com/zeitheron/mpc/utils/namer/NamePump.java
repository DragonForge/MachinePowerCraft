package com.zeitheron.mpc.utils.namer;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;

public class NamePump
{
	protected final SecureRandom random;
	protected final String[] buffer = new String[64];
	protected final NameGen gen;
	
	public NamePump(byte[] seed, int complexity)
	{
		this.random = new SecureRandom(seed);
		this.gen = new NameGen(seed, complexity);
		for(int i = 0; i < this.buffer.length; ++i)
		{
			this.buffer[i] = this.$generate();
		}
	}
	
	protected String $generate()
	{
		return this.gen.genNames(1)[0];
	}
	
	protected final String $_moveon()
	{
		String prev = this.buffer[this.buffer.length - 1];
		for(int i = this.buffer.length - 1; i > 0; --i)
		{
			this.buffer[i] = this.buffer[i - 1];
		}
		this.buffer[0] = this.$generate();
		return prev;
	}
	
	public String[] supply(int count)
	{
		ArrayList<String> supplied = new ArrayList<String>(count);
		int left = count;
		while(left-- > 0)
			supplied.add(this.$_moveon());
		return supplied.toArray(new String[count]);
	}
	
	public String supplyNext()
	{
		return this.$_moveon();
	}
	
	@Override
	public String toString()
	{
		String cn = this.getClass().getName();
		return cn.substring(cn.lastIndexOf(46) + 1) + "{buffer=" + Arrays.asList(this.buffer) + "}";
	}
}
