package com.zeitheron.mpc.utils.namer;

import java.io.Serializable;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;

public class PseudoRandomName implements Serializable
{
	private final BigInteger number;
	
	public PseudoRandomName(String word)
	{
		this(PseudoRandomName.toNumber(word));
	}
	
	public PseudoRandomName(byte[] data)
	{
		this.number = PseudoRandomName.decode(data);
	}
	
	private PseudoRandomName(BigInteger num)
	{
		this.number = num;
	}
	
	private static BigInteger toNumber(String ln)
	{
		byte[] data = ln.getBytes();
		SecureRandom rand = new SecureRandom(data);
		rand.nextBytes(data);
		return new BigInteger(ln.length() * (rand.nextInt(6) + 1), rand);
	}
	
	@Override
	public boolean equals(Object obj)
	{
		return obj instanceof PseudoRandomName && ((PseudoRandomName) obj).number.equals(this.number);
	}
	
	@Override
	public PseudoRandomName clone()
	{
		return new PseudoRandomName(this.number);
	}
	
	@Override
	public String toString()
	{
		return this.toString(true);
	}
	
	@Override
	public int hashCode()
	{
		return this.number.hashCode();
	}
	
	public String toString(boolean format)
	{
		byte[] mag = PseudoRandomName.encode(this.number);
		String bytes = Arrays.toString(mag);
		bytes = bytes.substring(1, bytes.length() - 1);
		bytes = "new byte[] { " + bytes + " }";
		if(!format)
		{
			return bytes;
		}
		ArrayList<String> lines = new ArrayList<String>();
		String accumulated = "";
		for(int start = 0; start < bytes.length(); ++start)
		{
			if(bytes.charAt(start) == ' ' && accumulated.length() >= 120)
			{
				lines.add((lines.isEmpty() ? "" : "\t") + accumulated);
				accumulated = "";
				continue;
			}
			accumulated = accumulated + bytes.charAt(start) + "";
		}
		if(!accumulated.isEmpty())
		{
			lines.add("\t" + accumulated);
			accumulated = "";
			bytes = "";
		}
		for(String i : lines)
		{
			bytes = bytes + i + "\n";
		}
		if(bytes.endsWith("\n"))
		{
			bytes = bytes.substring(0, bytes.length() - 1);
		}
		return bytes;
	}
	
	private static BigInteger decode(byte[] data)
	{
		return data == null || data.length == 0 ? BigInteger.ZERO : new BigInteger(data);
	}
	
	private static byte[] encode(BigInteger number)
	{
		return number != null ? number.toByteArray() : new byte[] {};
	}
}
