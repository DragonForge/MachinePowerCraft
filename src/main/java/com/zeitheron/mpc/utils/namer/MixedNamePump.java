package com.zeitheron.mpc.utils.namer;

public class MixedNamePump extends NamePump
{
	public MixedNamePump(byte[] seed, int complexity)
	{
		super(seed, complexity);
	}
	
	@Override
	protected String $generate()
	{
		return this.random.nextBoolean() ? this.gen.genDoubleName() : this.gen.genNames(1)[0];
	}
}
