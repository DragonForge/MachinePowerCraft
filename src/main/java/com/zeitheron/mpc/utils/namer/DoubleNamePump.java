package com.zeitheron.mpc.utils.namer;

public class DoubleNamePump extends NamePump
{
	public DoubleNamePump(byte[] seed, int complexity)
	{
		super(seed, complexity);
	}
	
	@Override
	protected String $generate()
	{
		return this.gen.genDoubleName();
	}
}
