package com.zeitheron.mpc.utils.namer;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class NameGen
{
	public static final String digrams = "ABOUSEITILETSTONLONUTHNO..LEXEGEZACEBISOUSESARMAINDIREA.ERATENBERALAVETIEDORQUANTEISRION";
	private static final Set<PseudoRandomName> blacklistedWords = new HashSet<PseudoRandomName>();
	public final int complexity;
	private final SecureRandom rand;
	
	public NameGen(byte[] seed, int complexity)
	{
		this.rand = new SecureRandom(seed);
		this.complexity = complexity;
	}
	
	public int rotatel(int x)
	{
		int tmp = (x & 255) * 2;
		if(tmp > 255)
		{
			tmp -= 255;
		}
		return tmp;
	}
	
	public int twist(int x)
	{
		return 256 * this.rotatel(x / 256) + this.rotatel(x & 255);
	}
	
	public int[] tweakseed(int[] seeds)
	{
		int tmp = 0;
		int i = 0;
		for(int s : seeds)
		{
			seeds[i] = tmp += s;
			++i;
		}
		int[] s = new int[seeds.length];
		for(i = 0; i < s.length; ++i)
		{
			s[i] = seeds[i] + (tmp & 65535);
		}
		return s;
	}
	
	public int[] next(int[] seeds)
	{
		int[] s = new int[seeds.length];
		for(int i = 0; i < s.length; ++i)
		{
			s[i] = this.twist(seeds[i]);
		}
		return s;
	}
	
	public String makename(String pairs, int[] seeds)
	{
		ArrayList<String> name = new ArrayList<String>();
		int[] pair = new int[2 + this.complexity];
		boolean longname = (seeds[0] & 64) == 0;
		for(int i = 0; i < pair.length; ++i)
		{
			seeds = this.tweakseed(seeds);
			pair[i] = 2 * (seeds[2] / 256 & 31);
		}
		int index = 0;
		for(int value : pair)
		{
			if(longname || index < pair.length - 1)
			{
				name.add("" + pairs.charAt(value));
				name.add("" + pairs.charAt(value + 1));
			}
			++index;
		}
		String name_ = "";
		for(String s : name)
		{
			name_ = name_ + s;
		}
		name_ = name_.toLowerCase();
		while(name_.contains("."))
		{
			name_ = name_.replace(".", "");
		}
		if(name_.isEmpty())
		{
			return this.makename(pairs, seeds);
		}
		if(blacklistedWords.contains(new PseudoRandomName(name_.toLowerCase())))
		{
			return this.makename(pairs, seeds);
		}
		char[] chars = name_.toCharArray();
		chars[0] = Character.toUpperCase(chars[0]);
		name_ = new String(chars);
		return name_;
	}
	
	public String genDoubleName()
	{
		String[] ns = this.genNames(2);
		return ns[1] + " " + ns[0];
	}
	
	public String[] genNames(int num)
	{
		String[] names = new String[++num - 1];
		int[] seeds = new int[] { this.rand.nextInt(), this.rand.nextInt(), this.rand.nextInt() };
		String pairs = "ABOUSEITILETSTONLONUTHNO..LEXEGEZACEBISOUSESARMAINDIREA.ERATENBERALAVETIEDORQUANTEISRION".substring(24);
		for(int i = 0; --num > 0 && i < names.length; ++i)
		{
			names[i] = this.makename(pairs, seeds);
			seeds = this.tweakseed(this.next(seeds));
		}
		return names;
	}
	
	static
	{
		blacklistedWords.add(new PseudoRandomName(new byte[] { 5, 54 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 85, 30, -102 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -77, -42, 51, -109, -6 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 5 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -57, 80, 1 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 90 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 11, -115, 123, 4, 102 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 55 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -101, 12, -74, -60, -63, 76 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 67 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 7, 99, -128 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 3, 30 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 39, 24 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 14, -68, 27 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 2, -68, -116, 45 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 17, 52, 50, 75, 126, 98, 13 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 14, -94 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 29, 76, 97 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 7, 18 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 55, 69, -95, 34, -28, -37, 82 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 1, 11 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 9, -95 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 31, 67, 111, -79, -110, 57, -119 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 26, 40, 63, -23, -127, -114 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -3, 30, 98, -88, 45, -111 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 6, 92, 98, 69 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 2, -38, 44 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 17, -90, -69, -71, 32, -65 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 3, -105, -47, -51, 108, -106 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 17, -13, -80 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 6, 6, 95, 104, 127 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 59, -100, 30, 95 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 27, 7 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 19, -21, 86 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 24, -71, -77, 6 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 27, 51 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 11, -5 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 1 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -11, 96, -3 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -33, -86, -29, -47, -71 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 10, -92, -38, 62, 25 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -13, 29, 84, -69 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 2, -113, 125 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -25, -45 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 15, -126, -115, -118, -120 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 20, -106, 43, 89 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 54, -103, -36, 40, 127 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 125, 68 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -8 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 5, -100, 63, -128 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 3, -55 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 1, 46, -103, -55, 58, -43, 85, -66, 39 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 8, 42, -10, 6, 37, 51, 126 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 25 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 109, -121 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 6, 42, -33 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 92, -43 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 11, -15, -113 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -33, 32 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 124 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -19, -105, 127, -83, 70, -19, -31, -29 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 108 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 2, 61, 98, 123, -45, 108 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -84, 40, -35, 79, 58 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 13, -23, 64, 1, -117, -107, -66, 80 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -23, -71, 93, 10, -124, 6 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 2, -12, -26, 72, 64, -112, 30 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -8, 1, -7, 120, -18 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 18, 10, 38, 55 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 13, -54, -65 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 1, 66, -9, -38, -65, -7, -6, 81 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 4, 25, 61, 16, 27, 73 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 111, 64, 18 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 112, 71, 125, -127, -34, -27 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -29, -15, -92, -81 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 111, 58, -80, 60 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 1, -115 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 112, -101 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 5, -110, 30, -125, 109 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 14, 14, 85, -50, 47 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 11, -96, -50 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 2, -44, 17, -115, 66, -35, -97, -51, 108 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 71, -49, -22, -2, 90 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 3, 18, -3, -17, 54, -90, -26 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 6, 5, -7, 77, -79 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 33, -82, -78, -52 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 108, -22 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 11 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 25, 19, 96 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 99, 41 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 2, -66, 93, 1, 18, 32 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 1, 47, -38, -111, 18 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 5, -37, 91, 107, 38 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 13, 59, 2, -48, 99, -10 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 3, -67, 60 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 3, -102, 77, -115, -69, 86 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -13, 58, 64 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -34, 105, 100, 77 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 118, 34 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 126, -72, 12 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -128, -36, 48, -21, 48, -127 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 6, -56, -105, 18, -114, 25, 19, -95 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 10, -51, 82 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -66, 11, 46 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 3, 65 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 41, 49, 95 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -17 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -62 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 24, -1, 52 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 79, 59, 112, 74, 87, -74, 54 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 115, -58, -66, 83 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 90, -106, -91, 60, 0 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 10, -74, -104 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -44, -68, 97, 19, -83 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -111, -20, 1, -39, -66, 88 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 1, 77 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 5, -48, 94, 31 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 1, 14, 123, 70, -7, -108, -26 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 50, -19, 65, 116 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 9, -31, 126, -82 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 12, -18, -104 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 57, -62, -39 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 2, -43, -102 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 2, 0, -90, 108, 86, -128 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 41, -35 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -70, -54 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 6, 29, -59, -65 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 15, 82, 92 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 5, 118, -94, 45 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 8, 98, -127, 91 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 6, 47, 31, 73, 48 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -3, -106 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 6, 96, -62, -76, -12 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 7, 79, 88, -82 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 10, -23 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 5, -110, 63, 119 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 5, 114, -121, 116, 70 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 30, -55 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -56, -84, -90, -14, -87, -57 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 43, -20 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 14, -109, 111 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 2, -82, 122 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 3, -80, -35, 21, -120 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 34, 20, 93, 91, 101, -32, 108 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 4, 54, -9, -33, 99 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 100, 72, -102 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 1, 73 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 9, 8, 85 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 2, 92, -29 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -87, -113 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 31 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 13, -29 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 10 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 31, 0 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 5, -35, -83 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 4, -77, -105, 6, -33 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 1, 89 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 3 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 3, -70 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -35, 127, 111 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 8, 102 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 12, -121 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -76, 40, 37, -24, 73, 86 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 85, 47 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 126, 50 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 3, 114 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 1, -6, -64, 4, 66, -18 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 2, -99, 103, 97, -77, 101 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 97 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 6, 3, -27, -19 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 28 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 127, -83 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 106, 52 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 2, 51, -19, -36, 6 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 21 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 107 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 7, 117, -80, -32 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -41 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -62, -43, 63, -112, 123, -76, 9, 103 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 61, -118, -81 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 11, -124 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 1, -30, 91, 20 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -102, -73, -14 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 2, 66 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 2, -61, 55, -63, 27, -29 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 3, -82, -65, 55 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 7, -103 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 10, 66, 18, 38 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 15, 54, -61, 126, -57, -107 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 13, -112, 45 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 34, 35, -81 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 34, 13, 87, 120 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 7 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 24, 70, 117 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 9 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -31, -4 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 3, -128 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 12, 30, 30 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 31, -78, 34, -7, -113, 28, -114, 113, -60 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 53, -69, -94, 84 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 1, 23, -14, 126, -7, 125 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 46, 104 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -61, 51 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 2, 47 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 73 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 1, 72, 121, -5 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -39, 1, -86 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 1, -88, -31, 122, 86, 99, -112, 58, 5 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 100 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 24, 83, 34, -57 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 34 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 12, -75, 64 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 8 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 97, -92 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 2, 74, 32, -1, 104 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 7, -6, 75 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 21, 124, 33 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 10, -16, 108 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 1, -43, 20 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -47, 20, 14, -46 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -53, -119 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 6, -4, 63 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -77, -28, 54, 27, 94 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 13, 126, -96 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 3, 79 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 114, -21, 11, 78, 125, 4, -29, 80 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -63, 8 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 9, -13, -18, -97, -59, 47 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 7, -2, 100, 46, 47 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 28 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 17, 84, 17 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 2, -16 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 27 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -18, 101, -46, -3, -39 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 12, -103 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 2, 98, -117, 52 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 2, 60, -87 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 10, 32, -125, 47 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 5 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 28, -61, 72, -91 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 122 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 1, -94, 119, -62, -126, -102, 70, 40, -10 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 61, 115, 83 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 2, 14, -55, -1, 92, 126, 86, -37, 105 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 62, 39, 43, 92 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 10, 27, 104, 82, -126, -110, -125 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -5, 55, -90, 11, 20 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 43, -58 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 4 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 13, 116, 69, 56, -41 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 86 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 109, -108, -78, 127, -19, -93 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 3, -105 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 1, 108, 64, 13 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 29, -65, 63 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -45, -50, 125 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 15, -98, 35 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 10 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -77, 54, -4, 125, 85, -48, 49, 30, 95 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 83 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 3, -34 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 5 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 3, 0, 19 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 1, 80, 38 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 51, -56 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 2, -57, 55 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 26, 50, 30 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 1, -124 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 24, -23 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 4, -42 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 19, 110 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 7, 22, -64, -113, 123 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 40 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 5, 112, -16, 127, 94, 16, -8, -45 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 1, 25, 27, -39, 10, 68, 4 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 2, 56, -8, -116, -62, -27, -48 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -75, -92, 78, -36, -91 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 56, -14, 16, 38, 118, -53 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 65, 99 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 70, 41, 21, 66 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 15, -120, -56, -100, 112 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 61, 51, 47 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -40, -68, 38, -31, 4 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 23, 65, -112, 50, -62, -115, -1 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 50, 17, -38 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -71, -100 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 59, 20 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 14, 123, -95, -18, 72 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -9, -63, -91, 125, -28 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 6, 56, -126, 37, -107 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 14, -72 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 12, -109, 55, 108 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 105, 65 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 3, 75 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 32 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 14 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 6, -77, -86, -117, -86 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 40, 15, -15 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 31, -38, -124 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 2, -45 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 7, 52, -108 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 34, 58 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 28 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 8, 104, 91, 108 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -96, -111, -10 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 27 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 3, -10 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 6, -89, -84, 106, -74 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 82 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 31, 27, -113, -53, 76, -121 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -46 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 122, 104 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 10, 81, 113 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 57, -110, -60, -85 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 30, -99, -26, 32 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 4 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -110, -67, -61 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -51, -27, 66, 3 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 7, -41 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 78, -83, 37 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 1, -108, -22, -36, -85, -21 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 14, 39, 112, -95 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 51, 67 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 0, -83, -96, -110 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 16, 102, 110, 20, 115, 81 }));
		blacklistedWords.add(new PseudoRandomName(new byte[] { 32, -128 }));
	}
}
