package com.zeitheron.mpc.utils;

import java.security.SecureRandom;
import java.util.Random;

import com.zeitheron.hammercore.utils.color.ColorHelper;

public class ColorPump
{
	private Random rand = new Random();
	private int[] allowedColors;
	private int minBrightness, maxBrightness;
	private long interpDel;
	
	private long lastPicked = 0L;
	private int ccol, ncol;
	
	/**
	 * Prepares a color pump that supplies all colors. The seed for color
	 * generation is random. You can change all of that
	 */
	public ColorPump(long delay)
	{
		rand = new SecureRandom();
		interpDel = delay;
		minBrightness = 0;
		maxBrightness = 255;
	}
	
	public ColorPump setAllowedColors(int... rgbs)
	{
		allowedColors = rgbs;
		return this;
	}
	
	public ColorPump setBrightnessRange(int min, int max)
	{
		this.minBrightness = min;
		this.maxBrightness = max;
		return this;
	}
	
	public ColorPump setSeed(long seed)
	{
		rand = new Random(seed);
		return this;
	}
	
	private int generateColor()
	{
		if(allowedColors != null)
			return allowedColors[rand.nextInt(allowedColors.length)];
		int r = minBrightness + rand.nextInt(maxBrightness - minBrightness + 1);
		int g = minBrightness + rand.nextInt(maxBrightness - minBrightness + 1);
		int b = minBrightness + rand.nextInt(maxBrightness - minBrightness + 1);
		return ColorHelper.packRGB(r / 255F, g / 255F, b / 255F);
	}
	
	public int get()
	{
		long now = System.currentTimeMillis();
		
		if(now - lastPicked > interpDel)
		{
			ccol = ncol;
			ncol = generateColor();
			lastPicked = now;
		}
		
		return ColorHelper.interpolate(ccol, ncol, (float) ((now - lastPicked) / (double) interpDel));
	}
}