/* Decompiled with CFR 0_123.
 * 
 * Could not load the following classes: net.minecraft.item.ItemStack */
package com.zeitheron.mpc.utils;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import net.minecraft.item.ItemStack;

public class PhysicsUtil
{
	public static final int KILO = 1000;
	public static final int MEGA = 1000000;
	public static final int TERRA = 1000000000;
	public static DecimalFormat numberFormat = new DecimalFormat("#0.00");
	
	public static String smartFormatJoules(double joules)
	{
		if(joules >= 1.0E9)
		{
			return numberFormat.format(joules / 1.0E9) + " TJ";
		}
		if(joules >= 1000000.0)
		{
			return numberFormat.format(joules / 1000000.0) + " MJ";
		}
		if(joules >= 1000.0)
		{
			return numberFormat.format(joules / 1000.0) + " kJ";
		}
		return numberFormat.format(joules) + " J";
	}
	
	public static final class Material
	{
		public static final Material WATER = new Material("water", new Density(1.0), new Meltable(100.0, 4200.0, 2300000.0));
		public static final Material CHARCOAL = new Material("charcoal", new Density(0.4), new Fuel(3.4E7));
		public static final Material BITUMINOUS_COAL = new Material("bituminous coal", new Density(1.4), new Fuel(2.7E7));
		public static final Material ANTHRACITE_COAL = new Material("anthracite coal", new Density(1.6), new Fuel(3.0E7));
		public static final Material DRY_OAK_WOOD = new Material("oak wood", new Density(0.7), new Fuel(1.0E7));
		public static final Material PEAT = new Material("peat", new Density(0.35), new Fuel(1.5E7));
		public static final Material IRON = new Material("iron", new Density(7.8), new Meltable(1535.0, 460.0, 270000.0));
		private final Map<Class<?>, IMaterialProperty> props = new HashMap();
		public final String name;
		public final String upperCasedName;
		
		public Material(String name, IMaterialProperty... props)
		{
			this.name = name = name.trim();
			char[] chars = name.toCharArray();
			chars[0] = Character.toUpperCase(chars[0]);
			Character prev = null;
			for(int i = 0; i < chars.length; ++i)
			{
				if(prev != null && prev.charValue() == ' ')
				{
					chars[i] = Character.toUpperCase(chars[i]);
				}
				prev = Character.valueOf(chars[i]);
			}
			this.upperCasedName = new String(chars);
			HashSet used = new HashSet();
			for(IMaterialProperty prop : props)
			{
				if(used.contains(prop.getClass()))
					continue;
				used.add(prop.getClass());
				this.props.put(prop.getClass(), prop);
			}
		}
		
		public void addTooltip(List<String> tip, ItemStack stack)
		{
			tip.add("Material: " + this.upperCasedName);
			for(IMaterialProperty prop : this.props.values())
			{
				prop.addTooltip(tip, stack);
			}
		}
		
		public Material addProperty(IMaterialProperty prop)
		{
			this.props.put(prop.getClass(), prop);
			return this;
		}
		
		public <T extends IMaterialProperty> T getCustomProperty(Class<T> type)
		{
			return (T) this.props.get(type);
		}
		
		public Fuel getFuelProperty()
		{
			return this.getCustomProperty(Fuel.class);
		}
		
		public Density getDensityProperty()
		{
			return this.getCustomProperty(Density.class);
		}
		
		public Meltable getMeltableProperty()
		{
			return this.getCustomProperty(Meltable.class);
		}
	}
	
	public static final class Meltable implements IMaterialProperty
	{
		private Material material;
		public final double meltTemperature;
		public final double jtoheatby1c;
		public final double jtomelt1kg;
		
		public Meltable(double meltTemperature, double jtoheatby1c, double jtomelt1kg)
		{
			this.meltTemperature = meltTemperature;
			this.jtomelt1kg = jtomelt1kg;
			this.jtoheatby1c = jtoheatby1c;
		}
		
		public double getJoulesUsedToHeat(double kg, double tempdelta)
		{
			return this.jtoheatby1c * kg * tempdelta;
		}
		
		public double getTempGiven(double kg, double joules)
		{
			return joules / (this.jtoheatby1c * kg);
		}
		
		public double getKGMelted(double joules)
		{
			return joules / this.jtomelt1kg;
		}
		
		@Override
		public void setMaterial(Material mat)
		{
			this.material = mat;
		}
		
		@Override
		public Material getMaterial()
		{
			return this.material;
		}
		
		@Override
		public void addTooltip(List<String> tooltip, ItemStack stack)
		{
			tooltip.add("Melting Temperature: " + this.meltTemperature);
			tooltip.add("Specific Heat: " + PhysicsUtil.smartFormatJoules(this.getJoulesUsedToHeat(1.0, 1.0)) + "/kg*" + '\u00b0' + "C");
			tooltip.add("Specific Melting Heat: " + PhysicsUtil.smartFormatJoules(this.jtomelt1kg) + "/kg");
		}
	}
	
	public static final class Fuel implements IMaterialProperty
	{
		private Material material;
		private final double jpkg;
		
		public Fuel(double joules)
		{
			this.jpkg = joules;
		}
		
		public double getJoulesProduced(double kg)
		{
			return this.jpkg * kg;
		}
		
		@Override
		public void setMaterial(Material mat)
		{
			this.material = mat;
		}
		
		@Override
		public Material getMaterial()
		{
			return this.material;
		}
		
		@Override
		public void addTooltip(List<String> tooltip, ItemStack stack)
		{
			tooltip.add("Fuel Value: " + PhysicsUtil.smartFormatJoules(this.getJoulesProduced(1.0)) + "/kg");
			tooltip.add("Fuel Item Value: " + PhysicsUtil.smartFormatJoules(this.getJoulesProduced(0.25)));
			tooltip.add("Fuel Stack Value: " + PhysicsUtil.smartFormatJoules(this.getJoulesProduced(0.25 * stack.getCount())));
		}
	}
	
	public static final class Density implements IMaterialProperty
	{
		private Material material;
		private final double density;
		
		public Density(double gpcm)
		{
			this.density = gpcm;
		}
		
		public double getKilogramsPerMeters(double m)
		{
			return this.density * m * 1000.0;
		}
		
		public double getGramsPerCentimeters(double cm)
		{
			return this.density * cm;
		}
		
		@Override
		public void setMaterial(Material mat)
		{
			this.material = mat;
		}
		
		@Override
		public Material getMaterial()
		{
			return this.material;
		}
		
		@Override
		public void addTooltip(List<String> tooltip, ItemStack stack)
		{
			tooltip.add("Density: " + this.getKilogramsPerMeters(1.0) + " kg/m" + '\u00b3');
		}
	}
	
	public static interface IMaterialProperty
	{
		public void setMaterial(Material var1);
		
		public Material getMaterial();
		
		public void addTooltip(List<String> var1, ItemStack var2);
	}
	
}
