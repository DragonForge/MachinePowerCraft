package com.zeitheron.mpc.ui.widget;

import com.zeitheron.hammercore.client.utils.UV;
import com.zeitheron.mpc.client.GuiBoxRenderer;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;

public class GuiWidget
{
	public static final ResourceLocation WIDGET_BASE = new ResourceLocation("mpc", "textures/gui/mechanical_book.png");
	private static final UV CORNER_1 = new UV(WIDGET_BASE, 0.0, 181.0, 4.0, 4.0);
	private static final UV CORNER_2 = new UV(WIDGET_BASE, 19.0, 181.0, 4.0, 4.0);
	private static final UV CORNER_3 = new UV(WIDGET_BASE, 19.0, 200.0, 4.0, 4.0);
	private static final UV CORNER_4 = new UV(WIDGET_BASE, 0.0, 200.0, 4.0, 4.0);
	public int posX;
	public int posY;
	public int width = 172;
	public int height = 60;
	private boolean isActive = false;
	
	public final void drawWidget(Minecraft mc, int mouseX, int mouseY)
	{
		GuiBoxRenderer.drawBox(mc, this.posX, this.posY, this.width, this.height);
		this.draw(mc, mouseX, mouseY);
	}
	
	public final void mouseClick(int mouseX, int mouseY, int clickType)
	{
		this.click(mouseX, mouseY, clickType);
	}
	
	protected void draw(Minecraft mc, int mouseX, int mouseY)
	{
	}
	
	protected void click(int mouseX, int mouseY, int clickType)
	{
	}
	
	public final boolean isActive()
	{
		return this.isActive;
	}
	
	public final void setActive(boolean isActive)
	{
		this.isActive = isActive;
	}
}
