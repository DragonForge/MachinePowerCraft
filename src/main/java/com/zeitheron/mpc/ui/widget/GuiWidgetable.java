package com.zeitheron.mpc.ui.widget;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.gui.GuiScreen;

public class GuiWidgetable extends GuiScreen
{
	public List<GuiWidget> widgets = new ArrayList<GuiWidget>();
	
	public void addWidget(GuiWidget widget)
	{
		this.widgets.add(widget);
		widget.posX = (this.width - widget.width) / 2;
		widget.posY = (this.height - widget.height) / 2;
	}
	
	public void drawWidgets(int mouseX, int mouseY)
	{
		for(int i = 0; i < this.widgets.size(); ++i)
		{
			this.widgets.get(i).drawWidget(this.mc, mouseX, mouseY);
			GL11.glColor4d(1.0, 1.0, 1.0, 1.0);
		}
	}
	
	@Override
	protected void mouseClickMove(int mouseX, int mouseY, int clickedMouseButton, long timeSinceLastClick)
	{
		super.mouseClickMove(mouseX, mouseY, clickedMouseButton, timeSinceLastClick);
		GuiWidget focus = null;
		for(int i = this.widgets.size() - 1; i >= 0; --i)
		{
			GuiWidget widget = this.widgets.get(i);
			if(mouseX < widget.posX - 4 || mouseY < widget.posY - 4 || mouseX >= widget.posX + widget.width - 4 || mouseY >= widget.posY + 4)
				continue;
			widget.posY = mouseY;
			widget.posX = mouseX - widget.width / 2;
			focus = widget;
			break;
		}
		if(focus != null)
		{
			this.widgets.remove(0);
			this.widgets.add(focus);
		}
	}
	
	@Override
	protected final void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
		super.mouseClicked(mouseX, mouseY, mouseButton);
		this.mouseClick(mouseX, mouseY, mouseButton);
		for(int i = 0; i < this.widgets.size(); ++i)
		{
			GuiWidget widget = this.widgets.get(i);
			widget.mouseClick(mouseX, mouseY, mouseButton);
		}
	}
	
	protected void mouseClick(int mouseX, int mouseY, int mouseButton)
	{
	}
}
