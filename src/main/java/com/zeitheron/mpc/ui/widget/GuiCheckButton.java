package com.zeitheron.mpc.ui.widget;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;

public class GuiCheckButton extends GuiButton
{
	public static final ResourceLocation WIDGET_TEXTURES = new ResourceLocation("machinepowercraft", "textures/gui/widgets.png");
	public boolean isTicked = false;
	
	public GuiCheckButton(int buttonId, int x, int y)
	{
		super(buttonId, x, y, 20, 20, "");
	}
	
	public void drawButton(Minecraft mc, int mouseX, int mouseY)
	{
		if(this.visible)
		{
			FontRenderer fontrenderer = mc.fontRenderer;
			mc.getTextureManager().bindTexture(WIDGET_TEXTURES);
			GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
			this.hovered = mouseX >= this.x && mouseY >= this.y && mouseX < this.x + this.width && mouseY < this.y + this.height;
			int i = this.getHoverState(this.hovered);
			GlStateManager.enableBlend();
			GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
			GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
			this.drawTexturedModalRect(this.x, this.y, this.isTicked ? 20 : 0, i * 20, this.width / 2, this.height);
			this.mouseDragged(mc, mouseX, mouseY);
		}
	}
}
