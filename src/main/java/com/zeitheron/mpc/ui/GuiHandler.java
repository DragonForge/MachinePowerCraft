/* Decompiled with CFR 0_123.
 * 
 * Could not load the following classes: net.minecraft.block.Block
 * net.minecraft.block.state.IBlockState
 * net.minecraft.entity.player.EntityPlayer net.minecraft.tileentity.TileEntity
 * net.minecraft.util.math.BlockPos net.minecraft.world.World
 * net.minecraftforge.fml.common.network.IGuiHandler
 * net.minecraftforge.fml.common.network.internal.FMLNetworkHandler */
package com.zeitheron.mpc.ui;

import com.zeitheron.mpc.MachinePowerCraft;
import com.zeitheron.mpc.blocks.BlockMachineBaseUnrotateable;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;
import net.minecraftforge.fml.common.network.internal.FMLNetworkHandler;

public class GuiHandler implements IGuiHandler
{
	@Override
	public Object getServerGuiElement(int ID2, EntityPlayer player, World world, int x, int y, int z)
	{
		IBlockState state;
		BlockPos pos = new BlockPos(x, y, z);
		if(ID2 == ID.MACHINE.ordinal() && (state = world.getBlockState(pos)).getBlock() instanceof BlockMachineBaseUnrotateable)
		{
			BlockMachineBaseUnrotateable mach = (BlockMachineBaseUnrotateable) state.getBlock();
			return mach.getServerGuiPart(player, world.getTileEntity(pos));
		}
		return null;
	}
	
	@Override
	public Object getClientGuiElement(int ID2, EntityPlayer player, World world, int x, int y, int z)
	{
		IBlockState state;
		BlockPos pos = new BlockPos(x, y, z);
		if(ID2 == ID.MACHINE.ordinal() && (state = world.getBlockState(pos)).getBlock() instanceof BlockMachineBaseUnrotateable)
		{
			BlockMachineBaseUnrotateable mach = (BlockMachineBaseUnrotateable) state.getBlock();
			return mach.getClientGuiPart(player, world.getTileEntity(pos));
		}
		return null;
	}
	
	public static void openGui(EntityPlayer entityPlayer, ID id, World world, BlockPos pos)
	{
		if(!world.isRemote)
		{
			FMLNetworkHandler.openGui(entityPlayer, MachinePowerCraft.instance, id.ordinal(), world, pos.getX(), pos.getY(), pos.getZ());
		}
	}
	
	public static enum ID
	{
		MACHINE;
		
		private ID()
		{
		}
	}
	
}
