package com.zeitheron.mpc.ui.inv;

import com.zeitheron.hammercore.client.gui.impl.container.ItemTransferHelper.TransferableContainer;
import com.zeitheron.mpc.blocks.kinetic.tile.TileMechanicalCrusher;
import com.zeitheron.mpc.ui.inv.slot.SlotMechanicalCrushable;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.SlotFurnaceOutput;

public class ContainerMechanicalCrusher extends TransferableContainer<TileMechanicalCrusher>
{
	public ContainerMechanicalCrusher(EntityPlayer player, TileMechanicalCrusher crusher)
	{
		super(player, crusher, 8, 84);
	}
	
	@Override
	public void addCustomSlots()
	{
		addSlotToContainer(new SlotMechanicalCrushable(t.inv, 0, 44, 28));
		addSlotToContainer(new SlotFurnaceOutput(player, t.inv, 1, 116, 28));
	}
	
	@Override
	protected void addTransfer()
	{
		transfer.addInTransferRule(0, getSlot(0)::isItemValid);
		transfer.toInventory(0);
		transfer.toInventory(1);
	}
}