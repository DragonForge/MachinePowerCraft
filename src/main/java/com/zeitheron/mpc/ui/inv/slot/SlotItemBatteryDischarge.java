package com.zeitheron.mpc.ui.inv.slot;

import com.zeitheron.mpc.api.energy.IEnergyItem;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotItemBatteryDischarge extends Slot
{
	public SlotItemBatteryDischarge(IInventory inventoryIn, int index, int xPosition, int yPosition)
	{
		super(inventoryIn, index, xPosition, yPosition);
	}
	
	@Override
	public boolean isItemValid(ItemStack stack)
	{
		return stack.getItem() instanceof IEnergyItem && ((IEnergyItem) stack.getItem()).getAmperesStored(stack) > 0.0;
	}
}