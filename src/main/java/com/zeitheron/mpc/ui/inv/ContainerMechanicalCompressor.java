package com.zeitheron.mpc.ui.inv;

import com.zeitheron.hammercore.client.gui.impl.container.ItemTransferHelper.TransferableContainer;
import com.zeitheron.mpc.blocks.kinetic.tile.TileMechanicalCompressor;
import com.zeitheron.mpc.ui.inv.slot.SlotMechanicalCompressable;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.SlotFurnaceOutput;

public class ContainerMechanicalCompressor extends TransferableContainer<TileMechanicalCompressor>
{
	public ContainerMechanicalCompressor(EntityPlayer player, TileMechanicalCompressor compressor)
	{
		super(player, compressor, 8, 84);
	}
	
	@Override
	protected void addCustomSlots()
	{
		addSlotToContainer(new SlotMechanicalCompressable(t.inv, 0, 44, 28));
		addSlotToContainer(new SlotFurnaceOutput(player, t.inv, 1, 116, 28));
	}
	
	@Override
	protected void addTransfer()
	{
		transfer.addInTransferRule(0, getSlot(0)::isItemValid);
		transfer.toInventory(0);
		transfer.toInventory(1);
	}
}