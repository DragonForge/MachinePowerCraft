package com.zeitheron.mpc.ui.inv.slot;

import com.zeitheron.hammercore.utils.InterItemStack;
import com.zeitheron.mpc.blocks.energy.tile.TileElectricalMachineBase;

import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotMachineInput extends Slot
{
	TileElectricalMachineBase base;
	
	public SlotMachineInput(TileElectricalMachineBase inventoryIn, int index, int xPosition, int yPosition)
	{
		super(inventoryIn, index, xPosition, yPosition);
		this.base = inventoryIn;
	}
	
	@Override
	public boolean isItemValid(ItemStack stack)
	{
		return !InterItemStack.isStackNull(this.base.getOutput(stack));
	}
}