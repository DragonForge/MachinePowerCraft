package com.zeitheron.mpc.ui.inv.slot;

import com.zeitheron.hammercore.utils.InterItemStack;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;

public class SlotCookableToCookable extends Slot
{
	public SlotCookableToCookable(IInventory inventoryIn, int index, int xPosition, int yPosition)
	{
		super(inventoryIn, index, xPosition, yPosition);
	}
	
	@Override
	public boolean isItemValid(ItemStack stack)
	{
		ItemStack smelted = FurnaceRecipes.instance().getSmeltingResult(stack);
		return stack.getItem() instanceof ItemFood && !InterItemStack.isStackNull(smelted) && smelted.getItem() instanceof ItemFood;
	}
}