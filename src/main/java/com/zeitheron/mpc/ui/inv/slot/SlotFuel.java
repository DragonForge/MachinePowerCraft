package com.zeitheron.mpc.ui.inv.slot;

import com.zeitheron.mpc.matreg.MaterialRegistry;
import com.zeitheron.mpc.utils.PhysicsUtil;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotFuel extends Slot
{
	public SlotFuel(IInventory inventoryIn, int index, int xPosition, int yPosition)
	{
		super(inventoryIn, index, xPosition, yPosition);
	}
	
	@Override
	public boolean isItemValid(ItemStack stack)
	{
		PhysicsUtil.Material mat = MaterialRegistry.getMaterial(stack);
		return mat != null && mat.getFuelProperty() != null && mat.getFuelProperty().getJoulesProduced(1.0) > 0.0;
	}
}