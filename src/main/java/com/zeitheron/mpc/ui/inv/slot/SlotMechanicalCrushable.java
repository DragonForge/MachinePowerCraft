package com.zeitheron.mpc.ui.inv.slot;

import com.zeitheron.mpc.api.EnumMachineType;
import com.zeitheron.mpc.api.recipes.crusher.RecipesCrusher;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotMechanicalCrushable extends Slot
{
	public SlotMechanicalCrushable(IInventory inventoryIn, int index, int xPosition, int yPosition)
	{
		super(inventoryIn, index, xPosition, yPosition);
	}
	
	@Override
	public boolean isItemValid(ItemStack stack)
	{
		return !RecipesCrusher.get(EnumMachineType.MECHANICAL).get(stack).isEmpty();
	}
}