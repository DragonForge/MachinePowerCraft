package com.zeitheron.mpc.ui.inv;

import com.zeitheron.hammercore.client.gui.impl.container.ItemTransferHelper.TransferableContainer;
import com.zeitheron.mpc.blocks.steam.tile.TileSteamBoiler;
import com.zeitheron.mpc.ui.inv.slot.SlotFuel;

import net.minecraft.entity.player.EntityPlayer;

public class ContainerSteamBoiler extends TransferableContainer<TileSteamBoiler>
{
	public ContainerSteamBoiler(EntityPlayer player, TileSteamBoiler boiler)
	{
		super(player, boiler, 8, 84);
	}
	
	@Override
	protected void addCustomSlots()
	{
		this.addSlotToContainer(new SlotFuel(t.inv, 0, 63, 26));
	}
	
	@Override
	protected void addTransfer()
	{
		transfer.addInTransferRule(0, getSlot(0)::isItemValid);
		transfer.toInventory(0);
	}
}