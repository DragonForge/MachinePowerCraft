package com.zeitheron.mpc.ui.inv;

import com.zeitheron.hammercore.client.gui.impl.container.ItemTransferHelper.TransferableContainer;
import com.zeitheron.mpc.blocks.energy.tile.TileElectricalMachineBase;
import com.zeitheron.mpc.ui.inv.slot.SlotItemBatteryDischarge;
import com.zeitheron.mpc.ui.inv.slot.SlotMachineInput;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.SlotFurnaceOutput;

public class ContainerElectricalBase extends TransferableContainer<TileElectricalMachineBase>
{
	public ContainerElectricalBase(EntityPlayer player, TileElectricalMachineBase tile)
	{
		super(player, tile, 8, 84);
	}
	
	@Override
	protected void addCustomSlots()
	{
		this.addSlotToContainer(new SlotMachineInput(t, 0, 59, 34));
		this.addSlotToContainer(new SlotFurnaceOutput(player, t, 1, 149, 34));
		this.addSlotToContainer(new SlotItemBatteryDischarge(t, 2, 26, 34));
	}
	
	@Override
	protected void addTransfer()
	{
		transfer.addInTransferRule(0, getSlot(0)::isItemValid);
		transfer.addInTransferRule(2, getSlot(2)::isItemValid);
		transfer.toInventory(0).toInventory(1).toInventory(2);
	}
}