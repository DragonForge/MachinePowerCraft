package com.zeitheron.mpc.ui.inv;

import com.zeitheron.hammercore.client.gui.impl.container.ItemTransferHelper.TransferableContainer;
import com.zeitheron.mpc.blocks.steam.tile.TileSteamOven;
import com.zeitheron.mpc.ui.inv.slot.SlotCookableToCookable;

import net.minecraft.entity.player.EntityPlayer;

public class ContainerSteamOven extends TransferableContainer<TileSteamOven>
{
	public ContainerSteamOven(EntityPlayer player, TileSteamOven oven)
	{
		super(player, oven, 8, 84);
	}
	
	@Override
	protected void addCustomSlots()
	{
		for(int x = 0; x < 2; ++x)
			for(int y = 0; y < 2; ++y)
				addSlotToContainer(new SlotCookableToCookable(t, x + y * 2, 67 + x * 24, 19 + y * 24));
	}
	
	@Override
	protected void addTransfer()
	{
		transfer.addInTransferRule(0, getSlot(0)::isItemValid);
		transfer.addInTransferRule(1, getSlot(1)::isItemValid);
		transfer.addInTransferRule(2, getSlot(2)::isItemValid);
		transfer.addInTransferRule(3, getSlot(3)::isItemValid);
		transfer.toInventory(0).toInventory(1).toInventory(2).toInventory(3);
	}
}