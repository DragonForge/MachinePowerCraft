package com.zeitheron.mpc.ui.gui;

import java.text.DecimalFormat;
import java.util.Arrays;

import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.utils.InterItemStack;
import com.zeitheron.mpc.blocks.steam.tile.TileSteamOven;
import com.zeitheron.mpc.ui.inv.ContainerSteamOven;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.util.ResourceLocation;

public class GuiSteamOven extends GuiContainer
{
	public final ResourceLocation gui = new ResourceLocation("mpc", "textures/gui/gui_steam_oven.png");
	private EntityPlayer player;
	private TileSteamOven oven;
	public DecimalFormat numberFormat = new DecimalFormat("#0.00");
	
	public GuiSteamOven(EntityPlayer player, TileSteamOven oven)
	{
		super(new ContainerSteamOven(player, oven));
		this.player = player;
		this.oven = oven;
		this.xSize = 176;
		this.ySize = 166;
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		drawDefaultBackground();
		super.drawScreen(mouseX, mouseY, partialTicks);
		renderHoveredToolTip(mouseX, mouseY);
	}
	
	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		this.mc.getTextureManager().bindTexture(this.gui);
		RenderUtil.drawTexturedModalRect(this.guiLeft, this.guiTop, 0.0, 0.0, this.xSize, this.ySize);
		for(int x = 0; x < 2; ++x)
		{
			for(int y = 0; y < 2; ++y)
			{
				int xp = this.guiLeft + 67 + x * 24;
				int yp = this.guiTop + 19 + y * 24;
				int id = x + y * 2;
				if(InterItemStack.isStackNull(this.oven.getStackInSlot(id)))
					continue;
				double progress = this.oven.steamValues[id] / (0.01 * this.oven.getStackInSlot(id).getCount()) * 16.0;
				RenderUtil.drawTexturedModalRect(xp, yp + 16 - progress, 176.0, 0.0, 16.0, progress);
			}
		}
		GL11.glEnable(3042);
		this.mc.getTextureManager().bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
		double steam = this.oven.steamStored / 2.0 * 65.0;
		double water = this.oven.waterBuf.getFluidAmount() / 8000.0 * 10.0;
		RenderUtil.drawTexturedModalRect(this.guiLeft + 8, this.guiTop + 8, this.mc.getTextureMapBlocks().getAtlasSprite("mpc:blocks/fluids/steam"), 5.0, steam);
		RenderUtil.drawTexturedModalRect(this.guiLeft + 24, this.guiTop + 77 - water, this.mc.getTextureMapBlocks().getAtlasSprite("minecraft:blocks/water_still"), 138.0, water);
	}
	
	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
	{
		if(mouseX >= this.guiLeft + 8 && mouseY >= this.guiTop + 8 && mouseX < this.guiLeft + 13 && mouseY <= this.guiTop + 8 + 65)
			this.drawHoveringText(Arrays.asList(I18n.format("gui.mpc:steam") + ": " + this.numberFormat.format(this.oven.steamStored).replaceAll("[.]", ",") + " " + I18n.format("gui.mpc:liters")), mouseX - this.guiLeft, mouseY - this.guiTop);
		if(mouseX >= this.guiLeft + 24 && mouseY >= this.guiTop + 67 && mouseX < this.guiLeft + 24 + 138 && mouseY <= this.guiTop + 77)
			this.drawHoveringText(Arrays.asList(Blocks.WATER.getLocalizedName() + ": " + this.numberFormat.format(this.oven.waterBuf.getFluidAmount() / 1000.0 * 20.0).replaceAll("[.]", ",") + " " + I18n.format("gui.mpc:liters")), mouseX - this.guiLeft, mouseY - this.guiTop);
	}
}