package com.zeitheron.mpc.ui.gui;

import java.text.DecimalFormat;
import java.util.Arrays;

import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.mpc.blocks.steam.tile.TileSteamBoiler;
import com.zeitheron.mpc.ui.inv.ContainerSteamBoiler;
import com.zeitheron.mpc.utils.PhysicsUtil;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.translation.I18n;

public class GuiSteamBoiler extends GuiContainer
{
	public final ResourceLocation gui = new ResourceLocation("mpc", "textures/gui/gui_blast_furnace.png");
	private EntityPlayer player;
	private TileSteamBoiler boiler;
	public DecimalFormat numberFormat = new DecimalFormat("#0.00");
	
	public GuiSteamBoiler(EntityPlayer player, TileSteamBoiler boiler)
	{
		super(new ContainerSteamBoiler(player, boiler));
		this.player = player;
		this.boiler = boiler;
		this.xSize = 176;
		this.ySize = 166;
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		drawDefaultBackground();
		super.drawScreen(mouseX, mouseY, partialTicks);
		renderHoveredToolTip(mouseX, mouseY);
	}
	
	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		this.mc.getTextureManager().bindTexture(this.gui);
		RenderUtil.drawTexturedModalRect(this.guiLeft, this.guiTop, 0.0, 0.0, this.xSize, this.ySize);
		RenderUtil.drawTexturedModalRect(this.guiLeft + 64.5, this.guiTop + 10.5, 176.0, 0.0, 13.0, 13.0);
		if(this.boiler.lastJFuelInjected != 0.0)
		{
			double fuel = this.boiler.jFuelLeft / this.boiler.lastJFuelInjected * 14.0;
			RenderUtil.drawTexturedModalRect(this.guiLeft + 64.5, this.guiTop + 24.5 - fuel, 176.0, 27.0 - fuel, 14.0, fuel);
		}
		double heat = this.boiler.waterTempC / PhysicsUtil.Material.WATER.getMeltableProperty().meltTemperature * 65.0;
		RenderUtil.drawTexturedModalRect(this.guiLeft + 33, this.guiTop + 8 + 65 - heat, 176.0, 92.0 - heat, 5.0, heat);
		this.mc.getTextureManager().bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
		double water = this.boiler.waterKG / 250.0 * 65.0;
		RenderUtil.drawTexturedModalRect(this.guiLeft + 8, this.guiTop + 8 + 65 - water, this.mc.getTextureMapBlocks().getAtlasSprite("minecraft:blocks/water_still"), 16.0, water);
	}
	
	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
	{
		if(mouseX >= this.guiLeft + 8 && mouseY >= this.guiTop + 8 && mouseX < this.guiLeft + 24 && mouseY <= this.guiTop + 8 + 65)
			this.drawHoveringText(Arrays.asList(I18n.translateToLocal("gui.mpc:fluid") + ": " + Blocks.WATER.getLocalizedName(), I18n.translateToLocal("gui.mpc:amount") + ": " + this.numberFormat.format(this.boiler.waterKG).replaceAll("[.]", ",") + " " + I18n.translateToLocal("gui.mpc:liters")), mouseX - this.guiLeft, mouseY - this.guiTop);
		if(mouseX >= this.guiLeft + 33 && mouseY >= this.guiTop + 8 && mouseX < this.guiLeft + 38 && mouseY <= this.guiTop + 8 + 65)
			this.drawHoveringText(Arrays.asList(I18n.translateToLocal("gui.mpc:temperature") + ": ", this.numberFormat.format(Math.min(this.boiler.waterTempC, PhysicsUtil.Material.WATER.getMeltableProperty().meltTemperature)).replaceAll("[.]", ",") + '\u00b0' + "C"), mouseX - this.guiLeft, mouseY - this.guiTop);
		if(mouseX >= this.guiLeft + 63 && mouseY >= this.guiTop + 9 && mouseX < this.guiLeft + 63 + 14 && mouseY <= this.guiTop + 24)
			this.drawHoveringText(Arrays.asList(I18n.translateToLocal("gui.mpc:fuel_left") + ":", PhysicsUtil.smartFormatJoules(this.boiler.jFuelLeft)), mouseX - this.guiLeft, mouseY - this.guiTop);
	}
}