package com.zeitheron.mpc.ui.gui;

import java.io.IOException;
import java.util.ArrayList;

import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.mpc.blocks.energy.tile.TileElectricalFurnace;
import com.zeitheron.mpc.blocks.energy.tile.TileElectricalMachineBase;
import com.zeitheron.mpc.blocks.energy.tile.TileExtractor;
import com.zeitheron.mpc.intr.jei.JeiIntegrator;
import com.zeitheron.mpc.ui.inv.ContainerElectricalBase;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.translation.I18n;

public class GuiElectricalFurnace extends GuiContainer
{
	public final ResourceLocation gui = new ResourceLocation("mpc", "textures/gui/gui_electrical_furnace.png");
	private EntityPlayer player;
	private TileElectricalMachineBase tile;
	
	public GuiElectricalFurnace(EntityPlayer player, TileElectricalMachineBase tile)
	{
		super(new ContainerElectricalBase(player, tile));
		this.player = player;
		this.tile = tile;
		this.xSize = 176;
		this.ySize = 166;
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		drawDefaultBackground();
		super.drawScreen(mouseX, mouseY, partialTicks);
		renderHoveredToolTip(mouseX, mouseY);
	}
	
	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		TileEntity worldTile = tile.getWorld().getTileEntity(tile.getPos());
		if(worldTile != tile)
		{
			tile = WorldUtil.cast(worldTile, TileElectricalMachineBase.class);
			if(tile == null)
			{
				mc.displayGuiScreen(null);
				return;
			}
		}
		this.mc.getTextureManager().bindTexture(gui);
		RenderUtil.drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
		if(this.tile.neededAmperes != 0.0)
		{
			RenderHelper.enableGUIStandardItemLighting();
			GlStateManager.enableBlend();
			GlStateManager.enableAlpha();
			GlStateManager.enableDepth();
			double transition = 1.0 - ((tile.amperesStoredLast + (tile.amperesStored - tile.amperesStoredLast) * partialTicks) / this.tile.neededAmperes);
			double scale = transition;
			GL11.glPushMatrix();
			GL11.glTranslated(this.guiLeft + 59 + 90.0 * (1.0 - transition), this.guiTop + 34, 0.0);
			GL11.glPushMatrix();
			GL11.glTranslated(0.0, 0.0, -(1.0 - transition) * 2.0);
			double t = (1.0 - scale) * 8.0;
			GL11.glTranslated(t, t, 0.0);
			GL11.glScaled(scale, scale, 1.0);
			this.itemRender.renderItemAndEffectIntoGUI(this.tile.getStackInSlot(0), 0, 0);
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			GL11.glTranslated(0.0, 0.0, (1.0 - transition) * 4.0);
			scale = 1 - scale;
			t = (1.0 - scale) * 8.0;
			GL11.glTranslated(t, t, 0.0);
			GL11.glScaled(scale, scale, 1.0);
			this.itemRender.renderItemAndEffectIntoGUI(this.tile.getOutput(this.tile.getStackInSlot(0)), 0, 0);
			GL11.glColor4d(1.0, 1.0, 1.0, 1.0);
			GL11.glPopMatrix();
			GL11.glPopMatrix();
		}
	}
	
	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
	{
		if(mouseX >= this.guiLeft + 78 && mouseY >= this.guiTop + 34 && mouseX < this.guiLeft + 146 && mouseY < this.guiTop + 50)
		{
			ArrayList<String> tip = new ArrayList<String>();
			tip.add(I18n.translateToLocal("gui.mpc:progress") + ":");
			tip.add("" + (this.tile.neededAmperes == 0.0 ? 100 : (int) (this.tile.amperesStored / this.tile.neededAmperes * 100.0)) + "%");
			if(JeiIntegrator.canWork())
				tip.add(I18n.translateToLocal("jei.tooltip.show.recipes"));
			drawHoveringText(tip, mouseX - guiLeft, mouseY - guiTop);
		}
		GlStateManager.disableLighting();
		GlStateManager.enableAlpha();
		mc.getTextureManager().bindTexture(gui);
		
		double energy = (tile.amperesCharge / tile.amperesChargeMax) * 53;
		RenderUtil.drawTexturedModalRect(8, 14 + 53 - energy, xSize + 8, 53 - energy, 7, energy);
		
		RenderUtil.drawTexturedModalRect(8, 14, xSize, 0, 7, 53);
	}
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
		if(mouseX >= this.guiLeft + 78 && mouseY >= this.guiTop + 34 && mouseX < this.guiLeft + 146 && mouseY < this.guiTop + 50)
		{
			if(this.tile instanceof TileElectricalFurnace)
				JeiIntegrator.showRecipes("minecraft.smelting");
			if(this.tile instanceof TileExtractor)
				JeiIntegrator.showRecipes(JeiIntegrator.EXTRACTOR);
		}
		super.mouseClicked(mouseX, mouseY, mouseButton);
	}
}