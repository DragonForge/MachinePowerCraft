package com.zeitheron.mpc.ui.gui;

import java.io.IOException;
import java.util.ArrayList;

import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.mpc.blocks.kinetic.tile.TileMechanicalCrusher;
import com.zeitheron.mpc.intr.jei.JeiIntegrator;
import com.zeitheron.mpc.ui.inv.ContainerMechanicalCrusher;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.translation.I18n;

public class GuiMechanicalCrusher extends GuiContainer
{
	public final ResourceLocation gui = new ResourceLocation("mpc", "textures/gui/gui_mechanical_crusher.png");
	private EntityPlayer player;
	private TileMechanicalCrusher crusher;
	
	public GuiMechanicalCrusher(EntityPlayer player, TileMechanicalCrusher crusher)
	{
		super(new ContainerMechanicalCrusher(player, crusher));
		this.player = player;
		this.crusher = crusher;
		this.xSize = 176;
		this.ySize = 166;
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		drawDefaultBackground();
		super.drawScreen(mouseX, mouseY, partialTicks);
		renderHoveredToolTip(mouseX, mouseY);
	}
	
	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		this.mc.getTextureManager().bindTexture(this.gui);
		RenderUtil.drawTexturedModalRect(this.guiLeft, this.guiTop, 0.0, 0.0, this.xSize, this.ySize);
		double deg = this.crusher.degrees / 1440.0 * 22.0;
		RenderUtil.drawTexturedModalRect(this.guiLeft + 78, this.guiTop + 29, 176.0, 0.0, deg, 15.0);
	}
	
	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
	{
		if(mouseX >= this.guiLeft + 78 && mouseY >= this.guiTop + 29 && mouseX < this.guiLeft + 100 && mouseY < this.guiTop + 44)
		{
			ArrayList<String> tip = new ArrayList<String>();
			tip.add(I18n.translateToLocal("gui.mpc:progress") + ":");
			tip.add("" + (int) (this.crusher.degrees / 1440.0 * 100.0) + "%");
			if(JeiIntegrator.canWork())
			{
				tip.add(I18n.translateToLocal("jei.tooltip.show.recipes"));
			}
			this.drawHoveringText(tip, mouseX - this.guiLeft, mouseY - this.guiTop);
		}
	}
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
		if(mouseX >= this.guiLeft + 78 && mouseY >= this.guiTop + 29 && mouseX < this.guiLeft + 100 && mouseY < this.guiTop + 44)
			JeiIntegrator.showRecipes(JeiIntegrator.CRUSHER);
		super.mouseClicked(mouseX, mouseY, mouseButton);
	}
}
