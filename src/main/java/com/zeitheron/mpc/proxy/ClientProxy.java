package com.zeitheron.mpc.proxy;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.annotation.Nonnull;

import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.api.multipart.MultipartRenderingRegistry;
import com.zeitheron.hammercore.api.multipart.MultipartSignature;
import com.zeitheron.hammercore.internal.blocks.multipart.BlockMultipart;
import com.zeitheron.hammercore.internal.blocks.multipart.TileMultipart;
import com.zeitheron.hammercore.internal.init.BlocksHC;
import com.zeitheron.hammercore.lib.zlib.error.JSONException;
import com.zeitheron.hammercore.lib.zlib.json.JSONArray;
import com.zeitheron.hammercore.lib.zlib.json.JSONTokener;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.hammercore.utils.math.vec.Cuboid6;
import com.zeitheron.mpc.MachinePowerCraft;
import com.zeitheron.mpc.api.energy.SetupWireRender;
import com.zeitheron.mpc.api.energy.impl.wire.MultipartAmpereWireBase;
import com.zeitheron.mpc.blocks.kinetic.BlockKineticShaft;
import com.zeitheron.mpc.blocks.kinetic.BlockManualCrank;
import com.zeitheron.mpc.blocks.kinetic.tile.TileKineticGenerator;
import com.zeitheron.mpc.blocks.kinetic.tile.TileKineticShaft;
import com.zeitheron.mpc.blocks.kinetic.tile.TileManualCrank;
import com.zeitheron.mpc.blocks.kinetic.tile.TileMechanicalLever;
import com.zeitheron.mpc.blocks.steam.tile.TileSteamEngine;
import com.zeitheron.mpc.client.color.items.MultiMaterialItemHandler;
import com.zeitheron.mpc.client.render.multipart.MultipartAbstractWireRender;
import com.zeitheron.mpc.client.render.multipart.MultipartSteamPipeRender;
import com.zeitheron.mpc.client.render.tile.TESRKineticGenerator;
import com.zeitheron.mpc.client.render.tile.TESRKineticShaft;
import com.zeitheron.mpc.client.render.tile.TESRManualCrank;
import com.zeitheron.mpc.client.render.tile.TESRMechanicalLever;
import com.zeitheron.mpc.client.render.tile.TESRSteamEngine;
import com.zeitheron.mpc.event.TooltipListener;
import com.zeitheron.mpc.init.ItemsMPC;
import com.zeitheron.mpc.items.multipart.MultipartSteamPipe;
import com.zeitheron.mpc.items.multipart.MultipartSteamPipeConnection;

import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderGlobal;
import net.minecraft.client.renderer.color.BlockColors;
import net.minecraft.client.renderer.color.ItemColors;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraftforge.client.event.DrawBlockHighlightEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.discovery.ASMDataTable;
import net.minecraftforge.fml.common.discovery.ASMDataTable.ASMData;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class ClientProxy extends CommonProxy
{
	ASMDataTable table;
	
	@Override
	public void preInit(ASMDataTable table)
	{
		this.table = table;
		
		MinecraftForge.EVENT_BUS.register(new TooltipListener());
	}
	
	@Override
	public void init()
	{
		ItemColors itemColors = Minecraft.getMinecraft().getItemColors();
		BlockColors blockColors = Minecraft.getMinecraft().getBlockColors();
		for(Item it : ItemsMPC.MULTI_MATERIALS)
			itemColors.registerItemColorHandler(new MultiMaterialItemHandler(), it);
		ClientRegistry.bindTileEntitySpecialRenderer(TileSteamEngine.class, new TESRSteamEngine());
		ClientRegistry.bindTileEntitySpecialRenderer(TileKineticShaft.class, new TESRKineticShaft());
		ClientRegistry.bindTileEntitySpecialRenderer(TileMechanicalLever.class, new TESRMechanicalLever());
		ClientRegistry.bindTileEntitySpecialRenderer(TileManualCrank.class, new TESRManualCrank());
		ClientRegistry.bindTileEntitySpecialRenderer(TileKineticGenerator.class, new TESRKineticGenerator());
		MultipartRenderingRegistry.bindSpecialMultipartRender(MultipartSteamPipe.class, new MultipartSteamPipeRender());
		List<Class<? extends MultipartAmpereWireBase>> setups = ClientProxy.getClasses(this.table, SetupWireRender.class, MultipartAmpereWireBase.class);
		for(Class<? extends MultipartAmpereWireBase> cls : setups)
			MultipartRenderingRegistry.bindSpecialMultipartRender(cls, new MultipartAbstractWireRender());
		try
		{
			ClientProxy.registerBlockAtlas(MachinePowerCraft.getResourceAsText("assets/mpc/models/block_atlas.json").replaceAll("%modid%", "mpc"));
		} catch(JSONException e)
		{
			e.printStackTrace();
		}
		// ItemRenderingHandler.INSTANCE.appendItemRender(ItemsMPC.GEM, new
		// ItemGemRender());
	}
	
	public static int registerBlockAtlas(String json) throws JSONException
	{
		JSONArray array = (JSONArray) new JSONTokener(json).nextValue();
		int total = 0;
		for(int i = 0; i < array.length(); ++i)
		{
			Minecraft.getMinecraft().getTextureMapBlocks().registerSprite(new ResourceLocation(array.getString(i)));
			++total;
		}
		return total;
	}
	
	@Override
	public boolean isShiftKeyDown_forTooltip()
	{
		return GuiScreen.isShiftKeyDown();
	}
	
	@SubscribeEvent
	public void drawHitbox(DrawBlockHighlightEvent evt)
	{
		try
		{
			RayTraceResult rtl = evt.getTarget();
			if(rtl == null || rtl.getBlockPos() == null || evt.getPlayer() == null || evt.getPlayer().world == null)
			{
				return;
			}
			BlockPos pos = rtl.getBlockPos();
			TileEntity tile = evt.getPlayer().world.getTileEntity(pos);
			IBlockState state = evt.getPlayer().world.getBlockState(pos);
			TileMultipart tmp = WorldUtil.cast((Object) tile, TileMultipart.class);
			Cuboid6 cbd = ((BlockMultipart) BlocksHC.MULTIPART).getCuboidFromPlayer(evt.getPlayer(), pos);
			GL11.glPushMatrix();
			GL11.glEnable(3042);
			GL11.glTranslated(pos.getX() - TileEntityRendererDispatcher.staticPlayerX, pos.getY() - TileEntityRendererDispatcher.staticPlayerY, pos.getZ() - TileEntityRendererDispatcher.staticPlayerZ);
			GlStateManager.enableBlend();
			GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
			GlStateManager.disableTexture2D();
			GlStateManager.depthMask(false);
			if(tmp != null && cbd != null)
			{
				MultipartSignature signature = tmp.getSignature(cbd.center().toVec3d());
				if(signature instanceof MultipartSteamPipeConnection)
				{
					signature = tmp.getSignature(MultipartSteamPipe.MAIN_POS);
				}
				if(signature instanceof MultipartSteamPipe)
				{
					RenderGlobal.drawSelectionBoundingBox(signature.getBoundingBox().grow(0.0020000000949949026), 0.0f, 0.0f, 0.0f, 0.35f);
					for(EnumFacing facing : EnumFacing.VALUES)
					{
						MultipartSignature conn = tmp.getSignature(MultipartSteamPipeConnection.getPosition(facing));
						if(!(conn instanceof MultipartSteamPipeConnection))
							continue;
						RenderGlobal.drawSelectionBoundingBox(conn.getBoundingBox().grow(0.0020000000949949026), 0.0f, 0.0f, 0.0f, 0.35f);
					}
					evt.setCanceled(true);
				}
			}
			if(tile instanceof TileKineticShaft)
			{
				TileKineticShaft shaft = (TileKineticShaft) tile;
				EnumFacing.Axis a = state.getValue(BlockKineticShaft.DIR);
				evt.setCanceled(true);
				GL11.glPushMatrix();
				GL11.glTranslated(0.5, 1.0, 0.5);
				if(a == EnumFacing.Axis.Z)
				{
					GL11.glTranslated(0.0, -0.5, 0.5);
					GL11.glRotated(90.0, 1.0, 0.0, 0.0);
				}
				if(a == EnumFacing.Axis.X)
				{
					GL11.glTranslated(-0.5, -0.5, 0.0);
					GL11.glRotated(90.0, 0.0, 0.0, 1.0);
				}
				GL11.glRotated(shaft.rotation, 0.0, 1.0, 0.0);
				GL11.glRotatef(180.0f, 0.0f, 0.0f, 1.0f);
				GL11.glPushMatrix();
				GL11.glTranslated(-0.5, 0.0, -0.5);
				RenderGlobal.drawSelectionBoundingBox(BlockKineticShaft.AABB_Y.grow(0.0020000000949949026), 0.0f, 0.0f, 0.0f, 0.35f);
				GL11.glPopMatrix();
				GL11.glPopMatrix();
			}
			if(tile instanceof TileManualCrank)
			{
				TileManualCrank shaft = (TileManualCrank) tile;
				evt.setCanceled(true);
				GL11.glPushMatrix();
				GL11.glTranslated(0.5, 1.0, 0.5);
				GL11.glRotated(shaft.rotation, 0.0, 1.0, 0.0);
				GL11.glRotatef(180.0f, 0.0f, 0.0f, 1.0f);
				GL11.glPushMatrix();
				GL11.glTranslated(-0.5, 0.0, -0.5);
				RenderGlobal.drawSelectionBoundingBox(BlockManualCrank.aabb.grow(0.0020000000949949026), 0.0f, 0.0f, 0.0f, 0.35f);
				GL11.glPopMatrix();
				GL11.glPopMatrix();
			}
			GlStateManager.depthMask(true);
			GlStateManager.enableTexture2D();
			GlStateManager.disableBlend();
			GL11.glPopMatrix();
		} catch(Throwable rtl)
		{
			// empty catch block
		}
	}
	
	public static <T> List<Class<? extends T>> getClasses(@Nonnull ASMDataTable asmDataTable, Class annotationClass, Class<T> instanceClass)
	{
		String annotationClassName = annotationClass.getCanonicalName();
		Set<ASMData> asmDatas = asmDataTable.getAll(annotationClassName);
		ArrayList<Class<? extends T>> instances = new ArrayList<>();
		for(ASMData asmData : asmDatas)
		{
			try
			{
				Class asmClass = Class.forName(asmData.getClassName());
				Class<T> asmInstanceClass = asmClass.asSubclass(instanceClass);
				instances.add(asmInstanceClass);
			} catch(Throwable asmClass)
			{
			}
		}
		return instances;
	}
}