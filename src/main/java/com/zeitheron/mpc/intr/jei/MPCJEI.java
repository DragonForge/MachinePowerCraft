package com.zeitheron.mpc.intr.jei;

import java.util.List;

import com.zeitheron.mpc.api.EnumMachineType;
import com.zeitheron.mpc.api.recipes.compressor.CompressorRecipes;
import com.zeitheron.mpc.api.recipes.compressor.RecipesCompressor;
import com.zeitheron.mpc.api.recipes.crusher.CrusherRecipe;
import com.zeitheron.mpc.api.recipes.crusher.RecipesCrusher;
import com.zeitheron.mpc.api.recipes.extractor.ExtractorRecipe;
import com.zeitheron.mpc.api.recipes.extractor.RecipesExtractor;
import com.zeitheron.mpc.init.BlocksMPC;
import com.zeitheron.mpc.intr.jei.compressor.CompressorCategory;
import com.zeitheron.mpc.intr.jei.compressor.CompressorWrapper;
import com.zeitheron.mpc.intr.jei.crusher.CrusherCategory;
import com.zeitheron.mpc.intr.jei.crusher.CrusherWrapper;
import com.zeitheron.mpc.intr.jei.extractor.ExtractorCategory;
import com.zeitheron.mpc.intr.jei.extractor.ExtractorWrapper;

import mezz.jei.api.IJeiRuntime;
import mezz.jei.api.IModPlugin;
import mezz.jei.api.IModRegistry;
import mezz.jei.api.JEIPlugin;
import mezz.jei.api.recipe.IRecipeCategoryRegistration;
import net.minecraft.item.ItemStack;

@JEIPlugin
public class MPCJEI implements IModPlugin, IJEIRuntimeMPC
{
	public static IJeiRuntime RUNTIME;
	
	@Override
	public void onRuntimeAvailable(IJeiRuntime runtime)
	{
		RUNTIME = runtime;
		JeiIntegrator.runtime = this;
	}
	
	@Override
	public void register(IModRegistry reg)
	{
		reg.handleRecipes(CrusherRecipe.class, t -> new CrusherWrapper(t), JeiIntegrator.CRUSHER);
		reg.handleRecipes(CompressorRecipes.class, t -> new CompressorWrapper(t), JeiIntegrator.COMPRESSOR);
		reg.handleRecipes(ExtractorRecipe.class, t -> new ExtractorWrapper(t), JeiIntegrator.EXTRACTOR);
		
		reg.addRecipeCatalyst(new ItemStack(BlocksMPC.MECHANICAL_CRUSHER), JeiIntegrator.CRUSHER);
		reg.addRecipeCatalyst(new ItemStack(BlocksMPC.ELECTRICAL_CRUSHER), JeiIntegrator.CRUSHER);
		reg.addRecipeCatalyst(new ItemStack(BlocksMPC.MECHANICAL_COMPRESSOR), JeiIntegrator.COMPRESSOR);
		reg.addRecipeCatalyst(new ItemStack(BlocksMPC.EXTRACTOR), JeiIntegrator.EXTRACTOR);
		
		for(EnumMachineType type : EnumMachineType.values())
		{
			reg.addRecipes(RecipesCrusher.get(type).entries(), JeiIntegrator.CRUSHER);
			reg.addRecipes(RecipesCompressor.get(type).entries(), JeiIntegrator.COMPRESSOR);
			reg.addRecipes(RecipesExtractor.get(type).entries(), JeiIntegrator.EXTRACTOR);
		}
	}
	
	@Override
	public void registerCategories(IRecipeCategoryRegistration reg)
	{
		reg.addRecipeCategories(new CrusherCategory());
		reg.addRecipeCategories(new CompressorCategory());
		reg.addRecipeCategories(new ExtractorCategory());
	}
	
	@Override
	public void showRecipeCategories(List<String> cats)
	{
		RUNTIME.getRecipesGui().showCategories(cats);
	}
}