package com.zeitheron.mpc.intr.jei.compressor;

import com.zeitheron.mpc.api.recipes.compressor.CompressorRecipes;

import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IRecipeWrapper;
import net.minecraft.item.ItemStack;

public class CompressorWrapper implements IRecipeWrapper
{
	public final CompressorRecipes entry;
	
	public CompressorWrapper(CompressorRecipes entry)
	{
		this.entry = entry;
	}
	
	@Override
	public void getIngredients(IIngredients ing)
	{
		ing.setOutput(ItemStack.class, this.entry.getOutput());
		ing.setInput(ItemStack.class, this.entry.getInput());
	}
}