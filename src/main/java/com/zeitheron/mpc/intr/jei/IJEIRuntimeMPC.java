package com.zeitheron.mpc.intr.jei;

import java.util.List;

public interface IJEIRuntimeMPC
{
	public void showRecipeCategories(List<String> var1);
}