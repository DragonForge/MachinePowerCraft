package com.zeitheron.mpc.intr.jei.compressor;

import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.client.utils.UV;
import com.zeitheron.mpc.api.recipes.compressor.CompressorRecipes;
import com.zeitheron.mpc.intr.jei.JeiIntegrator;
import com.zeitheron.mpc.reference.ModInfo;

import mezz.jei.api.gui.IDrawable;
import mezz.jei.api.gui.IGuiItemStackGroup;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IRecipeCategory;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.oredict.OreDictionary;

public class CompressorCategory implements IRecipeCategory<CompressorWrapper>
{
	public static final UV backgound = new UV(new ResourceLocation("mpc", "textures/gui/jei_crusher.png"), 0.0, 59.0, 168.0, 58.0);
	IDrawable bg;
	
	public CompressorCategory()
	{
		this.bg = new IDrawable()
		{
			
			@Override
			public int getWidth()
			{
				return (int) CompressorCategory.backgound.width;
			}
			
			@Override
			public int getHeight()
			{
				return (int) CompressorCategory.backgound.height;
			}
			
			@Override
			public void draw(Minecraft arg0, int arg1, int arg2)
			{
				CompressorCategory.backgound.render(arg1, arg2);
			}
			
			@Override
			public void draw(Minecraft arg0)
			{
				CompressorCategory.backgound.render(0.0, 0.0);
			}
		};
	}
	
	@Override
	public void drawExtras(Minecraft mc)
	{
		double progress = (Minecraft.getSystemTime() + this.hashCode()) % 5000 / 5000.0;
		RenderUtil.drawTexturedModalRect(87.0, 22.0, 168.0, 15.0, progress * 21.0, 15.0);
	}
	
	@Override
	public IDrawable getBackground()
	{
		return this.bg;
	}
	
	@Override
	public IDrawable getIcon()
	{
		return null;
	}
	
	@Override
	public String getTitle()
	{
		return I18n.translateToLocal("jei.mpc:compressor.title");
	}
	
	@Override
	public String getUid()
	{
		return JeiIntegrator.COMPRESSOR;
	}
	
	@Override
	public void setRecipe(IRecipeLayout lay, CompressorWrapper wrp, IIngredients ings)
	{
		CompressorRecipes ce = wrp.entry;
		IGuiItemStackGroup g = lay.getItemStacks();
		g.init(0, false, 25, 20);
		g.set(0, ce.getCompressor());
		g.init(1, true, 52, 20);
		Object in = ce.getInput();
		if(in instanceof ItemStack)
		{
			g.set(1, (ItemStack) in);
		}
		if(in instanceof String)
		{
			g.set(1, OreDictionary.getOres(((String) in)));
		}
		g.init(2, false, 124, 20);
		g.set(2, ce.getOutput());
	}
	
	@Override
	public String getModName()
	{
		return ModInfo.MOD_NAME;
	}
}