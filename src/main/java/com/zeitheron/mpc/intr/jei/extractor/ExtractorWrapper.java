package com.zeitheron.mpc.intr.jei.extractor;

import com.zeitheron.mpc.api.recipes.extractor.ExtractorRecipe;

import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IRecipeWrapper;
import net.minecraft.item.ItemStack;

public class ExtractorWrapper implements IRecipeWrapper
{
	public ExtractorRecipe entry;
	
	public ExtractorWrapper(ExtractorRecipe entry)
	{
		this.entry = entry;
	}
	
	@Override
	public void getIngredients(IIngredients ing)
	{
		ing.setOutput(ItemStack.class, this.entry.getOutput());
		ing.setInput(ItemStack.class, this.entry.getInput());
	}
}