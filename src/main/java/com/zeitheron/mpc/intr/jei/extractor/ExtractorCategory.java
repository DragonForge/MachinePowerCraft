package com.zeitheron.mpc.intr.jei.extractor;

import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.client.utils.UV;
import com.zeitheron.mpc.init.BlocksMPC;
import com.zeitheron.mpc.intr.jei.JeiIntegrator;
import com.zeitheron.mpc.reference.ModInfo;

import mezz.jei.api.gui.IDrawable;
import mezz.jei.api.gui.IGuiItemStackGroup;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IRecipeCategory;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.translation.I18n;

public class ExtractorCategory implements IRecipeCategory<ExtractorWrapper>
{
	public static final UV backgound = new UV(new ResourceLocation("mpc", "textures/gui/jei_crusher.png"), 0.0, 118.0, 168.0, 58.0);
	IDrawable bg;
	
	public ExtractorCategory()
	{
		this.bg = new IDrawable()
		{
			@Override
			public int getWidth()
			{
				return (int) ExtractorCategory.backgound.width;
			}

			@Override
			public int getHeight()
			{
				return (int) ExtractorCategory.backgound.height;
			}

			@Override
			public void draw(Minecraft arg0, int arg1, int arg2)
			{
				ExtractorCategory.backgound.render(arg1, arg2);
			}

			@Override
			public void draw(Minecraft arg0)
			{
				ExtractorCategory.backgound.render(0.0, 0.0);
			}
		};
	}

	@Override
	public void drawExtras(Minecraft mc)
	{
		double progress = (Minecraft.getSystemTime() + this.hashCode()) % 5000 / 5000.0;
		RenderUtil.drawTexturedModalRect(76.0, 22.0, 168.0, 30.0, progress * 21.0, 15.0);
	}

	@Override
	public IDrawable getBackground()
	{
		return this.bg;
	}

	@Override
	public IDrawable getIcon()
	{
		return null;
	}
	
	@Override
	public String getTitle()
	{
		return I18n.translateToLocal((BlocksMPC.EXTRACTOR.getUnlocalizedName() + ".name"));
	}

	@Override
	public String getUid()
	{
		return JeiIntegrator.EXTRACTOR;
	}

	@Override
	public void setRecipe(IRecipeLayout lay, ExtractorWrapper wrp, IIngredients ings)
	{
		IGuiItemStackGroup g = lay.getItemStacks();
		g.init(0, false, 40, 20);
		g.set(0, wrp.entry.getInput());
		g.init(1, false, 113, 20);
		g.set(1, wrp.entry.getOutput());
	}
	
	@Override
	public String getModName()
	{
		return ModInfo.MOD_NAME;
	}
}