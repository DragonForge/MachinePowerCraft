package com.zeitheron.mpc.intr.jei.crusher;

import com.zeitheron.mpc.api.recipes.crusher.CrusherRecipe;

import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IRecipeWrapper;
import net.minecraft.item.ItemStack;

public class CrusherWrapper implements IRecipeWrapper
{
	public final CrusherRecipe entry;
	
	public CrusherWrapper(CrusherRecipe entry)
	{
		this.entry = entry;
	}
	
	@Override
	public void getIngredients(IIngredients ing)
	{
		ing.setOutput(ItemStack.class, this.entry.getOutput());
		ing.setInput(ItemStack.class, this.entry.getInput());
	}
}