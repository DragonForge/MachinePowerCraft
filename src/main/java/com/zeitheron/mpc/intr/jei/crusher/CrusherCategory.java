package com.zeitheron.mpc.intr.jei.crusher;

import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.client.utils.UV;
import com.zeitheron.mpc.api.recipes.crusher.CrusherRecipe;
import com.zeitheron.mpc.intr.jei.JeiIntegrator;
import com.zeitheron.mpc.reference.ModInfo;

import mezz.jei.api.gui.IDrawable;
import mezz.jei.api.gui.IGuiItemStackGroup;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IRecipeCategory;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.oredict.OreDictionary;

public class CrusherCategory implements IRecipeCategory<CrusherWrapper>
{
	public static final UV backgound = new UV(new ResourceLocation("mpc", "textures/gui/jei_crusher.png"), 0.0, 0.0, 168.0, 58.0);
	IDrawable bg;
	
	public CrusherCategory()
	{
		this.bg = new IDrawable()
		{
			
			@Override
			public int getWidth()
			{
				return (int) CrusherCategory.backgound.width;
			}
			
			@Override
			public int getHeight()
			{
				return (int) CrusherCategory.backgound.height;
			}
			
			@Override
			public void draw(Minecraft arg0, int arg1, int arg2)
			{
				CrusherCategory.backgound.render(arg1, arg2);
			}
			
			@Override
			public void draw(Minecraft arg0)
			{
				CrusherCategory.backgound.render(0.0, 0.0);
			}
		};
	}
	
	@Override
	public void drawExtras(Minecraft mc)
	{
		double progress = (Minecraft.getSystemTime() + this.hashCode()) % 5000 / 5000.0;
		RenderUtil.drawTexturedModalRect(87.0, 22.0, 168.0, 0.0, progress * 22.0, 15.0);
	}
	
	@Override
	public IDrawable getBackground()
	{
		return this.bg;
	}
	
	@Override
	public IDrawable getIcon()
	{
		return null;
	}
	
	@Override
	public String getTitle()
	{
		return I18n.translateToLocal("jei.mpc:crusher.title");
	}
	
	@Override
	public String getUid()
	{
		return JeiIntegrator.CRUSHER;
	}
	
	@Override
	public void setRecipe(IRecipeLayout lay, CrusherWrapper wrp, IIngredients ings)
	{
		CrusherRecipe ce = wrp.entry;
		IGuiItemStackGroup g = lay.getItemStacks();
		g.init(0, false, 25, 20);
		g.set(0, ce.getCrusher());
		g.init(1, true, 52, 20);
		Object in = ce.getInput();
		if(in instanceof ItemStack)
		{
			g.set(1, (ItemStack) in);
		}
		if(in instanceof String)
		{
			g.set(1, OreDictionary.getOres(((String) in)));
		}
		g.init(2, false, 124, 20);
		g.set(2, ce.getOutput());
	}
	
	@Override
	public String getModName()
	{
		return ModInfo.MOD_NAME;
	}
}