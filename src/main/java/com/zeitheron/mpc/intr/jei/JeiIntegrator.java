package com.zeitheron.mpc.intr.jei;

import java.util.Arrays;

import net.minecraftforge.fml.common.Loader;

public class JeiIntegrator
{
	static IJEIRuntimeMPC runtime;
	public static final String CRUSHER = "mpc:crusher";
	public static final String COMPRESSOR = "mpc:compressor";
	public static final String EXTRACTOR = "mpc:extractor";
	
	public static boolean canWork()
	{
		return Loader.isModLoaded("jei");
	}
	
	public static void showRecipes(String... categories)
	{
		if(runtime != null)
			runtime.showRecipeCategories(Arrays.asList(categories));
	}
}