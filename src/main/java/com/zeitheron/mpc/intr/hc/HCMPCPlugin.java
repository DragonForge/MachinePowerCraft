package com.zeitheron.mpc.intr.hc;

import com.zeitheron.hammercore.api.mhb.IRayCubeRegistry;
import com.zeitheron.hammercore.api.mhb.IRayRegistry;
import com.zeitheron.hammercore.api.mhb.RaytracePlugin;
import com.zeitheron.mpc.blocks.kinetic.BlockKineticGenerator;
import com.zeitheron.mpc.init.BlocksMPC;

@RaytracePlugin
public class HCMPCPlugin implements IRayRegistry
{
	@Override
	public void registerCubes(IRayCubeRegistry cube)
	{
		BlockKineticGenerator gen = (BlockKineticGenerator) BlocksMPC.KINETIC_GENERATOR;
		cube.bindBlockCubeManager(gen, gen);
	}
}