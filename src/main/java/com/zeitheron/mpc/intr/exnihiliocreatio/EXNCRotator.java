package com.zeitheron.mpc.intr.exnihiliocreatio;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.mpc.MachinePowerCraft;

import net.minecraft.tileentity.TileEntity;

public class EXNCRotator
{
	private static final Class RotationalPowerMember;
	private static final Method RPM_addEffectiveRotation;
	
	static
	{
		MachinePowerCraft.LOG.info("Hooking to ExNihilioCreatio...");
		
		Class ct = null;
		Method mt = null;
		
		try
		{
			ct = Class.forName("exnihilocreatio.rotationalPower.IRotationalPowerMember");
			MachinePowerCraft.LOG.info(" -Found rotational member API at " + ct.getName());
			
			mt = ct.getMethod("addEffectiveRotation", float.class);
			
			if(mt != null)
				MachinePowerCraft.LOG.info("  -Found kinetic acceptor: " + mt);
			else
				MachinePowerCraft.LOG.info("  -Kinetic acceptor method not found!");
			
			MachinePowerCraft.LOG.info("ExNihilioCreatio hook complete!");
		} catch(Throwable e)
		{
			MachinePowerCraft.LOG.info("ExNihilioCreatio not found!");
		}
		
		RotationalPowerMember = ct;
		RPM_addEffectiveRotation = mt;
	}
	
	public static void hook()
	{
		
	}
	
	public static boolean isRotateable(TileEntity tile)
	{
		if(RotationalPowerMember == null || tile == null)
			return false;
		return WorldUtil.cast(tile, RotationalPowerMember) != null;
	}
	
	public static boolean addEffectiveRotation(TileEntity tile, float rotation)
	{
		if(RotationalPowerMember == null)
			return false;
		
		Object consumer = WorldUtil.cast(tile, RotationalPowerMember);
		
		if(consumer != null)
		{
			call(consumer, RPM_addEffectiveRotation, rotation);
			return true;
		}
		
		return false;
	}
	
	private static Object call(Object instance, Method m, Object... args)
	{
		if(m == null || (instance == null && !Modifier.isStatic(m.getModifiers())))
			return null;
		if(args == null)
			args = new Object[0];
		try
		{
			m.setAccessible(true);
			return m.invoke(instance, args);
		} catch(ReflectiveOperationException e)
		{
		}
		return null;
	}
}