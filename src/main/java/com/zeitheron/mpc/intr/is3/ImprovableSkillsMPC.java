package com.zeitheron.mpc.intr.is3;

import com.zeitheron.improvableskills.api.PlayerSkillBase;
import com.zeitheron.improvableskills.data.PlayerDataManager;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.registries.IForgeRegistry;

class ImprovableSkillsMPC implements iImprSkls
{
	@Override
	public int getSkillLvl(EntityPlayer player, ResourceLocation res)
	{
		return PlayerDataManager.getDataFor(player).getSkillLevel(GameRegistry.findRegistry(PlayerSkillBase.class).getValue(res));
	}
	
	@Override
	public void register()
	{
		IForgeRegistry<PlayerSkillBase> reg = GameRegistry.findRegistry(PlayerSkillBase.class);
		
		PlayerSkillBase sk = new PlayerSkillBase(25).setRegistryName(ImprovableSkillsAPI.ELECTRICAL_DEFLECTOR);
		sk.xpValue = 3;
		reg.register(sk);
	}
}