package com.zeitheron.mpc.intr.is3;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;

public interface iImprSkls
{
	iImprSkls DEFAULT = new iImprSkls()
	{
		@Override
		public void register()
		{
		}
		
		@Override
		public int getSkillLvl(EntityPlayer player, ResourceLocation res)
		{
			return 0;
		}
	};
	
	void register();
	
	int getSkillLvl(EntityPlayer player, ResourceLocation res);
}