package com.zeitheron.mpc.intr.is3;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.Loader;

public class ImprovableSkillsAPI
{
	public static final ResourceLocation //
	ELECTRICAL_DEFLECTOR = new ResourceLocation("mpc", "electrical_deflector");
	
	private static iImprSkls skills;
	
	public static iImprSkls getSkills()
	{
		if(skills == null && Loader.isModLoaded("improvableskills"))
			try
			{
				skills = new ImprovableSkillsMPC();
			} catch(Throwable err)
			{
			}
		return skills == null ? iImprSkls.DEFAULT : skills;
	}
}