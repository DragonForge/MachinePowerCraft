package com.zeitheron.mpc.client.render.tile;

import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.client.render.tesr.TESR;
import com.zeitheron.mpc.blocks.kinetic.tile.TileMechanicalLever;
import com.zeitheron.mpc.client.model.ModelShaft;

import net.minecraft.block.state.IBlockState;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;

public class TESRMechanicalLever extends TESR<TileMechanicalLever>
{
	public final ModelShaft shaft = new ModelShaft();
	
	@Override
	public void renderTileEntityAt(TileMechanicalLever te, double x, double y, double z, float partialTicks, ResourceLocation destroyStage, float alpha)
	{
		GL11.glPushMatrix();
		GL11.glTranslated(x + 0.5, y + 1.0, z + 0.5);
		for(EnumFacing facing : te.CONNECTIONS)
		{
			GL11.glPushMatrix();
			GL11.glTranslatef(facing.getFrontOffsetX() == 1 ? 0.5f : 0.0f, facing.getFrontOffsetY() == -1 ? -0.5f : 0.0f, facing.getFrontOffsetZ() == -1 ? -0.5f : 0.0f);
			shaft.bindTexture(facing.getAxis());
			this.renderSide(facing, te, destroyStage);
			GL11.glPopMatrix();
		}
		GL11.glPopMatrix();
	}
	
	public void renderSide(EnumFacing facing, TileMechanicalLever te, ResourceLocation destroy)
	{
		IBlockState state = te.getWorld().getBlockState(te.getPos());
		GL11.glPushMatrix();
		if(facing.getAxis() == EnumFacing.Axis.Z)
		{
			GL11.glTranslated(0.0, -0.5, 0.5);
			GL11.glRotated(90.0, 1.0, 0.0, 0.0);
		}
		if(facing.getAxis() == EnumFacing.Axis.X)
		{
			GL11.glTranslated(-0.5, -0.5, 0.0);
			GL11.glRotated(90.0, 0.0, 0.0, 1.0);
		}
		GL11.glRotated(te.rotation[facing.ordinal()], 0.0, 1.0, 0.0);
		GL11.glRotatef(180.0f, 0.0f, 0.0f, 1.0f);
		GL11.glPushMatrix();
		GL11.glScaled(1.0, 0.5, 1.0);
		GL11.glTranslated(0.0, -0.5, 0.0);
		this.shaft.render(null, 0.0f, 0.0f, -0.1f, 0.0f, 0.0f, 0.0625f);
		if(destroy != null)
		{
			GL11.glPushMatrix();
			GL11.glTranslatef(0.0f, -0.01f, 0.0f);
			GL11.glScaled(1.001, 1.001, 1.001);
			this.bindTexture(destroy);
			this.shaft.render(null, 0.0f, 0.0f, -0.1f, 0.0f, 0.0f, 0.0625f);
			GL11.glPopMatrix();
		}
		GL11.glPopMatrix();
		GL11.glPopMatrix();
	}
}
