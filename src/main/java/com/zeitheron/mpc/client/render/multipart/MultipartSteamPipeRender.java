package com.zeitheron.mpc.client.render.multipart;

import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.api.multipart.IMultipartRender;
import com.zeitheron.mpc.client.model.ModelSteamPipe;
import com.zeitheron.mpc.items.multipart.MultipartSteamPipe;

import net.minecraft.util.ResourceLocation;

public class MultipartSteamPipeRender implements IMultipartRender<MultipartSteamPipe>
{
	public final ModelSteamPipe pipe = new ModelSteamPipe();
	
	@Override
	public void renderMultipartAt(MultipartSteamPipe te, double x, double y, double z, float partialTicks, ResourceLocation destroyStage)
	{
		GL11.glTranslated((x + 0.5), (y + 1.0 + 0.063125), (z + 0.5));
		GL11.glRotatef(180F, 0F, 0F, 1F);
		GL11.glPushMatrix();
		GL11.glTranslated(0, -.4375, 0.0);
		pipe.bindTexture(null);
		pipe.render(null, 0F, 0F, -.1F, 0F, 0F, .0625F, te);
		if(destroyStage != null)
		{
			bindTexture(destroyStage);
			pipe.render(null, 0F, 0F, -.1F, 0F, 0F, .0625f, te);
		}
		GL11.glPopMatrix();
	}
}