package com.zeitheron.mpc.client.render.tile;

import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.client.render.tesr.TESR;
import com.zeitheron.hammercore.utils.math.vec.Cuboid6;
import com.zeitheron.mpc.blocks.kinetic.tile.TileKineticGenerator;
import com.zeitheron.mpc.client.model.ModelCoil;
import com.zeitheron.mpc.client.model.ModelShaft;

import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;

public class TESRKineticGenerator extends TESR<TileKineticGenerator>
{
	public final ModelShaft shaft = new ModelShaft();
	public final ModelCoil coil = new ModelCoil();
	
	@Override
	public void renderTileEntityAt(TileKineticGenerator te, double x, double y, double z, float partialTicks, ResourceLocation destroyStage, float alpha)
	{
		if(te.facing != null)
		{
			for(int i = 0; i < te.coils.length; ++i)
			{
				if(!te.coils[i] || te.cuboids[i] == null)
					continue;
				Cuboid6 c = te.cuboids[i];
				double mod = 0.875;
				GL11.glPushMatrix();
				if(te.facing.getAxis() == EnumFacing.Axis.Z)
				{
					GL11.glTranslated(x + 0.5, y + 1.0 + 0.490625, z + c.min.z + 0.0625);
				} else
				{
					GL11.glTranslated(x + c.min.x + 0.0625, y + 1.0 + 0.490625, z + 0.5);
					GL11.glRotated(90.0, 0.0, 1.0, 0.0);
				}
				GL11.glRotatef(180.0f, 0.0f, 0.0f, 1.0f);
				GL11.glPushMatrix();
				GL11.glTranslated(0.0, -0.4375, 0.0);
				if(te.facing.getAxis() == EnumFacing.Axis.Z)
				{
					GL11.glScaled(1.0, 1.0, (c.max.z - c.min.z) * 8.0);
				} else
				{
					GL11.glScaled((c.max.x - c.min.x) * 8.0, 1.0, 1.0);
				}
				
				coil.bindTexture(null);
				coil.render(null, 0.0f, 0.0f, -0.1f, 0.0f, 0.0f, 0.0625f);
				
				if(destroyStage != null)
				{
					bindTexture(destroyStage);
					coil.render(null, 0.0f, 0.0f, -0.1f, 0.0f, 0.0f, 0.0625f);
				}
				
				GL11.glPopMatrix();
				GL11.glPopMatrix();
			}
		}
		if(te.facing != null)
		{
			EnumFacing.Axis facing = te.facing.getAxis();
			GL11.glPushMatrix();
			GL11.glTranslated(x + 0.5, y + 1.0, z + 0.5);
			if(facing == EnumFacing.Axis.Z)
			{
				GL11.glTranslated(0.0, -0.5, te.facing == EnumFacing.SOUTH ? 0.501 : 0.499);
				GL11.glRotated(90.0, 1.0, 0.0, 0.0);
			}
			if(facing == EnumFacing.Axis.X)
			{
				GL11.glTranslated(te.facing == EnumFacing.WEST ? -0.501 : -0.499, -0.5, 0.0);
				GL11.glRotated(90.0, 0.0, 0.0, 1.0);
			}
			GL11.glRotated(te.rotation, 0.0, 1.0, 0.0);
			GL11.glRotatef(180.0f, 0.0f, 0.0f, 1.0f);
			GL11.glPushMatrix();
			GL11.glTranslated(0.0, -0.5, 0.0);
			shaft.bindTexture(facing);
			this.shaft.render(null, 0.0f, 0.0f, -0.1f, 0.0f, 0.0f, 0.0625f);
			if(destroyStage != null)
			{
				GL11.glPushMatrix();
				GL11.glTranslatef(0.0f, -0.01f, 0.0f);
				GL11.glScaled(1.001, 1.001, 1.001);
				this.bindTexture(destroyStage);
				this.shaft.render(null, 0.0f, 0.0f, -0.1f, 0.0f, 0.0f, 0.0625f);
				GL11.glPopMatrix();
			}
			GL11.glPopMatrix();
			GL11.glPopMatrix();
		}
	}
}
