package com.zeitheron.mpc.client.render.tile;

import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.client.render.tesr.TESR;
import com.zeitheron.mpc.blocks.kinetic.BlockKineticShaft;
import com.zeitheron.mpc.blocks.kinetic.tile.TileKineticShaft;
import com.zeitheron.mpc.client.model.ModelShaft;
import com.zeitheron.mpc.client.model.ModelWaterWheel;
import com.zeitheron.mpc.client.model.ModelWindMill;

import net.minecraft.block.state.IBlockState;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;

public class TESRKineticShaft extends TESR<TileKineticShaft>
{
	public final ModelShaft shaft = new ModelShaft();
	public final ModelWaterWheel waterWheel = new ModelWaterWheel();
	public final ModelWindMill windMill = new ModelWindMill();
	
	@Override
	public void renderTileEntityAt(TileKineticShaft te, double x, double y, double z, float partialTicks, ResourceLocation destroyStage, float alpha)
	{
		IBlockState state = te.getLocation().getState();
		EnumFacing.Axis facing = state.getValue(BlockKineticShaft.DIR);
		
		GL11.glPushMatrix();
		GL11.glTranslated(x + .5, y + 1, z + .5);
		if(facing == EnumFacing.Axis.Z)
		{
			GL11.glTranslated(0, -.5, .5);
			GL11.glRotated(90, 1, 0, 0);
		}
		if(facing == EnumFacing.Axis.X)
		{
			GL11.glTranslated(-.5, -.5, 0);
			GL11.glRotated(90, 0, 0, 1);
		}
		GL11.glRotated(te.rotation, 0, 1, 0);
		GL11.glRotatef(180F, 0F, 0F, 1F);
		GL11.glPushMatrix();
		GL11.glTranslated(0, -.5, 0);
		
		shaft.bindTexture(facing);
		shaft.render(null, 0F, 0F, -.1F, 0F, 0F, .0625F);
		if(te.module.isWaterWheel())
		{
			waterWheel.bindTexture(null);
			waterWheel.render(null, 0F, 0F, -.1F, 0F, 0F, .0625F);
		}
		if(te.module.isWindMill())
		{
			windMill.bindTexture(null);
			windMill.render(null, 0F, 0F, -.1F, 0F, 0F, .0625F);
		}
		
		if(destroyStage != null)
		{
			GL11.glPushMatrix();
			GL11.glTranslatef(0F, -.01F, 0F);
			GL11.glScaled(1.001, 1.001, 1.001);
			
			bindTexture(destroyStage);
			shaft.render(null, 0F, 0F, -.1F, 0F, 0F, .0625F);
			if(te.module.isWaterWheel())
				waterWheel.render(null, 0F, 0F, -.1F, 0F, 0F, .0625F);
			if(te.module.isWindMill())
				windMill.render(null, 0F, 0F, -.1F, 0F, 0F, .0625F);
			
			GL11.glPopMatrix();
		}
		GL11.glPopMatrix();
		GL11.glPopMatrix();
	}
}