package com.zeitheron.mpc.client.render.multipart;

import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.api.multipart.IMultipartRender;
import com.zeitheron.hammercore.client.utils.UtilsFX;
import com.zeitheron.mpc.api.energy.impl.wire.MultipartAmpereWireBase;
import com.zeitheron.mpc.client.model.ModelWire;

import net.minecraft.util.ResourceLocation;

public class MultipartAbstractWireRender<T extends MultipartAmpereWireBase> implements IMultipartRender<T>
{
	public final ModelWire wire = new ModelWire();
	
	@Override
	public void renderMultipartAt(T signature, double x, double y, double z, float partialTicks, ResourceLocation destroyStage)
	{
		GL11.glTranslated(x + .5, y + 1.063125, z + .5);
		GL11.glRotatef(180, 0, 0, 1);
		GL11.glPushMatrix();
		GL11.glTranslated(0, -.4375, 0);
		UtilsFX.bindTexture(signature.getModel());
//		wire.bindTexture(signature);
		wire.render(null, 0F, 0F, -.1F, 0F, 0F, .0625F, signature, signature.facing);
		if(destroyStage != null)
		{
			bindTexture(destroyStage);
			wire.render(null, 0F, 0F, -.1F, 0F, 0F, .0625F, signature, signature.facing);
		}
		GL11.glPopMatrix();
	}
}