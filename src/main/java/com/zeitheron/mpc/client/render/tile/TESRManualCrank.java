package com.zeitheron.mpc.client.render.tile;

import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.client.render.tesr.TESR;
import com.zeitheron.mpc.blocks.kinetic.tile.TileManualCrank;
import com.zeitheron.mpc.client.model.ModelManualCrank;

import net.minecraft.util.ResourceLocation;

public class TESRManualCrank extends TESR<TileManualCrank>
{
	public final ModelManualCrank crank = new ModelManualCrank();
	
	@Override
	public void renderTileEntityAt(TileManualCrank te, double x, double y, double z, float partialTicks, ResourceLocation destroyStage, float alpha)
	{
		GL11.glPushMatrix();
		GL11.glTranslated(x + .5, y + 1, z + .5);
		GL11.glRotated(te.rotation, 0, 1, 0);
		GL11.glRotatef(180F, 0F, 0F, 1F);
		GL11.glPushMatrix();
		GL11.glTranslated(0, -.5, 0);
		crank.bindTexture(te);
		crank.render(null, 0F, 0F, -.1F, 0F, 0F, .0625F);
		if(destroyStage != null)
		{
			GL11.glPushMatrix();
			GL11.glTranslatef(0F, -1F, 0F);
			GL11.glScaled(1.001, 1.001, 1.001);
			bindTexture(destroyStage);
			crank.render(null, 0F, 0F, -.1F, 0F, 0F, .0625F);
			GL11.glPopMatrix();
		}
		GL11.glPopMatrix();
		GL11.glPopMatrix();
	}
}