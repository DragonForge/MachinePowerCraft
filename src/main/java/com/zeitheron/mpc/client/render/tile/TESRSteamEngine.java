package com.zeitheron.mpc.client.render.tile;

import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.client.render.tesr.TESR;
import com.zeitheron.hammercore.utils.EnumRotation;
import com.zeitheron.mpc.blocks.steam.tile.TileSteamEngine;
import com.zeitheron.mpc.client.model.ModelShaft;

import net.minecraft.block.state.IBlockState;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;

public class TESRSteamEngine extends TESR<TileSteamEngine>
{
	public final ModelShaft shaft = new ModelShaft();
	
	@Override
	public void renderTileEntityAt(TileSteamEngine te, double x, double y, double z, float partialTicks, ResourceLocation destroyStage, float alpha)
	{
		IBlockState state = te.getWorld().getBlockState(te.getPos());
		EnumFacing facing = state.getValue(EnumRotation.EFACING);
		double pix = 0.063125;
		GL11.glPushMatrix();
		GL11.glTranslated((x + 0.5), (y + 1.0 + pix), (z + 0.5));
		if(facing == EnumFacing.NORTH || facing == EnumFacing.SOUTH)
		{
			GL11.glTranslated(0.0, (-0.5 - pix), (0.5 + pix));
			if(facing == EnumFacing.SOUTH)
				GL11.glTranslated(0.0, 0.0, -0.0016);
			GL11.glRotated(90.0, 1.0, 0.0, 0.0);
		}
		if(facing == EnumFacing.EAST || facing == EnumFacing.WEST)
		{
			GL11.glTranslated((-0.5 - pix), (-0.5 - pix), 0.0);
			if(facing == EnumFacing.WEST)
				GL11.glTranslated(0.0016, 0.0, 0.0);
			GL11.glRotated(90.0, 0.0, 0.0, 1.0);
		}
		if(facing == EnumFacing.UP)
			GL11.glTranslated(0.0, -0.00125, 0.0);
		GL11.glRotated(te.rotation, 0.0, 1.0, 0.0);
		GL11.glRotatef(180.0f, 0.0f, 0.0f, 1.0f);
		GL11.glPushMatrix();
		GL11.glTranslated(0.0, -0.4375, 0.0);
		shaft.bindTexture(facing.getAxis());
		this.shaft.render(null, 0.0f, 0.0f, -0.1f, 0.0f, 0.0f, 0.0625f);
		if(destroyStage != null)
		{
			GL11.glPushMatrix();
			GL11.glTranslatef(0.0f, -0.01f, 0.0f);
			GL11.glScaled(1.001, 1.001, 1.001);
			this.bindTexture(destroyStage);
			this.shaft.render(null, 0.0f, 0.0f, -0.1f, 0.0f, 0.0f, 0.0625f);
			GL11.glPopMatrix();
		}
		GL11.glPopMatrix();
		GL11.glPopMatrix();
	}
}