package com.zeitheron.mpc.client.render.item;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.HashMap;
import java.util.Map;

import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.client.render.item.IItemRender;
import com.zeitheron.hammercore.client.utils.GLImageManager;
import com.zeitheron.mpc.MachinePowerCraft;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ItemGemRender implements IItemRender
{
	private static final Map<String, Integer> openGlIds = new HashMap<String, Integer>();
	private static final Map<Integer, String> openGlIdsRev = new HashMap<Integer, String>();
	private static final Map<Integer, Long> lastSeen = new HashMap<Integer, Long>();
	private static final Map<String, String> nameMap = new HashMap<String, String>();
	private final ItemStack barrier;
	
	public ItemGemRender()
	{
		MinecraftForge.EVENT_BUS.register(this);
		this.barrier = new ItemStack(Blocks.BARRIER);
	}
	
	@Override
	public void renderItem(ItemStack itemStack)
	{
		String seed;
		NBTTagCompound nbt;
		Integer ogl = null;
		if(itemStack.getTagCompound() != null && itemStack.getTagCompound().hasKey("Seed", 7) && itemStack.getTagCompound().hasKey("Icon", 11) && (ogl = openGlIds.get(seed = new String((nbt = itemStack.getTagCompound()).getByteArray("Seed")))) == null)
		{
			ogl = GL11.glGenTextures();
			int[] pixels = itemStack.getTagCompound().getIntArray("Icon");
			int quality = (int) Math.sqrt(pixels.length);
			BufferedImage img = new BufferedImage(quality, quality, 2);
			int[] tpixels = ((DataBufferInt) img.getRaster().getDataBuffer()).getData();
			System.arraycopy(pixels, 0, tpixels, 0, tpixels.length);
			GLImageManager.loadTexture(img, ogl, false);
			openGlIds.put(seed, ogl);
			openGlIdsRev.put(ogl, seed);
			nameMap.put(seed, itemStack.getTagCompound().getString("Name"));
			MachinePowerCraft.LOG.info("Uploaded " + itemStack.getTagCompound().getString("Name") + " Gem to GPU.", new Object[0]);
		}
		
		if(ogl != null)
		{
			lastSeen.put(ogl, System.currentTimeMillis());
			// RenderHelper.disableStandardItemLighting();
			GlStateManager.bindTexture(ogl);
			GL11.glPushMatrix();
			GL11.glTranslated(1.0, 0.0, 0.5);
			GL11.glPushMatrix();
			double s = 0.006097560975609756;
			GL11.glScaled(s, s, s);
			GL11.glRotated(180.0, 0.0, 1.0, 0.0);
			// RenderUtil.drawTexturedModalRect((double) 0.0, (double) 0.0,
			// (double) 0.0, (double) 0.0, (double) 256.0, (double) 256.0);
			GL11.glPopMatrix();
			GL11.glPopMatrix();
			// RenderHelper.enableStandardItemLighting();
		} else
		{
			// GL11.glPushMatrix();
			// GL11.glTranslated((double) 1.0, (double) 0.1, (double) 0.0);
			// Minecraft.getMinecraft().getRenderItem().renderItem(this.barrier,
			// ItemCameraTransforms.TransformType.NONE);
			// GL11.glPopMatrix();
		}
	}
	
	@SubscribeEvent
	public void textureStitch(TextureStitchEvent evt)
	{
		boolean shouldLog;
		boolean bl = shouldLog = !openGlIds.isEmpty();
		if(shouldLog)
		{
			MachinePowerCraft.LOG.info("Cleaning seen gem crystals... (" + openGlIds.size() + " values)", new Object[0]);
		}
		for(Integer val : openGlIds.values())
		{
			if(val == null)
				continue;
			GlStateManager.deleteTexture(val);
		}
		openGlIds.clear();
		if(shouldLog)
		{
			MachinePowerCraft.LOG.info("Cleaned " + openGlIds.size() + " gems from GPU", new Object[0]);
		}
	}
	
	@SubscribeEvent
	public void tick(TickEvent.ClientTickEvent evt)
	{
		int timeout = 5 * 60 * 1000;
		Integer remid = null;
		for(Integer glid : lastSeen.keySet())
		{
			if(lastSeen.get(glid) == null || System.currentTimeMillis() - lastSeen.get(glid) < timeout)
				continue;
			remid = glid;
		}
		if(remid != null)
		{
			this.cleanTextureId(openGlIdsRev.get(remid), "Texture Wasn't Seen in Last 5 Minutes");
		}
	}
	
	private void cleanTextureId(String id, String reason)
	{
		String gemName = nameMap.remove(id);
		MachinePowerCraft.LOG.info("Cleaning " + gemName + " Gem Because " + reason + "...", new Object[0]);
		int glid = openGlIds.remove(id);
		openGlIdsRev.remove(glid);
		lastSeen.remove(glid);
		GlStateManager.deleteTexture(glid);
		MachinePowerCraft.LOG.info(gemName + " Gem Was Cleaned.", new Object[0]);
	}
}
