package com.zeitheron.mpc.client;

import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.client.utils.UV;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;

public class GuiBoxRenderer
{
	public static final ResourceLocation WIDGET_BASE = new ResourceLocation("mpc", "textures/gui/mechanical_book.png");
	private static final UV CORNER_1 = new UV(WIDGET_BASE, 0.0, 181.0, 4.0, 4.0);
	private static final UV CORNER_2 = new UV(WIDGET_BASE, 19.0, 181.0, 4.0, 4.0);
	private static final UV CORNER_3 = new UV(WIDGET_BASE, 19.0, 200.0, 4.0, 4.0);
	private static final UV CORNER_4 = new UV(WIDGET_BASE, 0.0, 200.0, 4.0, 4.0);
	
	public static void drawBox(Minecraft mc, int posX, int posY, int width, int height)
	{
		CORNER_1.render(posX - GuiBoxRenderer.CORNER_1.width, posY - GuiBoxRenderer.CORNER_1.height);
		CORNER_2.render(posX + width, posY - GuiBoxRenderer.CORNER_2.height);
		CORNER_3.render(posX + width, posY + height);
		CORNER_4.render(posX - GuiBoxRenderer.CORNER_4.width, posY + height);
		GL11.glPushMatrix();
		GL11.glTranslated(posX - GuiBoxRenderer.CORNER_1.width, posY, 0.0);
		GL11.glScaled(1.0, height, 1.0);
		RenderUtil.drawTexturedModalRect(0.0, 0.0, 0.0, 185.0, 4.0, 1.0);
		RenderUtil.drawTexturedModalRect(width + GuiBoxRenderer.CORNER_1.width, 0.0, 19.0, 185.0, 4.0, 1.0);
		GL11.glPopMatrix();
		GL11.glPushMatrix();
		GL11.glTranslated(posX, posY - GuiBoxRenderer.CORNER_1.height, 0.0);
		GL11.glScaled(width, 1.0, 1.0);
		RenderUtil.drawTexturedModalRect(0.0, 0.0, 4.0, 181.0, 1.0, 4.0);
		RenderUtil.drawTexturedModalRect(0.0, height + GuiBoxRenderer.CORNER_1.height, 4.0, 200.0, 1.0, 4.0);
		GL11.glPopMatrix();
		GL11.glPushMatrix();
		GL11.glTranslated(posX, posY, 0.0);
		GL11.glScaled(width, height, 1.0);
		RenderUtil.drawTexturedModalRect(0.0, 0.0, 4.0, 185.0, 1.0, 1.0);
		GL11.glPopMatrix();
	}
}
