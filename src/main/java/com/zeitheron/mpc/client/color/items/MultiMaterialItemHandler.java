package com.zeitheron.mpc.client.color.items;

import com.zeitheron.hammercore.utils.color.ColorHelper;
import com.zeitheron.mpc.items.ItemMultiMaterial.EnumMultiMaterialType;
import com.zeitheron.mpc.utils.ColorPump;

import net.minecraft.client.renderer.color.IItemColor;
import net.minecraft.item.ItemStack;

public class MultiMaterialItemHandler implements IItemColor
{
	private final ColorPump nanoCircuitColors = new ColorPump(250L).setBrightnessRange(64, 255);
	
	@Override
	public int getColorFromItemstack(ItemStack stack, int tintIndex)
	{
		if(EnumMultiMaterialType.URANIUM_INGOT.isThisMaterial(stack))
		{
			double del = 2000.0;
			double g = ((stack.hashCode() + System.currentTimeMillis()) % (long) del) / del;
			if(g > 0.5)
				g = 1.0 - g;
			int green = (int) Math.round((g *= 2.0) * 55.0);
			return 6553600 | 200 + green << 8 | 100;
		} else if(EnumMultiMaterialType.CESIUM_DUST.isThisMaterial(stack))
		{
			double del = 8000.0;
			double g = (stack.hashCode() + System.currentTimeMillis()) % (long) del / del;
			if(g > 0.5)
				g = 1.0 - g;
			float rgb = (float) (g *= 2.0);
			rgb = 0.8f + rgb * 0.2f;
			return ColorHelper.packRGB(rgb, rgb, rgb);
		} else if(EnumMultiMaterialType.CIRCUIT_NANO.isThisMaterial(stack))
			return tintIndex == 1 ? nanoCircuitColors.get() : 0xFFFFFF;
		return 0xFFFFFFFF;
	}
}
