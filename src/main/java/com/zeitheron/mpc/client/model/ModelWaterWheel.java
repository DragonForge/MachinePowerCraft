package com.zeitheron.mpc.client.model;

import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.client.model.ModelSimple;

import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

/**
 * WaterWheel - by _MasterEnderman_
 */
public class ModelWaterWheel extends ModelSimple<Object>
{
	public ModelRenderer shape2;
	public ModelRenderer shape3;
	public ModelRenderer shape4;
	public ModelRenderer shape5;
	public ModelRenderer shape6;
	public ModelRenderer shape6_1;
	public ModelRenderer shape9;
	public ModelRenderer shape9_1;
	public ModelRenderer shape12;
	public ModelRenderer shape12_1;
	public ModelRenderer shape14;
	public ModelRenderer shape14_1;
	public ModelRenderer shape14_2;
	public ModelRenderer shape14_3;
	public ModelRenderer shape14_4;
	public ModelRenderer shape14_5;
	
	public ModelWaterWheel()
	{
		super(128, 64, new ResourceLocation("mpc", "textures/models/water_wheel.png"));
		
		shape6 = new ModelRenderer(this, 0, 2);
		shape6.setRotationPoint(-7, -1.5F, -1.5F);
		shape6.addBox(0, 0, 0, 14, 1, 3, 0);
		
		shape9_1 = new ModelRenderer(this, 97, 5);
		shape9_1.setRotationPoint(-7, -0.5F, -2.5F);
		shape9_1.addBox(0, 0, 0, 14, 3, 1, 0);
		
		shape14 = new ModelRenderer(this, 60, 6);
		shape14.setRotationPoint(-7, 0.5F, -7.5F);
		shape14.addBox(0, 0, 0, 14, 1, 5, 0);
		
		shape14_2 = new ModelRenderer(this, 38, 12);
		shape14_2.setRotationPoint(-7, 5.9F, -5.5F);
		shape14_2.addBox(0, 0, 0, 14, 1, 5, 0);
		setRotateAngle(shape14_2, .7853981633974483F, 0, 0);
		
		shape2 = new ModelRenderer(this, 0, 0);
		shape2.setRotationPoint(0, 0, 0);
		shape2.addBox(-7, -1, 1, 14, 1, 1, 0);
		
		shape12_1 = new ModelRenderer(this, 30, 6);
		shape12_1.setRotationPoint(-7, 3.5F, -0.5F);
		shape12_1.addBox(0, 0, 0, 14, 5, 1, 0);
		
		shape14_5 = new ModelRenderer(this, 38, 18);
		shape14_5.setRotationPoint(-7, -1, 1.3F);
		shape14_5.addBox(0, 0, 0, 14, 1, 5, 0);
		setRotateAngle(shape14_5, .7853981633974483F, 0, 0);
		
		shape14_1 = new ModelRenderer(this, 0, 12);
		shape14_1.setRotationPoint(-7, 0.5F, 2.5F);
		shape14_1.addBox(0, 0, 0, 14, 1, 5, 0);
		
		shape5 = new ModelRenderer(this, 90, 0);
		shape5.setRotationPoint(-7, 2, -2);
		shape5.addBox(0, 0, 0, 14, 1, 1, 0);
		
		shape12 = new ModelRenderer(this, 0, 6);
		shape12.setRotationPoint(-7, -6.5F, -0.5F);
		shape12.addBox(0, 0, 0, 14, 5, 1, 0);
		
		shape14_4 = new ModelRenderer(this, 0, 18);
		shape14_4.setRotationPoint(-7, -4.6F, -4.8F);
		shape14_4.addBox(0, 0, 0, 14, 1, 5, 0);
		setRotateAngle(shape14_4, -.7853981633974483F, 0, 0);
		
		shape9 = new ModelRenderer(this, 68, 2);
		shape9.setRotationPoint(-7, -0.5F, 1.5F);
		shape9.addBox(0, 0, 0, 14, 3, 1, 0);
		
		shape3 = new ModelRenderer(this, 30, 0);
		shape3.setRotationPoint(-7, -1, -2);
		shape3.addBox(0, 0, 0, 14, 1, 1, 0);
		
		shape4 = new ModelRenderer(this, 60, 0);
		shape4.setRotationPoint(-7, 2, 1);
		shape4.addBox(0, 0, 0, 14, 1, 1, 0);
		
		shape6_1 = new ModelRenderer(this, 34, 2);
		shape6_1.setRotationPoint(-7, 2.5F, -1.5F);
		shape6_1.addBox(0, 0, 0, 14, 1, 3, 0);
		
		shape14_3 = new ModelRenderer(this, 76, 12);
		shape14_3.setRotationPoint(-7, 2.4F, 2);
		shape14_3.addBox(0, 0, 0, 14, 1, 5, 0);
		setRotateAngle(shape14_3, -.7853981633974483F, 0, 0);
	}
	
	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		GL11.glPushMatrix();
		
		GL11.glTranslated(.075, 1, 0);
		
		GL11.glScaled(1.25, 1.1427, 1.25);
		
		GL11.glRotated(90, 0, 0, 1);
		
		shape6.render(f5);
		shape9_1.render(f5);
		shape14.render(f5);
		shape14_2.render(f5);
		shape2.render(f5);
		shape12_1.render(f5);
		shape14_5.render(f5);
		shape14_1.render(f5);
		shape5.render(f5);
		shape12.render(f5);
		shape14_4.render(f5);
		shape9.render(f5);
		shape3.render(f5);
		shape4.render(f5);
		shape6_1.render(f5);
		shape14_3.render(f5);
		
		GL11.glPopMatrix();
	}
}