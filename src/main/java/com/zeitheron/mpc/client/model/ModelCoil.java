package com.zeitheron.mpc.client.model;

import com.zeitheron.hammercore.client.model.ModelSimple;

import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class ModelCoil extends ModelSimple<Object>
{
	ModelRenderer Shape1;
	
	public ModelCoil()
	{
		super(32, 32, new ResourceLocation("mpc", "textures/models/tin_coil.png"));
		Shape1 = new ModelRenderer(this, 0, 0);
		Shape1.addBox(0F, 0F, 0F, 4, 4, 2);
		Shape1.setRotationPoint(-2F, 20F, -1F);
		Shape1.setTextureSize(32, 32);
		Shape1.mirror = true;
		setRotateAngle(Shape1, 0F, 0F, 0F);
	}
	
	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		super.render(entity, f, f1, f2, f3, f4, f5);
		setRotationAngles(f, f1, f2, f3, f4, f5);
		Shape1.render(f5);
	}
}