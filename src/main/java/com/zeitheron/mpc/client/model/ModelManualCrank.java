package com.zeitheron.mpc.client.model;

import com.zeitheron.hammercore.client.model.ModelSimple;
import com.zeitheron.mpc.blocks.kinetic.tile.TileManualCrank;

import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class ModelManualCrank extends ModelSimple<TileManualCrank>
{
	ModelRenderer Shape1;
	ModelRenderer Shape2;
	
	public ModelManualCrank()
	{
		super(64, 32, new ResourceLocation("mpc", "textures/models/manual_crank.png"));
		
		Shape1 = new ModelRenderer(this, 0, 0);
		Shape1.addBox(0F, 0F, 0F, 2, 8, 2);
		Shape1.setRotationPoint(-1F, 16F, -1F);
		Shape1.setTextureSize(64, 32);
		Shape1.mirror = true;
		setRotateAngle(Shape1, 0F, 0F, 0F);
		
		Shape2 = new ModelRenderer(this, 0, 11);
		Shape2.addBox(0F, 0F, 0F, 2, 2, 6);
		Shape2.setRotationPoint(-1F, 16F, -7F);
		Shape2.setTextureSize(64, 32);
		Shape2.mirror = true;
		setRotateAngle(Shape2, 0F, 0F, 0F);
	}
	
	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		super.render(entity, f, f1, f2, f3, f4, f5);
		setRotationAngles(f, f1, f2, f3, f4, f5);
		Shape1.render(f5);
		Shape2.render(f5);
	}
}