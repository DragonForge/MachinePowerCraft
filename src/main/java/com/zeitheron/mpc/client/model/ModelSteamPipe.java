package com.zeitheron.mpc.client.model;

import com.zeitheron.hammercore.api.multipart.MultipartSignature;
import com.zeitheron.hammercore.client.model.ModelSimple;
import com.zeitheron.hammercore.internal.blocks.multipart.TileMultipart;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.mpc.api.tile.ISteamAcceptor;
import com.zeitheron.mpc.items.multipart.MultipartSteamPipe;
import com.zeitheron.mpc.items.multipart.MultipartSteamPipeConnection;

import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;

public class ModelSteamPipe extends ModelSimple<Object>
{
	ModelRenderer Core;
	ModelRenderer CoreDown;
	ModelRenderer CoreUp;
	ModelRenderer CoreFront;
	ModelRenderer CoreBack;
	ModelRenderer CoreLeft;
	ModelRenderer CoreRight;
	
	public ModelSteamPipe()
	{
		super(64, 64, new ResourceLocation("mpc", "textures/blocks/steam/pipe_model.png"));
		
		Core = new ModelRenderer(this, 0, 0);
		Core.addBox(0F, 0F, 0F, 8, 8, 8);
		Core.setRotationPoint(-4F, 12F, -4F);
		Core.setTextureSize(64, 64);
		Core.mirror = true;
		setRotateAngle(Core, 0F, 0F, 0F);
		
		CoreDown = new ModelRenderer(this, 0, 22);
		CoreDown.addBox(0F, 0F, 0F, 8, 4, 8);
		CoreDown.setRotationPoint(-4F, 20F, -4F);
		CoreDown.setTextureSize(64, 64);
		CoreDown.mirror = true;
		setRotateAngle(CoreDown, 0F, 0F, 0F);
		
		CoreUp = new ModelRenderer(this, 25, 17);
		CoreUp.addBox(0F, 0F, 0F, 8, 4, 8);
		CoreUp.setRotationPoint(-4F, 8F, -4F);
		CoreUp.setTextureSize(64, 64);
		CoreUp.mirror = true;
		setRotateAngle(CoreUp, 0F, 0F, 0F);
		
		CoreFront = new ModelRenderer(this, 0, 36);
		CoreFront.addBox(0F, 0F, 0F, 8, 8, 4);
		CoreFront.setRotationPoint(-4F, 12F, -8F);
		CoreFront.setTextureSize(64, 64);
		CoreFront.mirror = true;
		setRotateAngle(CoreFront, 0F, 0F, 0F);
		
		CoreBack = new ModelRenderer(this, 0, 49);
		CoreBack.addBox(0F, 0F, 0F, 8, 8, 4);
		CoreBack.setRotationPoint(-4F, 12F, 4F);
		CoreBack.setTextureSize(64, 64);
		CoreBack.mirror = true;
		setRotateAngle(CoreBack, 0F, 0F, 0F);
		
		CoreLeft = new ModelRenderer(this, 26, 48);
		CoreLeft.addBox(0F, 0F, 0F, 4, 8, 8);
		CoreLeft.setRotationPoint(4F, 12F, -4F);
		CoreLeft.setTextureSize(64, 64);
		CoreLeft.mirror = true;
		setRotateAngle(CoreLeft, 0F, 0F, 0F);
		
		CoreRight = new ModelRenderer(this, 26, 32);
		CoreRight.addBox(0F, 0F, 0F, 4, 8, 8);
		CoreRight.setRotationPoint(-8F, 12F, -4F);
		CoreRight.setTextureSize(64, 64);
		CoreRight.mirror = true;
		setRotateAngle(CoreRight, 0F, 0F, 0F);
		
//		boxList.add(Core);
//		boxList.add(CoreBack);
//		boxList.add(CoreDown);
//		boxList.add(CoreFront);
//		boxList.add(CoreLeft);
//		boxList.add(CoreRight);
//		boxList.add(CoreUp);
	}
	
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5, ISteamAcceptor pipe)
	{
		MultipartSignature s;
		TileMultipart tmp;
		
		super.render(entity, f, f1, f2, f3, f4, f5);
		setRotationAngles(f, f1, f2, f3, f4, f5);
		
		Core.render(f5);
		if(pipe instanceof MultipartSignature && (tmp = (s = (MultipartSignature) pipe).getOwner()) != null)
		{
			if(hasConnection(tmp, EnumFacing.DOWN))
				CoreDown.render(f5);
			if(hasConnection(tmp, EnumFacing.UP))
				CoreUp.render(f5);
			if(hasConnection(tmp, EnumFacing.NORTH))
				CoreFront.render(f5);
			if(hasConnection(tmp, EnumFacing.SOUTH))
				CoreBack.render(f5);
			if(hasConnection(tmp, EnumFacing.WEST))
				CoreLeft.render(f5);
			if(hasConnection(tmp, EnumFacing.EAST))
				CoreRight.render(f5);
		}
	}
	
	public boolean hasConnection(TileMultipart tmp, EnumFacing side)
	{
		MultipartSteamPipe sign = WorldUtil.cast(tmp.getSignature(MultipartSteamPipe.MAIN_POS), MultipartSteamPipe.class);
		if(sign != null)
			return sign.hasConnection(side) || tmp.getSignature(MultipartSteamPipeConnection.getPosition(side)) instanceof MultipartSteamPipeConnection;
		return tmp.getSignature(MultipartSteamPipeConnection.getPosition(side)) instanceof MultipartSteamPipeConnection;
	}
}