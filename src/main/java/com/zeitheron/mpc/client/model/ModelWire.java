package com.zeitheron.mpc.client.model;

import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.client.model.ModelSimple;
import com.zeitheron.mpc.api.IWire;
import com.zeitheron.mpc.api.energy.impl.wire.MultipartAmpereWireBase;

import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;

public class ModelWire extends ModelSimple<MultipartAmpereWireBase>
{
	ModelRenderer North;
	ModelRenderer South;
	ModelRenderer Center;
	ModelRenderer East;
	ModelRenderer West;
	
	public ModelWire()
	{
		super(64, 32, "null");
		
		North = new ModelRenderer(this, 0, 0);
		North.addBox(0F, 0F, 0F, 4, 3, 6);
		North.setRotationPoint(-2F, 21.5F, -8F);
		North.setTextureSize(64, 32);
		North.mirror = true;
		setRotateAngle(North, 0F, 0F, 0F);
		
		South = new ModelRenderer(this, 0, 10);
		South.addBox(0F, 0F, 0F, 4, 3, 6);
		South.setRotationPoint(-2F, 21.5F, 2F);
		South.setTextureSize(64, 32);
		South.mirror = true;
		setRotateAngle(South, 0F, 0F, 0F);
		
		Center = new ModelRenderer(this, 0, 21);
		Center.addBox(0F, 0F, 0F, 4, 3, 4);
		Center.setRotationPoint(-2F, 21.5F, -2F);
		Center.setTextureSize(64, 32);
		Center.mirror = true;
		setRotateAngle(Center, 0F, 0F, 0F);
		
		East = new ModelRenderer(this, 21, 0);
		East.addBox(0F, 0F, 0F, 6, 3, 4);
		East.setRotationPoint(-8F, 21.5F, -2F);
		East.setTextureSize(64, 32);
		East.mirror = true;
		setRotateAngle(East, 0F, 0F, 0F);
		
		West = new ModelRenderer(this, 21, 8);
		West.addBox(0F, 0F, 0F, 6, 3, 4);
		West.setRotationPoint(2F, 21.5F, -2F);
		West.setTextureSize(64, 32);
		West.mirror = true;
		setRotateAngle(West, 0F, 0F, 0F);
	}
	
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5, IWire connectable, EnumFacing facing)
	{
		setRotationAngles(f, f1, f2, f3, f4, f5, null);
		GL11.glPushMatrix();
		switch(facing)
		{
		case DOWN:
		{
			if(connectable.doesConnectTo(EnumFacing.NORTH))
				North.render(f5);
			if(connectable.doesConnectTo(EnumFacing.SOUTH))
				South.render(f5);
			if(connectable.doesConnectTo(EnumFacing.EAST))
				East.render(f5);
			if(connectable.doesConnectTo(EnumFacing.WEST))
				West.render(f5);
			Center.render(f5);
			break;
		}
		case UP:
		{
			GL11.glRotated(180, 0, 0, 1);
			GL11.glRotated(180, 0, 1, 0);
			GL11.glTranslated(0, -2, 0);
			if(connectable.doesConnectTo(EnumFacing.SOUTH))
				North.render(f5);
			if(connectable.doesConnectTo(EnumFacing.NORTH))
				South.render(f5);
			if(connectable.doesConnectTo(EnumFacing.EAST))
				East.render(f5);
			if(connectable.doesConnectTo(EnumFacing.WEST))
				West.render(f5);
			Center.render(f5);
			break;
		}
		case NORTH:
		{
			GL11.glRotated(90, 1, 0, 0);
			GL11.glTranslated(0, 0, -1);
			GL11.glTranslated(0, -1.875, 0);
			if(connectable.doesConnectTo(EnumFacing.DOWN))
				North.render(f5);
			if(connectable.doesConnectTo(EnumFacing.UP))
				South.render(f5);
			if(connectable.doesConnectTo(EnumFacing.EAST))
				East.render(f5);
			if(connectable.doesConnectTo(EnumFacing.WEST))
				West.render(f5);
			Center.render(f5);
			break;
		}
		case SOUTH:
		{
			GL11.glRotated(270, 1, 0, 0);
			GL11.glTranslated(0, 0, 1);
			GL11.glTranslated(0, -1.875, 0);
			if(connectable.doesConnectTo(EnumFacing.UP))
				North.render(f5);
			if(connectable.doesConnectTo(EnumFacing.DOWN))
				South.render(f5);
			if(connectable.doesConnectTo(EnumFacing.EAST))
				East.render(f5);
			if(connectable.doesConnectTo(EnumFacing.WEST))
				West.render(f5);
			Center.render(f5);
			break;
		}
		case EAST:
		{
			GL11.glRotated(270, 0, 0, 1);
			GL11.glTranslated(-1, 0, 0);
			GL11.glTranslated(0, -1.875, 0);
			if(connectable.doesConnectTo(EnumFacing.NORTH))
				North.render(f5);
			if(connectable.doesConnectTo(EnumFacing.SOUTH))
				South.render(f5);
			if(connectable.doesConnectTo(EnumFacing.DOWN))
				East.render(f5);
			if(connectable.doesConnectTo(EnumFacing.UP))
				West.render(f5);
			Center.render(f5);
			break;
		}
		case WEST:
		{
			GL11.glRotated(90, 0, 0, 1);
			GL11.glTranslated(1, -2, 0);
			GL11.glTranslated(0, .125, 0);
			if(connectable.doesConnectTo(EnumFacing.NORTH))
				North.render(f5);
			if(connectable.doesConnectTo(EnumFacing.SOUTH))
				South.render(f5);
			if(connectable.doesConnectTo(EnumFacing.UP))
				East.render(f5);
			if(connectable.doesConnectTo(EnumFacing.DOWN))
				West.render(f5);
			Center.render(f5);
			break;
		}
		}
		GL11.glPopMatrix();
	}
	
	@Override
	public ResourceLocation getTexture(MultipartAmpereWireBase mp)
	{
		return mp.getModel();
	}
}