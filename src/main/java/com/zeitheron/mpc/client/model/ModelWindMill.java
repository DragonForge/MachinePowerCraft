package com.zeitheron.mpc.client.model;

import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.client.model.ModelSimple;

import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

/**
 * WindMill - by _MasterEnderman_
 */
public class ModelWindMill extends ModelSimple<Object>
{
	public ModelRenderer post;
	public ModelRenderer wing;
	public ModelRenderer post_1;
	public ModelRenderer wing_1;
	public ModelRenderer post_2;
	public ModelRenderer wing_2;
	public ModelRenderer post_3;
	public ModelRenderer wing_3;
	public ModelRenderer center1;
	public ModelRenderer center2;
	public ModelRenderer center3;
	public ModelRenderer center4;
	
	public ModelWindMill()
	{
		super(128, 64, new ResourceLocation("mpc", "textures/models/wind_mill.png"));
		
		center2 = new ModelRenderer(this, 8, 0);
		center2.setRotationPoint(-1.8F, 0.0F, -1.5F);
		center2.addBox(0.0F, 0.0F, 0.0F, 1, 8, 3, 0.0F);
		
		wing_1 = new ModelRenderer(this, 68, 0);
		wing_1.setRotationPoint(5.2F, 2.7F, -4.7F);
		wing_1.addBox(0.0F, 0.0F, 0.0F, 5, 1, 17, 0.0F);
		setRotateAngle(wing_1, 0.0F, -3.141592653589793F, 0.16755160819145562F);
		
		post_3 = new ModelRenderer(this, 0, 22);
		post_3.setRotationPoint(1.8F, 1.6F, 0.5F);
		post_3.addBox(0.0F, 0.0F, 0.0F, 1, 2, 20, 0.0F);
		setRotateAngle(post_3, 0.0F, 1.5707963267948966F, 0.0F);
		
		post = new ModelRenderer(this, 0, 0);
		post.setRotationPoint(-0.5F, 1.6F, 1.8F);
		post.addBox(0.0F, 0.0F, 0.0F, 1, 2, 20, 0.0F);
		
		post_1 = new ModelRenderer(this, 46, 0);
		post_1.setRotationPoint(0.5F, 1.6F, -1.8F);
		post_1.addBox(0.0F, 0.0F, 0.0F, 1, 2, 20, 0.0F);
		setRotateAngle(post_1, -0.00767944870877505F, 3.141592653589793F, -0.0F);
		
		center3 = new ModelRenderer(this, 22, 0);
		center3.setRotationPoint(-1.5F, 0.0F, -1.8F);
		center3.addBox(0.0F, 0.0F, 0.0F, 3, 8, 1, 0.0F);
		
		wing_3 = new ModelRenderer(this, 44, 22);
		wing_3.setRotationPoint(20.7F, 2.7F, 5.2F);
		wing_3.addBox(0.0F, 0.0F, 0.0F, 17, 1, 5, 0.0F);
		setRotateAngle(wing_3, 0.16755160819145562F, -3.141592653589793F, 0.0F);
		
		wing_2 = new ModelRenderer(this, 83, 18);
		wing_2.setRotationPoint(-20.7F, 2.7F, -5.2F);
		wing_2.addBox(0.0F, 0.0F, 0.0F, 17, 1, 5, 0.0F);
		setRotateAngle(wing_2, 0.16755160819145562F, 0.0F, 0.0F);
		
		center1 = new ModelRenderer(this, 0, 0);
		center1.setRotationPoint(0.8F, 0.0F, -1.5F);
		center1.addBox(0.0F, 0.0F, 0.0F, 1, 8, 3, 0.0F);
		
		center4 = new ModelRenderer(this, 30, 0);
		center4.setRotationPoint(-1.5F, 0.0F, 0.8F);
		center4.addBox(0.0F, 0.0F, 0.0F, 3, 8, 1, 0.0F);
		
		wing = new ModelRenderer(this, 22, 0);
		wing.setRotationPoint(-5.2F, 2.7F, 4.7F);
		wing.addBox(0.0F, 0.0F, 0.0F, 5, 1, 17, 0.0F);
		setRotateAngle(wing, 0.0F, 0.0F, -0.16755160819145562F);
		
		post_2 = new ModelRenderer(this, 22, 18);
		post_2.setRotationPoint(-1.8F, 1.6F, -0.5F);
		post_2.addBox(0.0F, 0.0F, 0.0F, 1, 2, 20, 0.0F);
		setRotateAngle(post_2, 0.0F, -1.5707963267948966F, 0.0F);
	}
	
	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		GL11.glPushMatrix();
		
		GL11.glTranslated(0, .49, 0);
		
		GL11.glScaled(1.4, 1, 1.4);
		
		center2.render(f5);
		GlStateManager.pushMatrix();
		GlStateManager.translate(wing_1.offsetX, wing_1.offsetY, wing_1.offsetZ);
		GlStateManager.translate(wing_1.rotationPointX * f5, wing_1.rotationPointY * f5, wing_1.rotationPointZ * f5);
		GlStateManager.scale(1.0D, 0.5D, 1.0D);
		GlStateManager.translate(-wing_1.offsetX, -wing_1.offsetY, -wing_1.offsetZ);
		GlStateManager.translate(-wing_1.rotationPointX * f5, -wing_1.rotationPointY * f5, -wing_1.rotationPointZ * f5);
		wing_1.render(f5);
		GlStateManager.popMatrix();
		post_3.render(f5);
		post.render(f5);
		post_1.render(f5);
		center3.render(f5);
		GlStateManager.pushMatrix();
		GlStateManager.translate(wing_3.offsetX, wing_3.offsetY, wing_3.offsetZ);
		GlStateManager.translate(wing_3.rotationPointX * f5, wing_3.rotationPointY * f5, wing_3.rotationPointZ * f5);
		GlStateManager.scale(1.0D, 0.5D, 1.0D);
		GlStateManager.translate(-wing_3.offsetX, -wing_3.offsetY, -wing_3.offsetZ);
		GlStateManager.translate(-wing_3.rotationPointX * f5, -wing_3.rotationPointY * f5, -wing_3.rotationPointZ * f5);
		wing_3.render(f5);
		GlStateManager.popMatrix();
		GlStateManager.pushMatrix();
		GlStateManager.translate(wing_2.offsetX, wing_2.offsetY, wing_2.offsetZ);
		GlStateManager.translate(wing_2.rotationPointX * f5, wing_2.rotationPointY * f5, wing_2.rotationPointZ * f5);
		GlStateManager.scale(1.0D, 0.5D, 1.0D);
		GlStateManager.translate(-wing_2.offsetX, -wing_2.offsetY, -wing_2.offsetZ);
		GlStateManager.translate(-wing_2.rotationPointX * f5, -wing_2.rotationPointY * f5, -wing_2.rotationPointZ * f5);
		wing_2.render(f5);
		GlStateManager.popMatrix();
		center1.render(f5);
		center4.render(f5);
		GlStateManager.pushMatrix();
		GlStateManager.translate(wing.offsetX, wing.offsetY, wing.offsetZ);
		GlStateManager.translate(wing.rotationPointX * f5, wing.rotationPointY * f5, wing.rotationPointZ * f5);
		GlStateManager.scale(1.0D, 0.5D, 1.0D);
		GlStateManager.translate(-wing.offsetX, -wing.offsetY, -wing.offsetZ);
		GlStateManager.translate(-wing.rotationPointX * f5, -wing.rotationPointY * f5, -wing.rotationPointZ * f5);
		wing.render(f5);
		GlStateManager.popMatrix();
		post_2.render(f5);
		
		GL11.glPopMatrix();
	}
}