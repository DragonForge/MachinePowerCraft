package com.zeitheron.mpc.api.recipes.compressor;

import com.zeitheron.hammercore.utils.InterItemStack;
import com.zeitheron.mpc.api.EnumMachineType;
import com.zeitheron.mpc.init.BlocksMPC;

import net.minecraft.item.ItemStack;

public class CompressorRecipes
{
	EnumMachineType type;
	ItemStack in;
	String inod;
	ItemStack out;
	
	public CompressorRecipes(EnumMachineType type, ItemStack in, ItemStack out)
	{
		this.type = type;
		this.in = in;
		this.out = out;
	}
	
	public CompressorRecipes(EnumMachineType type, String inOD, ItemStack out)
	{
		this.type = type;
		this.inod = inOD;
		this.out = out;
	}
	
	public ItemStack getCompressor()
	{
		return this.type == EnumMachineType.MECHANICAL ? new ItemStack(BlocksMPC.MECHANICAL_COMPRESSOR) : InterItemStack.NULL_STACK;
	}
	
	public ItemStack getInput()
	{
		return this.in;
	}
	
	public ItemStack getOutput()
	{
		return this.out;
	}
}