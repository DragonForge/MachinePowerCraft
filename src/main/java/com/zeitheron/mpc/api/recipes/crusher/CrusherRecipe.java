package com.zeitheron.mpc.api.recipes.crusher;

import com.zeitheron.hammercore.utils.InterItemStack;
import com.zeitheron.mpc.api.EnumMachineType;
import com.zeitheron.mpc.init.BlocksMPC;

import net.minecraft.item.ItemStack;

public class CrusherRecipe
{
	EnumMachineType type;
	ItemStack in;
	ItemStack out;
	
	public CrusherRecipe(EnumMachineType type, ItemStack in, ItemStack out)
	{
		this.type = type;
		this.in = in;
		this.out = out;
	}
	
	public ItemStack getCrusher()
	{
		return type == EnumMachineType.MECHANICAL ? new ItemStack(BlocksMPC.MECHANICAL_CRUSHER) : type == EnumMachineType.ELECTRICAL ? new ItemStack(BlocksMPC.ELECTRICAL_CRUSHER) : InterItemStack.NULL_STACK;
	}
	
	public Object getInput()
	{
		return this.in;
	}
	
	public ItemStack getOutput()
	{
		return this.out;
	}
}