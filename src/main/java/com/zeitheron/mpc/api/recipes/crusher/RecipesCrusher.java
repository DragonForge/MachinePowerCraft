package com.zeitheron.mpc.api.recipes.crusher;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.google.common.collect.Maps;
import com.zeitheron.mpc.api.EnumMachineType;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;

public class RecipesCrusher
{
	public final EnumMachineType type;
	
	private static final Map<EnumMachineType, RecipesCrusher> INSTANCES = new HashMap<>();
	
	/** The list of Crushing results. */
	private final Map<ItemStack, ItemStack> crushingList = Maps.<ItemStack, ItemStack> newHashMap();
	
	/**
	 * Returns an instance of FurnaceRecipes.
	 */
	public static RecipesCrusher get(EnumMachineType type)
	{
		if(!INSTANCES.containsKey(type))
			INSTANCES.put(type, new RecipesCrusher(type));
		return INSTANCES.get(type);
	}
	
	private RecipesCrusher(EnumMachineType type)
	{
		this.type = type;
	}
	
	/**
	 * Adds a Crushing recipe, where the input item is an instance of Block.
	 */
	public void add(Block input, ItemStack stack)
	{
		this.add(Item.getItemFromBlock(input), stack);
	}
	
	/**
	 * Adds a Crushing recipe using an Item as the input item.
	 */
	public void add(Item input, ItemStack stack)
	{
		this.add(new ItemStack(input, 1, OreDictionary.WILDCARD_VALUE), stack);
	}
	
	/**
	 * Adds a Crushing recipe using an ItemStack as the input for the recipe.
	 */
	public void add(ItemStack input, ItemStack stack)
	{
		if(!get(input).isEmpty())
		{
			net.minecraftforge.fml.common.FMLLog.log.info("Ignored Crushing recipe with conflicting input: {} = {}", input, stack);
			return;
		}
		this.crushingList.put(input, stack);
	}
	
	/**
	 * Returns the Crushing result of an item.
	 */
	public ItemStack get(ItemStack stack)
	{
		for(Entry<ItemStack, ItemStack> entry : this.crushingList.entrySet())
			if(this.compare(stack, entry.getKey()))
				return entry.getValue();
			
		if(type.ordinal() > 0)
			return get(EnumMachineType.values()[type.ordinal() - 1]).get(stack);
		
		return ItemStack.EMPTY;
	}
	
	/**
	 * Compares two itemstacks to ensure that they are the same. This checks
	 * both the item and the metadata of the item.
	 */
	private boolean compare(ItemStack stack1, ItemStack stack2)
	{
		return stack2.getItem() == stack1.getItem() && (stack2.getMetadata() == OreDictionary.WILDCARD_VALUE || stack2.getMetadata() == stack1.getMetadata());
	}
	
	public Map<ItemStack, ItemStack> recipes()
	{
		return this.crushingList;
	}
	
	public Set<CrusherRecipe> entries()
	{
		HashSet<CrusherRecipe> ents = new HashSet<CrusherRecipe>();
		for(ItemStack in : crushingList.keySet())
			ents.add(new CrusherRecipe(this.type, in.copy(), get(in)));
		return ents;
	}
}