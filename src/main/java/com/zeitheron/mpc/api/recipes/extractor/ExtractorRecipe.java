package com.zeitheron.mpc.api.recipes.extractor;

import com.zeitheron.hammercore.utils.InterItemStack;
import com.zeitheron.mpc.api.EnumMachineType;
import com.zeitheron.mpc.init.BlocksMPC;

import net.minecraft.item.ItemStack;

public class ExtractorRecipe
{
	EnumMachineType type;
	ItemStack in;
	ItemStack out;
	
	public ExtractorRecipe(EnumMachineType type, ItemStack in, ItemStack out)
	{
		this.type = type;
		this.in = in;
		this.out = out;
	}
	
	public ItemStack getExtractor()
	{
		return this.type == EnumMachineType.ELECTRICAL ? new ItemStack(BlocksMPC.EXTRACTOR) : InterItemStack.NULL_STACK;
	}
	
	public ItemStack getInput()
	{
		return this.in;
	}
	
	public ItemStack getOutput()
	{
		return this.out;
	}
}