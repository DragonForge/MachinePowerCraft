package com.zeitheron.mpc.api;

import net.minecraft.util.EnumFacing;

public interface IWire
{
	public boolean doesConnectTo(EnumFacing var1);
}