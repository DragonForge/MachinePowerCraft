package com.zeitheron.mpc.api.tile;

import net.minecraft.util.EnumFacing;

public interface IKineticSRC
{
	public boolean isSourceEmittingTo(EnumFacing var1);
}