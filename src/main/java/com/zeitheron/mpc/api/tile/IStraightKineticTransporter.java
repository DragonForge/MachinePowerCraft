package com.zeitheron.mpc.api.tile;

import net.minecraft.block.state.IBlockState;
import net.minecraft.util.EnumFacing;

public interface IStraightKineticTransporter
{
	public double getWeightInKilograms();
	
	public boolean canTransport(IBlockState var1, EnumFacing var2);
	
	default public void rotate(double deg)
	{
	}
	
	default public void setRotation(double deg)
	{
	}
}