package com.zeitheron.mpc.api.tile;

import com.zeitheron.hammercore.api.handlers.ITileHandler;

import net.minecraft.util.EnumFacing;

public interface ISteamAcceptor extends ITileHandler
{
	public double acceptSteam(double var1);
	
	public boolean canConnectTo(EnumFacing var1);
}