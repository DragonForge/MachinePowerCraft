package com.zeitheron.mpc.api.tile;

import net.minecraft.util.EnumFacing;

public interface IKineticAcceptor
{
	public double getWeightInKilograms();
	
	public boolean canAcceptKineticFrom(EnumFacing from);
	
	public void acceptKinetic(double deg, EnumFacing from);
	
	default void setVisualRotation(double deg, EnumFacing from)
	{
		
	}
}