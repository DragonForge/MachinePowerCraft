package com.zeitheron.mpc.api.tile;

import net.minecraft.util.ITickable;

public interface ISystemTimeTickable extends ITickable
{
	public TimeContainer getContainer();
	
	@Override
	default public void update()
	{
		if(this.getContainer().shouldUpdate())
		{
			this.tick();
			this.getContainer().lastTick = System.currentTimeMillis();
		} else
		{
			this.illegalTick();
		}
	}
	
	public void tick();
	
	default public void illegalTick()
	{
	}
	
	public static class TimeContainer
	{
		private long lastTick;
		
		private boolean shouldUpdate()
		{
			return this.lastTick > System.currentTimeMillis() || System.currentTimeMillis() - this.lastTick >= 50;
		}
	}
}