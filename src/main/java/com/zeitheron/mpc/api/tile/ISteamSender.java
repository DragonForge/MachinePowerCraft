package com.zeitheron.mpc.api.tile;

import com.zeitheron.hammercore.api.handlers.ITileHandler;

import net.minecraft.util.EnumFacing;

public interface ISteamSender extends ITileHandler
{
	public boolean canConnectTo(EnumFacing var1);
}