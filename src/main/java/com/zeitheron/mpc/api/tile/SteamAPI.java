package com.zeitheron.mpc.api.tile;

import java.util.Collection;

import com.zeitheron.hammercore.api.handlers.IHandlerProvider;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.mpc.items.multipart.MultipartSteamPipe;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;

public class SteamAPI
{
	public static ISteamSender getSteamSender(TileEntity tile, EnumFacing facing)
	{
		IHandlerProvider provider;
		if(tile instanceof IHandlerProvider && (provider = (IHandlerProvider) tile).hasHandler(facing, ISteamSender.class))
			return provider.getHandler(facing, ISteamSender.class);
		return WorldUtil.cast(tile, ISteamSender.class);
	}
	
	public static ISteamAcceptor getSteamAcceptor(TileEntity tile, EnumFacing facing)
	{
		IHandlerProvider provider;
		if(tile instanceof IHandlerProvider && (provider = (IHandlerProvider) tile).hasHandler(facing, ISteamAcceptor.class))
			return provider.getHandler(facing, ISteamAcceptor.class);
		return WorldUtil.cast(tile, ISteamAcceptor.class);
	}
	
	public static void equalize(Collection<MultipartSteamPipe> pipes)
	{
		double steam = 0;
		for(MultipartSteamPipe msp : pipes)
			steam += msp.steamStored;
		if(steam > 0)
		{
			double spp = steam / pipes.size();
			for(MultipartSteamPipe msp : pipes)
				msp.steamStored = spp;
		}
	}
}