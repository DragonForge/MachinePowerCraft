package com.zeitheron.mpc.api.energy.impl;

import com.zeitheron.hammercore.tile.TileSyncable;
import com.zeitheron.mpc.api.energy.EnergyPacket;
import com.zeitheron.mpc.api.energy.IAmpereReceiver;

import net.minecraft.util.math.BlockPos;

public abstract class TileAmpereReceiverBase extends TileSyncable implements IAmpereReceiver
{
	@Override
	public BlockPos pos()
	{
		return this.getPos();
	}
	
	@Override
	public double getResistance(EnergyPacket packet)
	{
		return 1.0E-119;
	}
}