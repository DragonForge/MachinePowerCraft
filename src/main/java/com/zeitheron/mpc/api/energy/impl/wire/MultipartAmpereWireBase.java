package com.zeitheron.mpc.api.energy.impl.wire;

import java.awt.Color;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.zeitheron.hammercore.HammerCore;
import com.zeitheron.hammercore.api.handlers.IHandlerProvider;
import com.zeitheron.hammercore.api.handlers.ITileHandler;
import com.zeitheron.hammercore.api.multipart.MultipartSignature;
import com.zeitheron.hammercore.internal.blocks.multipart.TileMultipart;
import com.zeitheron.hammercore.utils.InterItemStack;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.mpc.api.IWire;
import com.zeitheron.mpc.api.energy.EnergyPacket;
import com.zeitheron.mpc.api.energy.IAmpereReceiver;
import com.zeitheron.mpc.api.energy.IAmpereSender;
import com.zeitheron.mpc.init.DamageSourceMPC;

import net.minecraft.block.SoundType;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.IBlockAccess;

public abstract class MultipartAmpereWireBase extends MultipartSignature implements IAmpereReceiver, ITickable, IWire, IHandlerProvider
{
	public static final AxisAlignedBB AABB_DOWN = new AxisAlignedBB(0.0, 0.0, 0.0, 1.0, 0.15625, 1.0);
	public static final AxisAlignedBB AABB_UP = new AxisAlignedBB(0.0, 0.84375, 0.0, 1.0, 1.0, 1.0);
	public static final AxisAlignedBB AABB_NORTH = new AxisAlignedBB(0.0, 0.0, 0.0, 1.0, 1.0, 0.15625);
	public static final AxisAlignedBB AABB_SOUTH = new AxisAlignedBB(0.0, 0.0, 0.84375, 1.0, 1.0, 1.0);
	public static final AxisAlignedBB AABB_WEST = new AxisAlignedBB(0.0, 0.0, 0.0, 0.15625, 1.0, 1.0);
	public static final AxisAlignedBB AABB_EAST = new AxisAlignedBB(0.84375, 0.0, 0.0, 1.0, 1.0, 1.0);
	public static final Vec3d POS_DOWN = AABB_DOWN.getCenter();
	public static final Vec3d POS_UP = AABB_UP.getCenter();
	public static final Vec3d POS_NORTH = AABB_NORTH.getCenter();
	public static final Vec3d POS_SOUTH = AABB_SOUTH.getCenter();
	public static final Vec3d POS_WEST = AABB_WEST.getCenter();
	public static final Vec3d POS_EAST = AABB_EAST.getCenter();
	protected Map<EnumFacing, Boolean> CONNECTIONS = new HashMap<EnumFacing, Boolean>();
	protected static final Vec3d[] POS_V3D = new Vec3d[] { POS_DOWN, POS_UP, POS_NORTH, POS_SOUTH, POS_WEST, POS_EAST };
	protected static final AxisAlignedBB[] VERTEX_AABB = new AxisAlignedBB[] { AABB_DOWN, AABB_UP, AABB_NORTH, AABB_SOUTH, AABB_WEST, AABB_EAST };
	public EnumFacing facing;
	public WireNetwork network = new WireNetwork();
	
	public static Vec3d getPosition(EnumFacing face)
	{
		return POS_V3D[face.getIndex()];
	}
	
	public static AxisAlignedBB getBox(EnumFacing face)
	{
		return VERTEX_AABB[face.getIndex()];
	}
	
	@Override
	public void update()
	{
		if(network == null || !network.isAlive)
			network = new WireNetwork();
		if(!network.nodes.containsKey(pos.toLong()))
			network.defineWire(this);
		for(EnumFacing face : EnumFacing.VALUES)
		{
			BlockPos o = pos.offset(face);
			if(!world.isBlockLoaded(o))
				continue;
			boolean skipCheck = false;
			MultipartAmpereWireBase t = null;
			TileEntity te = world.getTileEntity(o);
			TileMultipart tmp = WorldUtil.cast(te, TileMultipart.class);
			
			if(skipCheck = owner.getSignature(MultipartAmpereWireBase.getPosition(face)) instanceof MultipartAmpereWireBase && face.getAxis() != facing.getAxis())
				t = (MultipartAmpereWireBase) owner.getSignature(MultipartAmpereWireBase.getPosition(face));
			else if(t == null && !world.isSideSolid(o, face.getOpposite()) && !world.isSideSolid(o, facing))
			{
				TileMultipart tm = WorldUtil.cast(world.getTileEntity(o.offset(facing)), TileMultipart.class);
				
				if(tmp != null && tmp.getSignature(MultipartAmpereWireBase.getPosition(facing)) instanceof MultipartAmpereWireBase)
					t = (MultipartAmpereWireBase) tmp.getSignature(MultipartAmpereWireBase.getPosition(facing));
				else if(tm != null && tm.getSignature(MultipartAmpereWireBase.getPosition(face.getOpposite())) instanceof MultipartAmpereWireBase)
				{
					t = (MultipartAmpereWireBase) tm.getSignature(MultipartAmpereWireBase.getPosition(face.getOpposite()));
					skipCheck = true;
				}
			}
			
			ntw: if(t != null)
			{
				if(!skipCheck && !t.canConnectTo(face.getOpposite()))
					break ntw;
				if(t.network == network)
					break ntw;
				
				if(t.network.nodes.size() > network.nodes.size())
				{
					t.network.merge(network);
					break ntw;
				}
				network.merge(t.network);
			}
			
			if(te instanceof IAmpereReceiver && !network.nodes.containsKey(o.toLong()) && ((IAmpereReceiver) te).canConnectTo(face.getOpposite()))
			{
				network.define(ReceiverNode.create((IAmpereReceiver) te, face.getOpposite()));
				continue;
			}
			if(te instanceof IAmpereSender && !network.nodes.containsKey(o.toLong()) && ((IAmpereSender) te).canConnectTo(face.getOpposite()))
			{
				network.define(SenderNode.create((IAmpereSender) te, face.getOpposite()));
				continue;
			}
			if(!network.nodes.containsKey(o.toLong()))
				continue;
			network.nodes.remove(o.toLong());
			ReceiverNode rrem = null;
			for(ReceiverNode node2 : network.receivers)
			{
				if(node2.pos().toLong() != o.toLong())
					continue;
				rrem = node2;
			}
			network.receivers.remove(rrem);
			SenderNode srem = null;
			for(SenderNode node3 : network.senders)
			{
				if(node3.pos().toLong() != o.toLong())
					continue;
				srem = node3;
			}
			network.senders.remove(srem);
			network.paths.remove(srem);
		}
		if(owner != null && owner.atTickRate(10))
		{
			Arrays.stream(EnumFacing.values()).forEach(ef ->
			{
				CONNECTIONS.put(ef, checkConnect(ef));
			});
			if(!world.isBlockLoaded(pos.offset(facing)) || !world.getBlockState(pos.offset(facing)).isSideSolid(world, pos.offset(facing), facing.getOpposite()))
			{
				owner.removeMultipart(this, true);
				network.isAlive = false;
			}
		}
	}
	
	@Override
	public void acceptPacket(EnergyPacket packet, EnumFacing from)
	{
		if(packet == null)
			return;
		
		Object node;
		BlockPos f = pos.offset(from);
		TileEntity te = world.getTileEntity(f);
		if(te instanceof IAmpereSender && (node = network.nodes.get(f.toLong())) instanceof SenderNode && ((SenderNode) node).sender() == te)
		{
			Set<WirePath> paths = network.paths.get(node);
			if(paths == null)
			{
				network.defineWire(this);
				paths = network.paths.get(node);
				if(paths == null)
					return;
			}
			
			EnergyPacket[] packets = packet.divide(paths.size());
			int i = 0;
			paths.remove(null);
			if(paths != null)
			{
				for(WirePath path : paths)
				{
					EnergyPacket pkt = packets[i];
					path.applyPossibleExplosions(pkt);
					pkt.amperes = pkt.getAmperesScaled(path.getTotalResistance(pkt));
					path.b.receiver().acceptPacket(pkt, path.facing);
					++i;
				}
			}
		}
	}
	
	@Override
	public BlockPos pos()
	{
		return pos;
	}
	
	@Override
	public boolean canConnectTo(EnumFacing facing)
	{
		return doesConnectTo(facing);
	}
	
	public abstract boolean isInsulated();
	
	@Override
	public void damageNearby(EnergyPacket packet)
	{
		if(!isInsulated())
		{
			damageBy(packet);
		}
	}
	
	public void damageBy(EnergyPacket pkt)
	{
		double res = getResistance(pkt);
		double rad = pkt.getVoltsScaled(res) / getMaxVoltage(pkt);
		AxisAlignedBB aabb = new AxisAlignedBB(pos.getX() - rad, pos.getY() - rad, pos.getZ() - rad, pos.getX() + rad, pos.getY() + rad, pos.getZ() + (rad *= world.isRainingAt(pos.up()) ? 16.0 : 8.0));
		List<EntityLivingBase> ents = world.getEntitiesWithinAABB(EntityLivingBase.class, aabb);
		for(EntityLivingBase ent : ents)
		{
			if(world.rand.nextDouble() >= 0.1 || world.isRemote)
				continue;
			DamageSourceMPC.dealElectricalDamage(ent, pkt);
			HammerCore.particleProxy.spawnZap(world, MultipartAmpereWireBase.getPosition(facing).addVector(pos.getX(), pos.getY(), pos.getZ()), ent.getPositionVector(), Color.CYAN.getRGB());
		}
	}
	
	@Override
	public boolean canPlaceInto(TileMultipart tmp)
	{
		if(facing == null || tmp.getPos() == null || tmp.getWorld() == null)
		{
			return tmp != null && tmp.canPlace_def(this);
		}
		if(!tmp.getWorld().isBlockLoaded(tmp.getPos().offset(facing)) || !tmp.getWorld().getBlockState(tmp.getPos().offset(facing)).isSideSolid(tmp.getWorld(), tmp.getPos().offset(facing), facing.getOpposite()))
		{
			return false;
		}
		for(EnumFacing ef : EnumFacing.VALUES)
		{
			if(ef == facing || ef.getOpposite() == facing || !(tmp.getSignature(MultipartAmpereWireBase.getPosition(ef)) instanceof IWire) || tmp.getSignature(MultipartAmpereWireBase.getPosition(facing)) != null)
				continue;
			return true;
		}
		return super.canPlaceInto(tmp);
	}
	
	@Override
	public AxisAlignedBB getBoundingBox()
	{
		return MultipartAmpereWireBase.getBox(facing);
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setString("Facing", facing.getName());
		Arrays.stream(EnumFacing.values()).forEach(ef ->
		{
			nbt.setBoolean("Facing" + ef, CONNECTIONS.get(ef) == Boolean.TRUE);
		});
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		facing = EnumFacing.byName(nbt.getString("Facing"));
		Arrays.stream(EnumFacing.values()).forEach(ef ->
		{
			CONNECTIONS.put(ef, nbt.getBoolean("Facing" + ef));
		});
	}
	
	@Override
	public ItemStack getPickBlock(EntityPlayer player)
	{
		return getWireStack();
	}
	
	private boolean checkConnect(EnumFacing ef)
	{
		BlockPos npos = pos.offset(ef);
		if(ef == facing || ef.getOpposite() == facing)
			return false;
		if(owner.getSignature(MultipartAmpereWireBase.getPosition(ef)) instanceof IWire)
			return true;
		if(world != null && world.isBlockLoaded(npos))
		{
			TileEntity te = world.getTileEntity(npos);
			TileMultipart tmp = WorldUtil.cast(te, TileMultipart.class);
			IHandlerProvider provider = WorldUtil.cast(te, IHandlerProvider.class);
			IAmpereReceiver receiver = WorldUtil.cast((Object) te, IAmpereReceiver.class);
			IAmpereSender sender = WorldUtil.cast((Object) te, IAmpereSender.class);
			if(tmp == null && provider != null && (provider.hasHandler(ef.getOpposite(), IAmpereReceiver.class, new Object[0]) || provider.hasHandler(ef.getOpposite(), IAmpereSender.class, new Object[0])))
				return true;
			if(receiver != null && receiver.canConnectTo(ef.getOpposite()))
				return true;
			if(sender != null && sender.canConnectTo(ef.getOpposite()))
				return true;
			if(world.isSideSolid(npos, ef.getOpposite()))
				return false;
			if(tmp != null && tmp.getSignature(MultipartAmpereWireBase.getPosition(facing)) instanceof IWire)
				return true;
			if(tmp != null && tmp.getSignature(MultipartAmpereWireBase.getPosition(facing)) != null)
				return false;
			tmp = WorldUtil.cast(world.getTileEntity(npos.offset(facing)), TileMultipart.class);
			if(tmp != null && tmp.getSignature(MultipartAmpereWireBase.getPosition(ef.getOpposite())) instanceof IWire)
				return true;
		}
		return false;
	}
	
	public ItemStack getWireStack()
	{
		return InterItemStack.NULL_STACK;
	}
	
	@Override
	public boolean doesConnectTo(EnumFacing ef)
	{
		return CONNECTIONS.get(ef) == Boolean.TRUE;
	}
	
	@Override
	protected float getMultipartHardness(EntityPlayer player)
	{
		return 1.0f;
	}
	
	@Override
	public float getHardness(EntityPlayer player)
	{
		float hardness = getMultipartHardness(player);
		if(hardness < 0.0f)
			return 0.0f;
		return player.getDigSpeed(Blocks.STONE.getDefaultState(), pos) / hardness / 30.0f * 4;
	}
	
	@Override
	public SoundType getSoundType(EntityPlayer player)
	{
		return SoundType.METAL;
	}
	
	@Override
	public void onRemoved(boolean spawnDrop)
	{
		if(spawnDrop && !world.isRemote)
			WorldUtil.spawnItemStack(world, pos, getWireStack());
		if(network != null)
			network.isAlive = false;
	}
	
	@Override
	public void onNeighborChange(IBlockAccess world, BlockPos pos, BlockPos neighbor)
	{
		Arrays.stream(EnumFacing.values()).forEach(ef -> CONNECTIONS.put(ef, checkConnect(ef)));
		if(this.pos != null && this.owner != null && this.world != null && !this.world.isBlockLoaded(pos.offset(facing)) || !world.getBlockState(pos.offset(facing)).isSideSolid(world, pos.offset(facing), facing.getOpposite()))
			owner.removeMultipart(this, true);
	}
	
	@Override
	public <T extends ITileHandler> boolean hasHandler(EnumFacing facing, Class<T> handler, Object... params)
	{
		return checkConnect(facing) && handler == IAmpereReceiver.class;
	}
	
	@Override
	public <T extends ITileHandler> T getHandler(EnumFacing facing, Class<T> handler, Object... params)
	{
		if(hasHandler(facing, handler, params) && handler == IAmpereReceiver.class)
			return (T) this;
		return null;
	}
	
	public abstract ResourceLocation getModel();
}
