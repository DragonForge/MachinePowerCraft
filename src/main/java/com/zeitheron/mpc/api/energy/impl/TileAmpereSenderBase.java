package com.zeitheron.mpc.api.energy.impl;

import java.util.HashSet;
import java.util.Set;

import com.zeitheron.hammercore.api.handlers.IHandlerProvider;
import com.zeitheron.hammercore.tile.TileSyncable;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.mpc.api.energy.EnergyPacket;
import com.zeitheron.mpc.api.energy.IAmpereReceiver;
import com.zeitheron.mpc.api.energy.IAmpereSender;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;

public abstract class TileAmpereSenderBase extends TileSyncable implements IAmpereSender
{
	private final Set<EnergyPacket> sent = new HashSet<EnergyPacket>();
	
	@Override
	public boolean hasSent(EnergyPacket packet)
	{
		return this.sent.contains(packet);
	}
	
	@Override
	public void clearSendCache(EnergyPacket packet)
	{
		this.sent.remove(packet);
	}
	
	protected boolean send(EnergyPacket packet, EnumFacing facing)
	{
		IHandlerProvider provider;
		TileEntity te = this.world.getTileEntity(this.pos.offset(facing));
		IAmpereReceiver receiver = WorldUtil.cast((Object) te, IAmpereReceiver.class);
		if(te instanceof IHandlerProvider && (provider = WorldUtil.cast(te, IHandlerProvider.class)).hasHandler(facing.getOpposite(), IAmpereReceiver.class))
			receiver = provider.getHandler(facing.getOpposite(), IAmpereReceiver.class);
		if(receiver != null && receiver.canConnectTo(facing.getOpposite()))
		{
			receiver.acceptPacket(packet, facing);
			if(receiver.canExplode(packet))
			{
				receiver.explodeOnVoltageOverload(packet);
			}
			return true;
		}
		return false;
	}
}
