package com.zeitheron.mpc.api.energy.impl.wire;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.zeitheron.mpc.api.energy.IAmpereReceiver;

import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;

public class WireNetwork
{
	public final Set<ReceiverNode> receivers = new HashSet<ReceiverNode>();
	public final Set<SenderNode> senders = new HashSet<SenderNode>();
	public final Map<Long, Object> nodes = new HashMap<Long, Object>();
	public final Map<SenderNode, Set<WirePath>> paths = new HashMap<SenderNode, Set<WirePath>>();
	public boolean isAlive = false;
	
	public void defineWire(IAmpereReceiver wire)
	{
		if(this.nodes.containsValue(wire))
			return;
		this.nodes.put(wire.pos().toLong(), wire);
		
		for(SenderNode s : senders)
		{
			Set<WirePath> paths = new HashSet<>();
			receivers.forEach(r -> paths.add(createIfPossible(s, r)));
			this.paths.put(s, paths);
		}
	}
	
	public void define(SenderNode sender)
	{
		if(nodes.containsKey(sender.pos().toLong()))
			return;
		HashSet<WirePath> paths0 = new HashSet<WirePath>();
		for(ReceiverNode node : receivers)
			paths0.add(createIfPossible(sender, node));
		paths.put(sender, paths0);
		nodes.put(sender.pos().toLong(), sender);
		senders.add(sender);
	}
	
	public void define(ReceiverNode receiver)
	{
		if(this.nodes.containsKey(receiver.pos().toLong()))
			return;
		for(SenderNode node : senders)
		{
			Set<WirePath> paths0 = paths.get(node);
			if(paths0 == null)
			{
				paths0 = new HashSet<WirePath>();
				paths.put(node, paths0);
			}
			WirePath path = createIfPossible(node, receiver);
			paths0.add(path);
		}
		receivers.add(receiver);
		nodes.put(receiver.pos().toLong(), receiver);
	}
	
	public WirePath createIfPossible(SenderNode a, ReceiverNode b)
	{
		HashSet<IAmpereReceiver> wu = new HashSet<IAmpereReceiver>();
		nodes.values().stream().filter(o -> o instanceof MultipartAmpereWireBase).forEach(o ->
		{
			MultipartAmpereWireBase wire = (MultipartAmpereWireBase) o;
			double dist = b.pos().distanceSq(wire.pos());
			if(dist < 1.25)
				wu.add(wire);
		});
		if(wu.size() > 0)
			return new WirePath(a, wu.toArray(new IAmpereReceiver[0]), b);
		return null;
	}
	
	private boolean addOn(Set<IAmpereReceiver> used, BlockPos start, EnumFacing blacklist, ReceiverNode node)
	{
		for(EnumFacing facing : EnumFacing.VALUES)
		{
			if(facing == blacklist)
				continue;
			long pos = start.offset(facing).toLong();
			Object obj = this.nodes.get(pos);
			if(obj == node || obj == node.receiver() || node.pos().toLong() == pos)
				return true;
			if(!(obj instanceof IAmpereReceiver) || used.contains(obj))
				continue;
			used.add((IAmpereReceiver) obj);
			if(!this.addOn(used, start.offset(facing), facing.getOpposite(), node))
				continue;
			return true;
		}
		return false;
	}
	
	public void merge(WireNetwork other)
	{
		other.isAlive = false;
		this.isAlive = true;
		for(Object node : other.nodes.values())
		{
			if(!(node instanceof MultipartAmpereWireBase))
				continue;
			((MultipartAmpereWireBase) node).network = this;
		}
		this.nodes.putAll(other.nodes);
		other.nodes.clear();
		for(ReceiverNode rec : other.receivers)
		{
			this.define(rec);
		}
		for(SenderNode sen : other.senders)
		{
			this.define(sen);
		}
		other.receivers.clear();
		other.senders.clear();
	}
}
