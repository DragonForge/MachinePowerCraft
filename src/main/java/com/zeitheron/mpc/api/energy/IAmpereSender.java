package com.zeitheron.mpc.api.energy;

import com.zeitheron.hammercore.api.handlers.ITileHandler;

import net.minecraft.util.EnumFacing;

public interface IAmpereSender extends ITileHandler
{
	public boolean canConnectTo(EnumFacing var1);
	
	public boolean hasSent(EnergyPacket var1);
	
	public void clearSendCache(EnergyPacket var1);
}