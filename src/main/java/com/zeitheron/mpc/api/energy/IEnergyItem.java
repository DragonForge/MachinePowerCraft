package com.zeitheron.mpc.api.energy;

import net.minecraft.item.ItemStack;

public interface IEnergyItem
{
	public double getAmperesStored(ItemStack var1);
	
	public void setAmperesStored(ItemStack var1, double var2);
	
	public double getMaxAmperes(ItemStack var1);
	
	public double takeAmperes(ItemStack var1, double var2);
	
	public double chargeAmperes(ItemStack var1, double var2);
	
	public boolean canCharge();
}
