package com.zeitheron.mpc.api.energy.impl.wire;

import com.zeitheron.mpc.api.energy.IAmpereSender;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public final class SenderNode
{
	private final IAmpereSender sender;
	private final EnumFacing definedFrom;
	private final TileEntity tile;
	
	private SenderNode(TileEntity te, EnumFacing definedFrom)
	{
		this.tile = te;
		this.definedFrom = definedFrom;
		this.sender = (IAmpereSender) te;
	}
	
	public static SenderNode create(IAmpereSender sender, EnumFacing definedFrom)
	{
		if(!(sender instanceof TileEntity))
		{
			return null;
		}
		return new SenderNode((TileEntity) sender, definedFrom);
	}
	
	public World world()
	{
		return this.tile.getWorld();
	}
	
	public BlockPos pos()
	{
		return this.tile.getPos();
	}
	
	public IAmpereSender sender()
	{
		return this.sender != null && this.sender.canConnectTo(this.definedFrom) ? this.sender : null;
	}
}
