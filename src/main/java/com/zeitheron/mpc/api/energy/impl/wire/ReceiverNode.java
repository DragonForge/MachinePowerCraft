package com.zeitheron.mpc.api.energy.impl.wire;

import com.zeitheron.mpc.api.energy.IAmpereReceiver;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public final class ReceiverNode
{
	private final IAmpereReceiver receiver;
	private final EnumFacing definedFrom;
	private final TileEntity tile;
	
	private ReceiverNode(TileEntity te, EnumFacing definedFrom)
	{
		this.tile = te;
		this.definedFrom = definedFrom;
		this.receiver = (IAmpereReceiver) te;
	}
	
	public static ReceiverNode create(IAmpereReceiver sender, EnumFacing definedFrom)
	{
		if(!(sender instanceof TileEntity))
		{
			return null;
		}
		return new ReceiverNode((TileEntity) sender, definedFrom);
	}
	
	public World world()
	{
		return this.tile.getWorld();
	}
	
	public BlockPos pos()
	{
		return this.tile.getPos();
	}
	
	public IAmpereReceiver receiver()
	{
		return this.receiver != null && this.receiver.canConnectTo(this.definedFrom) ? this.receiver : null;
	}
}
