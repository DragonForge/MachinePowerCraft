package com.zeitheron.mpc.api.energy;

public class EnergyPacket
{
	public static final double ZERO_RESISTANCE = 1.0E-119;
	public double amperes;
	
	private EnergyPacket(double amp)
	{
		this.amperes = amp;
	}
	
	public static EnergyPacket create(double amperes)
	{
		if(amperes == 0.0)
		{
			return new EnergyPacket(1.0E-119);
		}
		return new EnergyPacket(amperes);
	}
	
	public void decreaseAmperes(double resistance)
	{
		this.amperes = this.getAmperesScaled(resistance);
	}
	
	public double getAmperesScaled(double resistance)
	{
		double v = this.getVoltsScaled(resistance);
		return v == 0.0 || resistance == 0.0 ? this.amperes : v / resistance;
	}
	
	public double getVoltsScaled(double resistance)
	{
		return this.amperes * (resistance == 1.0E-119 ? 0.0 : resistance);
	}
	
	public double getResistanceScaled(double volts)
	{
		return volts / this.amperes;
	}
	
	public EnergyPacket[] divide(int count)
	{
		EnergyPacket[] buf = new EnergyPacket[count];
		for(int i = 0; i < count; ++i)
		{
			buf[i] = EnergyPacket.create(this.amperes / count);
		}
		return buf;
	}
	
	public static double calculateTotalResistance(EnergyPacket packet, IAmpereReceiver... wires)
	{
		double value = 0.0;
		for(IAmpereReceiver wire : wires)
		{
			double r;
			if(wire == null || (r = wire.getResistance(packet)) <= 0.0 || r == ZERO_RESISTANCE)
				continue;
			value += r;
		}
		return value <= 0.0 ? ZERO_RESISTANCE : value;
	}
	
	public static double getAmperesBasedOnRV(double resistance, double volts)
	{
		return volts / resistance;
	}
}