package com.zeitheron.mpc.api.energy.impl.wire;

import com.zeitheron.mpc.api.energy.EnergyPacket;
import com.zeitheron.mpc.api.energy.IAmpereReceiver;

import net.minecraft.util.EnumFacing;

public class WirePath
{
	public final SenderNode a;
	public final IAmpereReceiver[] wires;
	public EnumFacing facing;
	public final ReceiverNode b;
	
	public WirePath(SenderNode a, IAmpereReceiver[] wires, ReceiverNode b)
	{
		this.a = a;
		this.wires = wires;
		this.b = b;
	}
	
	public boolean validate()
	{
		return this.a.sender() != null && this.b.receiver() != null;
	}
	
	public double getTotalResistance(EnergyPacket packet)
	{
		return EnergyPacket.calculateTotalResistance(packet, this.wires);
	}
	
	public void applyPossibleExplosions(EnergyPacket packet)
	{
		for(IAmpereReceiver wire : this.wires)
		{
			if(wire != null && wire.canExplode(packet))
				wire.explodeOnVoltageOverload(packet);
			if(wire == null)
				continue;
			wire.damageNearby(packet);
		}
	}
}
