package com.zeitheron.mpc.api.energy;

import com.zeitheron.hammercore.api.handlers.ITileHandler;

import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;

public interface IAmpereReceiver extends ITileHandler
{
	public boolean canConnectTo(EnumFacing var1);
	
	public void acceptPacket(EnergyPacket var1, EnumFacing var2);
	
	public boolean canExplode(EnergyPacket var1);
	
	public void explodeOnVoltageOverload(EnergyPacket var1);
	
	public void damageNearby(EnergyPacket var1);
	
	public BlockPos pos();
	
	public double getResistance(EnergyPacket var1);
	
	public double getMaxVoltage(EnergyPacket var1);
}