package com.zeitheron.mpc.api.armor;

import java.util.HashSet;
import java.util.Set;

import com.zeitheron.hammercore.utils.match.item.ItemContainer;

import net.minecraft.item.ItemStack;

public class ElectrialConductorArmorRegistry
{
	private static Set<ElectrialConductorArmorEntry> entries = new HashSet<ElectrialConductorArmorEntry>();
	
	public static void addEntry(ItemContainer item, float dmgScale, float additionalDamage)
	{
		ElectrialConductorArmorRegistry.addEntry(new ElectrialConductorArmorEntry(item, dmgScale, additionalDamage));
	}
	
	public static void addEntry(ElectrialConductorArmorEntry entry)
	{
		entries.add(entry);
	}
	
	public static ElectrialConductorArmorEntry getEntry(ItemStack stack)
	{
		for(ElectrialConductorArmorEntry e : entries)
		{
			if(!e.matches(stack))
				continue;
			return e;
		}
		return null;
	}
}