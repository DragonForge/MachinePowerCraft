package com.zeitheron.mpc.api.armor;

import com.zeitheron.hammercore.utils.match.item.ItemContainer;
import com.zeitheron.hammercore.utils.match.item.ItemMatchParams;
import com.zeitheron.mpc.api.energy.EnergyPacket;

import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;

public class ElectrialConductorArmorEntry
{
	private ItemMatchParams pars = new ItemMatchParams();
	protected final ItemContainer item;
	protected float dmgScale;
	protected float additionalDamage;
	
	public ElectrialConductorArmorEntry(ItemContainer item, float dmgScale, float additionalDamage)
	{
		if(item.getType() == 1)
			this.pars.setUseOredict(true);
		this.item = item;
		this.dmgScale = dmgScale;
		this.additionalDamage = additionalDamage;
	}
	
	public float getDamageScale(Entity ent, ItemStack armor, EnergyPacket packet, double damagePrimal)
	{
		return this.dmgScale;
	}
	
	public float getAdditionalDamage(Entity ent, ItemStack armor, EnergyPacket packet, double damagePrimal)
	{
		return this.additionalDamage;
	}
	
	public void doAdditionalDamage(Entity ent, ItemStack armor, EnergyPacket packet, double damagePrimal)
	{
	}
	
	public boolean matches(ItemStack stack)
	{
		return !stack.isEmpty() && this.item.matches(stack, this.pars);
	}
}