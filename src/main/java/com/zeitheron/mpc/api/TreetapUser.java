package com.zeitheron.mpc.api;

import java.util.HashSet;
import java.util.Set;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class TreetapUser
{
	private static final Set<TreetapUser> USERS = new HashSet<TreetapUser>();
	
	{
		USERS.add(this);
	}
	
	public static EnumActionResult tryUse(EntityPlayer player, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		for(TreetapUser user : USERS)
		{
			if(!user.canUse(player, worldIn, pos, hand, facing, hitX, hitY, hitZ))
				continue;
			return user.use(player, worldIn, pos, hand, facing, hitX, hitY, hitZ);
		}
		return EnumActionResult.PASS;
	}
	
	public boolean canUse(EntityPlayer player, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		return false;
	}
	
	public EnumActionResult use(EntityPlayer player, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		return EnumActionResult.PASS;
	}
}