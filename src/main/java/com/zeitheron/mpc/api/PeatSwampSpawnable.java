package com.zeitheron.mpc.api;

import java.util.HashMap;
import java.util.Map;

import net.minecraft.init.Biomes;
import net.minecraft.world.biome.Biome;

public class PeatSwampSpawnable
{
	private static final Map<Biome, Double> CHANCES = new HashMap<Biome, Double>();
	
	public static void registerSpawnChance(Biome b, double chance)
	{
		CHANCES.put(b, chance);
	}
	
	public static double getSpawnChance(Biome b)
	{
		Double d = CHANCES.get(b);
		if(d == null || d.isInfinite() || d.isNaN())
		{
			return -1.0;
		}
		return d;
	}
	
	static
	{
		PeatSwampSpawnable.registerSpawnChance(Biomes.PLAINS, 0.005);
		PeatSwampSpawnable.registerSpawnChance(Biomes.ROOFED_FOREST, 0.02);
		PeatSwampSpawnable.registerSpawnChance(Biomes.FOREST, 0.03);
		PeatSwampSpawnable.registerSpawnChance(Biomes.BIRCH_FOREST, 0.03);
		PeatSwampSpawnable.registerSpawnChance(Biomes.SWAMPLAND, 0.04);
	}
}