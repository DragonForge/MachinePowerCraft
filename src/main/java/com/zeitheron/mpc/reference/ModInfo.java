package com.zeitheron.mpc.reference;

public class ModInfo
{
	public static final String MOD_ID = "mpc";
	public static final String MOD_NAME = "Machine Power Craft";
	public static final String MOD_VERSION = "@VERSION@";
}